# AutoSD-Demo Changelog

## [0.1.0] - 2024-09-24

### 🚀 Features

- **[addons]** Use selinux types - ([95c7a8d](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/95c7a8d3e4f2b43984f809172ad06c6855e54382)) - Albert Esteve
- **[addons]** Dbus-broker addon - ([98aecee](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/98aecee96c516e1d58be9ecb748d3963140516c8)) - Albert Esteve
- **[addons]** Add mutter addon - ([a50afa3](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/a50afa3b965480a190d82ea89848adb74e77d6a8)) - Albert Esteve
- **[addons]** Use dbus_broker in mutter - ([cae5c4c](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/cae5c4c471c275b7bb3a7d8c8ebe750a14abdbb5)) - Albert Esteve
- **[addons]** Add wayland_session - ([c0c5a00](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/c0c5a003eefff9879a11d798ab35f346e98edace)) - Albert Esteve
- **[addons]** Make weston use wayland_session - ([737f629](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/737f6292f7a5bfef597d905d6a62830dd0fa2cb7)) - Albert Esteve
- **[addons]** Wayland_session pam config - ([6a11b9e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/6a11b9e98dd7b0a3b3f603883498825ce15b32ea)) - Albert Esteve
- **[addons]** Add README.md - ([0f812f8](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/0f812f803accd3dd2493e59a247de48c5762a844)) - Albert Esteve
- **[addons]** Wayland_session create user dir - ([7cd1d0c](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/7cd1d0ccf390265f15b475dfd8bd0f4bb1468f48)) - Albert Esteve
- **[addons]** Add can_utils - ([b33213e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/b33213ef8becdb284307462b4073d556e7aebc03)) - Albert Esteve
- **[addons]** Validator addon - ([324f4e9](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/324f4e9a009a1a2d0cc915c1e910958c9634b226)) - Roberto Majadas
- **[addons]** Add "iputils" useful package - ([46167b6](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/46167b6a23f6cdb0639fd33c57f4b0745f1e86a2)) - Pavel Bar
- **[addons]** Add alias for wayland - ([761f704](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/761f704e08d2c498cdf864f9f9de30b0ade128f2)) - Albert Esteve
- **[addons]** Add d_spy - ([8303df8](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/8303df8691c74f96ba28cb80f9e77ba0275cbad7)) - Albert Esteve
- **[ansible]** Remove unused common variables - ([a6577e0](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/a6577e01b84ce63c0bc71e01da1622689e432230)) - Pavel Bar
- **[ansible]** Improve task names - ([0e9e21f](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/0e9e21faa795c92171c2a17200b29a3da01bc7d4)) - Pavel Bar
- **[ansible]** Match the path in the task name with real path - ([a492a5f](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/a492a5f9f8b4ca1dc4c16049f78c35f3d86804e9)) - Pavel Bar
- **[ansible]** Copy only regular files - ([7e8c37a](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/7e8c37a3cf2a887c31a21a2acd07c51fd1d76c55)) - Albert Esteve
- **[cmd_build]** Always clean after build - ([93a11df](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/93a11df3aec1913b4a67b7a507ce46f8489b3ed2)) - Albert Esteve
- **[cmd_build]** Clean also during prepare fails - ([e11b39b](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/e11b39b32e75e96b03e4534481a9865589109ec1)) - Albert Esteve
- **[cmd_build]** Expand "build-dir" path when passing it to "automotive-image-builder" - ([433c50f](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/433c50ffde90fd818fe792a646b358c52b56a55d)) - Pavel Bar
- **[cmd_build]** Improve "--no-clean" usage user-experience - ([8b7be44](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/8b7be4457a3a0b1dde7af3abd0c2c80beef605d9)) - Pavel Bar
- **[cmd_build]** Allow quadlet disable - ([b9c0c4f](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/b9c0c4ff53e383c4da6cf7f7b239e179d47f434c)) - Albert Esteve
- **[cmd_build]** Conditional qm layer - ([af0b916](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/af0b9169006ba36e358ededdcba570d547e6dd85)) - Albert Esteve
- **[cmd_build]** Small fixes - ([7cfe295](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/7cfe295b33cf56733f15807106da18a407faea5e)) - Albert Esteve
- **[cmd_build]** Add mpp_vars processsing - ([56de4b2](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/56de4b277c051ae33308e5cb7d663f126acd4801)) - Albert Esteve
- **[cmd_build]** Preprocess all mpp_vars - ([e35d40e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/e35d40e00b2561c36dc599c2121e155bca85aa8a)) - Albert Esteve
- **[cmd_build]** Consider mpp_vars for qm layer - ([0a41312](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/0a41312f5ba069426def9bc34b8925c2d8c7ee43)) - Albert Esteve
- **[cmd_build]** Support different exports - ([99b347b](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/99b347b6e949bb0a9a4cad03930b9405f6953377)) - Albert Esteve
- **[cmd_build]** Image mode configurable - ([59ed3cd](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/59ed3cd1fecfdfc2d15941c1e1430bb64f344ab8)) - Albert Esteve
- **[cmd_build]** Add defines options - ([2252bf9](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/2252bf9deef3e74187a3c0e3348b2ff4057a6b0e)) - Albert Esteve
- **[cmd_build]** Add ostree-repo option - ([fda1341](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/fda1341a843a281a81f6f7e8488c07b9bb9153c0)) - Albert Esteve
- **[cmd_build]** Takes defaults from settings if exists - ([f38e7c0](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/f38e7c080ad8a59e9a65b7066a991587954e42dd)) - Roberto Majadas
- **[cmd_build]** Progress bar for aib - ([3655a48](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/3655a487c32356aa3ed73d89e8ff5d60bf4e95f5)) - Albert Esteve
- **[cmd_build]** Fix bug with distro flag - ([c7ca682](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/c7ca68253313b6f7ee5e51a8e5d50d329bacc78e)) - Roberto Majadas
- **[cmd_compose]** Limit config search - ([ae6f551](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/ae6f551ea803c9592f6720f06312f5f1d3d7195c)) - Albert Esteve
- **[cmd_compose]** Become_required false - ([f79d2bb](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/f79d2bb56ceb84aff99b8af02dfcbb50df091a97)) - Albert Esteve
- **[cmd_show]** Fix osbuild parameter - ([63040c8](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/63040c874ae7083ad858cda3d25e65d7a6c41a48)) - Albert Esteve
- **[config]** Refactor init params - ([0152e42](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/0152e42cd83d9831197d34377cb1ed473a148fef)) - Albert Esteve
- **[config]** Add support for nested addons - ([b255b26](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/b255b26f0a882630ae4df199d353c73a56f69c86)) - Albert Esteve
- **[config]** Add global settings - ([9add6a2](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/9add6a29a8277bb2f92c0a703cda331560e67060)) - Albert Esteve
- **[config]** Renamed local (project) configuration filenames - ([c0642a8](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/c0642a8ae080bd99f90acba94eaa868b3e0344c3)) - Pavel Bar
- **[error_handling]** Add traceback for unexpected exceptions - ([faf41c5](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/faf41c52ca994ab98350b0f785bcb8cce3b4d3df)) - Albert Esteve
- **[examples]** Add examples folder - ([25ca0cb](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/25ca0cb9750b653ae76326f42d1aa411a6b91ccb)) - Albert Esteve
- **[examples]** Add ASIL-QM IPC demo - ([b4774df](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/b4774dffa4dd88776d87476153c83c7914212444)) - Albert Esteve
- **[examples/ffi-demo]** Add memory restriction - ([37fe336](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/37fe336d2b725d31bc005962d04ee4978d3ba59d)) - Albert Esteve
- **[examples/ffi-demo]** Add the missing "flush()" call - ([6f6250b](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/6f6250b8d2820fab9b2092ed308325b564463e58)) - Pavel Bar
- **[executor]** Fix debug parameter lookup - ([2daf1a7](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/2daf1a71af089546e9136b4ef5b224605882ebef)) - Albert Esteve
- **[executor]** Change the "remote" parameter default value - ([33ca37f](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/33ca37f9e1c93ed775513267bee958d664df21b2)) - Pavel Bar
- **[executor]** Change debug strategy - ([ea7ad59](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/ea7ad5974aee4da058ad1f456da25f83ec378c7a)) - Albert Esteve
- **[executor]** Fix invoke aib runner - ([7fd9b4e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/7fd9b4e84195133bb085de7ac4e3a9b526a624ad)) - Albert Esteve
- **[executor]** Trim lines and check failed - ([757adc7](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/757adc789eaf975e466cb09e51c5fce6bd6a728f)) - Albert Esteve
- **[logging]** Filter wrapper generalisation - ([826ac0d](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/826ac0d1183898d2cfad4f8993587d481751d2c0)) - Albert Esteve
- **[logging]** Verbose errors - ([5b2d7c7](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/5b2d7c7a5bd465b456b00c4c6c829b5a6c5e5c61)) - Albert Esteve
- **[logging]** Code refactoring to simplify the implementation (1/3) - ([8049d56](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/8049d56a65b529d2389552fd08513158a2911d29)) - Pavel Bar
- **[logging]** Code refactoring to simplify the implementation (2/3) - ([9de6f0e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/9de6f0efa1aa6dda1f3251743d97763b374ce15e)) - Pavel Bar
- **[logging]** Code refactoring to simplify the implementation (3/3) - ([ff0d58e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/ff0d58e89873a2a3ff2278ee2dc8369f245520d9)) - Pavel Bar
- **[osbuild]** Drop unnecessary includes - ([f60cd02](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/f60cd02b3f0fa54eb6b3d264e61f1df3a112baca)) - Albert Esteve
- **[osbuild]** Add gid stage - ([bdbe666](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/bdbe666fad806a327acb1bb2defd3f5b39611e3e)) - Albert Esteve
- **[osbuild]** Fix spacing - ([a20676a](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/a20676a706fcee085be0494cc7849091ce63302c)) - Albert Esteve
- Add support for preloading addon configurations - ([ec7ebe7](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/ec7ebe727214e6d1ee0bfd1dfad88f06092ef085)) - Roberto Majadas
- Add hostname configuration to image.mpp.yaml.j2 file - ([94b62d2](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/94b62d27d60e0e9eddbb7bbca318a791573f2244)) - Roberto Majadas
- Add systemd services configuration for QM and root filesystems - ([750215e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/750215e537c7f1a79658e5aa73c28ddaf83e6d2f)) - Roberto Majadas
- Add shell_extras addon - ([7e508c1](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/7e508c122e8525a9dc2be16fb614d25c7b269614)) - Roberto Majadas
- Add export command to autosdo-demo - ([b8de648](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/b8de64895132c83c7503e3fdb42c14de3c329ee8)) - Roberto Majadas
- Add podman_next addon - ([6c1da0d](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/6c1da0dfdf715037e5ac8c50ceb6bd225504484a)) - Roberto Majadas
- Add qm_next addon - ([552dd8c](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/552dd8c74a144ad3f4e0dbc696453c0f538a57ab)) - Albert Esteve
- Add netcat to shell_extras - ([abf9e65](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/abf9e659856136c2747145f1fc03a6cecf4bef69)) - Albert Esteve
- Add automotive-image-builder to the autosd-demo setup - ([636ea21](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/636ea219b6229c1d25f6a8a68637b97de1c3289d)) - Roberto Majadas
- Add build --verbosity flag - ([89b36af](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/89b36af7302518f4c9f21180234a84bfdcaade59)) - Albert Esteve
- Add the "--git" option to the "new" command - ([a9b714b](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/a9b714bca813b81787792c411d0ee59cb03affcd)) - Pavel Bar
- Introduce "FEDORA_VERSION" build argument - ([7516eee](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/7516eee5bdeca8e475e7b01d61c81accedfd616e)) - Pavel Bar
- Add "--build-arg" logic to podman build - ([6cccbb7](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/6cccbb7921fb35b36098ed4a26b637f9b04d781e)) - Pavel Bar
- Add secrets and interpolation - ([f87dcd3](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/f87dcd381b6eb73b505d0d6ae26f69eff94500c4)) - Albert Esteve
- Add docker build arguments section in "documentation.rst" - ([adce45c](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/adce45cfcc1cd04d68e7ada454cbc3334fda6714)) - Pavel Bar
- Add files' download section in "documentation.rst" - ([7dde930](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/7dde930d32970b5e023b61875dcfe8e9430e0745)) - Pavel Bar
- Add SELinux troubleshooting rpm - ([1db5bf4](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/1db5bf4da6ae98d01eeb45feb9611582bfd0aaf6)) - Albert Esteve
- Add initial weston addon - ([603b488](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/603b4880f941f57e097e0d13b2ac5f1f09ca0f8e)) - Roberto Majadas
- Add examples/wayland - ([debb67f](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/debb67f5e51a7ddc80be2fb4d357813e55625c14)) - Roberto Majadas
- Add more utilities to shell_extras addon - ([cd72d3e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/cd72d3e56c6dc23ac5b65e3d060b905ba73ff7df)) - Shmuel Melamud
- Add dracut support - ([765aa60](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/765aa6012cc0abb6c3d677e7ca07abc8c74b2bdc)) - Roberto Majadas
- Add 'addon' subcommands in the CLI - ([5b08273](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/5b0827300973aae8d97099ec052cc0512aee98b8)) - Shmuel Melamud
- Add back the build arguments support to the Ansible playbook - ([a9143b2](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/a9143b22794555fb30207ca4fd09523dc48f934b)) - Pavel Bar

### 🐛 Bug Fixes

- **[cmd_build]** Make build raise when aib fails - ([80e0067](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/80e00673c34947c20d3d5dc29401d2907e2a6695)) - Albert Esteve
- Clean unsetted systemd default values - ([0f7cb49](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/0f7cb498c75786362bb7d28b67d397e711c2ed4f)) - Albert Esteve
- Improve systemd orchestration for mutter - ([fb6dc43](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/fb6dc439cc23769376c4c831c4c25fecb7ac3ffc)) - Albert Esteve

### 🚜 Refactor

- **[build]** Move to core - ([4a6fec7](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/4a6fec78a82d492f3de1439289995d58b8653560)) - Albert Esteve
- **[core.build]** Split osbuild code - ([5b75b0a](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/5b75b0a61be61a7165898c1e95936073844d2dbd)) - Albert Esteve
- **[core.osbuild]** Make class updateable - ([96d4149](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/96d4149fc41ff4dff51cf4e29ee74bc87f1abedc)) - Albert Esteve
- Eliminate duplicate code in "export" command - ([bf733f9](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/bf733f97352d54ba5249366c712bb582aee5a9ea)) - Pavel Bar
- Improve disallowed commands verification flow - ([284e789](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/284e78936df1aceab3a3b9469ba8787ac4088c58)) - Pavel Bar
- Use "in_autosd_demo()" function - ([176e9fe](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/176e9fea771e2881060fb7e05c11fd884d9741dc)) - Pavel Bar
- Introduce constants for global and local configuration filenames - ([092ccce](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/092ccce815bac7a7670ebddf5f0bbc443030069c)) - Pavel Bar
- Improve code related to remote hosts - ([2b6b03b](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/2b6b03bcd4f7694d28ade9d0b3e1f00832c1618e)) - Pavel Bar

### 📚 Documentation

- **[README.md]** Improving documentation - ([4a148b4](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/4a148b4ab3444ba9a9e16787e7f3809d0e487f16)) - Pavel Bar
- **[README.md]** Improving documentation - ([999a6a7](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/999a6a7af03c0511b8924eb4a9eb0ddbb3d6e768)) - Pavel Bar
- Use sphinx_rtd_theme - ([2e4ed6e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/2e4ed6eae8bd926bd17257fe49346a079128be55)) - Roberto Majadas
- Add initial autosd-demo documentation - ([1eef4d1](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/1eef4d1df59576c439d7c08e38a7d916c7a465c8)) - Roberto Majadas
- Improve "--help" messages - ([2c5e604](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/2c5e6040312d695a332ed26f7807e7e9dec65006)) - Pavel Bar
- Improve "CONTRIBUTING.md" & "README.md" - ([e1c8bdf](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/e1c8bdfc0abc304dc03e6f191777b912071f52d5)) - Pavel Bar
- Improve "quick-start.rst" - ([ff64326](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/ff643264d922e456d436c6b3275d03bdf663b38b)) - Pavel Bar
- Fixed "secrets.toml" filename - ([bacec9c](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/bacec9c13dc4ba18f4706325b0ade71363c31587)) - Pavel Bar
- Fix typo - ([fb83d97](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/fb83d97ea56a77c184dc0621e81e657a63a88070)) - Albert Esteve
- Mpp_vars sections - ([78d065a](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/78d065ad3a8275de2293084a750c8dd304e8c417)) - Albert Esteve
- Create empty files and directories - ([764418d](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/764418dbce40e2e947d3ca1c6c31fefc35e89129)) - Shmuel Melamud
- Addons commands - ([176da13](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/176da1385ad1375d55a4b838d399be6c7e822bcb)) - Shmuel Melamud

### 🔧 Building

- **[rpm]** New build-rpm script - ([675d5e2](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/675d5e2e001eccfdf99169e2c30482d6981f9ee7)) - Roberto Majadas

### ⚙️ Miscellaneous Tasks

- **[changelog]** Add git-cliff configuration - ([c1f0162](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/c1f01625037d97113144e81038a26bea8df614da)) - Roberto Majadas
- **[changelog]** Add initial CHANGELOG.md - ([07051f5](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/07051f51b7f9a9cb43bbc327c54d3652fcb0a999)) - Roberto Majadas
- **[ci]** Add general workflow:rules - ([119c678](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/119c678e4ae7caf5fa923edcdb7f002ccc06dfcc)) - Roberto Majadas
- **[containers]** Remove old autosd-demo container - ([4df6d05](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/4df6d05ababa5ab3050c15575bd6862589d1f47a)) - Roberto Majadas
- **[pip-pkg]** Remove internal pip package - ([fe0dfb3](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/fe0dfb32a932501e8e643205ee9a2734c4b70727)) - Roberto Majadas
- **[release]** Add a bump.sh script to generate a new version - ([92f4b82](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/92f4b82881c957aa65d59800be8f8233ff8e0962)) - Roberto Majadas
- **[release]** Add initial stages & jobs for gitlab-ci release process - ([90a0883](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/90a0883c39e6638f3b145b0af32b5efa8ac39696)) - Roberto Majadas
- **[release]** Enforce ci workflow rules for release stages - ([33bea69](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/33bea69ff4d9eccf238c68f507ba70664a8ece87)) - Roberto Majadas

### 🧪 Testing

- Quadlet rm unused labels - ([a550268](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/a5502682c23d402ce235bd473887c9006890d3cc)) - Albert Esteve

### Other (unconventional)

- Add description for "host" commands option - ([43f57d8](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/43f57d82302fb1c722b34f04aa7260d8b61bc86c)) - Pavel Bar

## [0.0.1] - 2024-04-06

### 🚀 Features

- **[cmd]** Add exception handler - ([8ed36e6](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/8ed36e60b85d44e225931b16435fb0258ad8e33a)) - Albert Esteve
- **[cmd_build]** Fail after prepare - ([f81da20](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/f81da200af9da6706cf2de24604cd85e67b9c4be)) - Albert Esteve
- **[cmd_containers]** Add new compose command - ([a8fcce0](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/a8fcce0cea5ef159f4dad98b182e33f0045dd91a)) - Albert Esteve
- **[config]** Make enrich to class method - ([e01c81d](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/e01c81d2f04d553642f1a7a3aadb940d956549da)) - Albert Esteve
- **[config]** Refactoring to make it compatible with dynaconf>3.1.2 - ([ceddbbe](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/ceddbbe10975d3d8b67066d3b70161436a791813)) - Roberto Majadas
- **[logging]** Filter task by name - ([5b9650e](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/5b9650e498e70956768e2af15a27e961ee0a6e37)) - Albert Esteve
- Add README - ([afa5661](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/afa56616db8c13779d11b2303c1cb3b412b5473c)) - Roberto Majadas
- Add minimal sphinx doc - ([244c717](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/244c7173783910f1dcf898704097ae4dd19ec24e)) - Roberto Majadas
- Add 'new' command to create demos and refactor tests - ([cf796fc](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/cf796fc723a1beaa0ed22ede442c7c9cb3fd1e1d)) - Roberto Majadas
- Add configurable settings, 'show' command, and respective test - ([a6a3b11](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/a6a3b11b43b646f60e9911af49c3a7bf5a49ce7c)) - Roberto Majadas
- Add 'setup' command to create demo environment - ([e344b13](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/e344b138eac9032fb6048568ecc82a7eb5958945)) - Roberto Majadas
- Add multi-environment configurations support - ([8c72296](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/8c72296e1a0e0ce2063c049cb788b9810c402ecd)) - Roberto Majadas
- Add initial 'build' command - ([c048a12](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/c048a12ccf7e5470e3d8bd8ada3786b88abaf29f)) - Roberto Majadas
- Add build options & osbuid template, and enhance testing - ([6c1077c](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/6c1077c87b6a29c9ece4ad8082fa76fe775b1109)) - Roberto Majadas
- Add get_build_env_config function and tests - ([61fa2d1](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/61fa2d15e14a9def40d891ebc8384d0bbdd8451b)) - Roberto Majadas
- Add ansible filter plugin for case filtering - ([6ee6f41](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/6ee6f4137f1c5acfbbdc4b3450b303e728cecbe2)) - Roberto Majadas
- Add settings enrichment for container configurations - ([e7fe82c](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/e7fe82c2186de09c3c9da917eca3bf178d335efa)) - Roberto Majadas
- Add containers and systemd files to the osbuild image - ([36c55e1](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/36c55e1a370f501a7dbe5adbdf6edf9564ab2dd6)) - Roberto Majadas
- Add integration tests and update Ansible, CI files - ([efd7259](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/efd7259325f7518eb7b1c018a447b4352cd6ef70)) - Roberto Majadas
- Add support for extra files in build process - ([dcd221f](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/dcd221fe04f87fdcfa898941e69bcd174776f423)) - Roberto Majadas
- Add secrets config to autosd_demo.settings - ([8c8995d](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/8c8995d9651946140f1b936cfc86feb9a16d6334)) - Roberto Majadas
- Add podman-compose as required package - ([8c603c1](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/8c603c1dc1f93ad91c4153351060d9630a5be415)) - Roberto Majadas
- Add rpm build insfrastructure - ([bd8b4c2](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/bd8b4c2e8b60d37600c95ecae066dd1c7e846b78)) - Roberto Majadas
- Add exec perms to tools/build-srpm.sh - ([4d90ee4](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/4d90ee4656d3faffded4e1587ee106c277e97987)) - Roberto Majadas
- Add python3 devel deps for rpm build - ([3cc9666](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/3cc96668ffa6b28a76c544931e57be8adcf2f25d)) - Roberto Majadas

### 📚 Documentation

- **[README.md]** Fix wrong project mentions - ([da5b48f](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/commit/da5b48fe54de3964d489a43b349f5520e9bfda72)) - Albert Esteve
