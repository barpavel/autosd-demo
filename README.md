# AutoSD Demo

[Website](https://gitlab.com/CentOS/automotive/src/autosd-demo)
|
[Getting started](https://centos.gitlab.io/automotive/src/autosd-demo/quick-start.html)
|
[Documentation](https://centos.gitlab.io/automotive/src/autosd-demo/)
|
[Contributing](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/blob/main/CONTRIBUTING.md)

## Why AutoSD Demo

AutoSD demo is a CLI tool for creating new AutoSD demos.

In a nutshell, AutoSD demo is a sandbox for developers to experiment with new
setups and scenarios, and design demos on quick iterations.
It was designed to be an easy ramp-up for developers.

This is an upstream-only project with no guarantees. Currently, it is only
supported on Fedora.

## Report Bugs

If you encounter any problem or bug running this tool, feel free to open an
[Issue](https://gitlab.com/CentOS/automotive/src/autosd-demo/-/issues)
in the proper Section of the repository.

## License

AutoSD Demo is released under the MIT License. See the [LICENSE](LICENSE) file for more details.

## Authors

- Roberto Majadas <rmajadas@redhat.com>
- Albert Esteve <aesteve@redhat.com>
- Pavel Bar <pbar@redhat.com>
- Shmuel Melamud <smelamud@redhat.com>
