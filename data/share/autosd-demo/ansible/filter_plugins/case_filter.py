#!/usr/bin/python3


class FilterModule:
    def filters(self):
        return {"pascal_case": self.pascal_case, "sd_pascal_case": self.sd_pascal_case}

    def pascal_case(self, input_text):
        components = input_text.lower().split("_")
        return "".join(component.title() for component in components)

    def sd_pascal_case(self, input_text):
        """Converts input text to PascalCase,
        used specifically for naming systemd fields."""
        if any(c.isupper() for c in input_text):
            return input_text
        else:
            return self.pascal_case(input_text)
