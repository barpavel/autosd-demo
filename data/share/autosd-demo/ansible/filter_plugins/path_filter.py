#!/usr/bin/python3
from jinja2 import Template


class FilterModule:
    def filters(self):
        return {
            "render_path": self.render_path,
        }

    def render_path(self, input_text, conf):
        template = Template(input_text)
        return template.render(conf=conf)
