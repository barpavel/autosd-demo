---
- hosts: all
  gather_facts: yes
  tasks:
    - name: Setting common variables
      ansible.builtin.import_tasks:
        file: "{{ conf.centos_sig.autosd_demo.playbooks_dir }}/common-build-vars.yaml"

    - name: Prepare project required directories
      ansible.builtin.file:
        path: "{{ build_dir }}/{{ item }}"
        state: directory
        recurse: yes
      with_items: "{{ build.src.directories }}"
      when: build.src.directories is defined
      tags: [build-image, build-export, build-apps]

    - name: Download project required remote files
      ansible.builtin.get_url:
        url: "{{ item.url }}"
        checksum: "{{ item.checksum }}"
        dest: "{{ build_dir }}/{{ item.file }}"
      with_items: "{{ build.src.fetch_files }}"
      when: build.src.fetch_files is defined
      tags: [build-image, build-export]

    - name: Prepare project required files
      ansible.builtin.copy:
        src: "{{ build_dir + '/' if is_remote }}{{ item.src }}"
        dest: "{{ build_dir }}/{{ item.dest }}"
        remote_src: "{{ true if is_remote else omit}}"
        mode: preserve
      with_items: "{{ build.src.files }}"
      vars:
        is_remote: "{{ item.remote_src is defined and item.remote_src }}"
      when:
        - item.src is defined
        - build.src.files is defined
      tags: [build-image, build-export, build-apps]

    - name: Prepare project required templated files
      ansible.builtin.template:
        src: "{{ item.src }}"
        dest: "{{ build_dir }}/{{ item.dest }}"
      with_items: "{{ build.src.templated_files }}"
      when: build.src.templated_files is defined
      tags: [build-image, build-export, build-apps]

    - name: Touch project required files
      ansible.builtin.file:
        path: "{{ build_dir }}/{{ item.dest }}"
        state: touch
      with_items: "{{ build.src.files }}"
      when:
        - item.src is not defined
        - build.src.files is defined
      tags: [build-image, build-export, build-apps]

    - name: Prepare directories for container building
      ansible.builtin.file:
        path: "{{ item }}"
        setype: "{% if item.endswith('/storage') %}container_var_lib_t{% else%}_default{% endif %}"
        state: directory
      with_items:
        - "{{ current_local_containers_dir }}"
        - "{{ current_local_containers_dir }}/storage"
      when: build.containers | length > 0
      tags: [build-image, build-apps]

    - name: Provision configuration files for container building
      ansible.builtin.template:
        src: "{{ item.src }}"
        dest: "{{ item.dest }}"
      with_items:
        - { src: "{{ conf.centos_sig.autosd_demo.templates.containers.containers_conf_path }}",
            dest: "{{ current_local_containers_dir }}/containers.conf"}
        - { src: "{{ conf.centos_sig.autosd_demo.templates.containers.storage_conf_path }}",
            dest: "{{ current_local_containers_dir }}/storage.conf"}
      when: build.containers | length > 0
      tags: [build-image, build-apps]

    - name: Generate registry auth file
      containers.podman.podman_login:
        authfile: "{{ current_build_dir }}/auth.json"
        username: "{{ conf.registry_creds[item].username | default('') }}"
        password: "{{ conf.registry_creds[item].password | default('') }}"
        registry: "{{ conf.registry_creds[item].registry | default('') }}"
      loop: "{{ conf.registry_creds.keys() }}"
      when: conf.registry_creds is defined
      tags: [build-image, build-apps, build-compose]

    - name: Pull containers for the image
      containers.podman.podman_image:
        name: "{{ item.image_ref }}"
      loop: "{{ build.containers }}"
      environment:
        REGISTRY_AUTH_FILE: "{{ current_build_dir }}/auth.json"
        CONTAINERS_CONF: "{{ current_local_containers_dir }}/containers.conf"
        CONTAINERS_STORAGE_CONF: "{{ current_local_containers_dir }}/storage.conf"
      when: build.containers | length > 0 and item.container_context is undefined
      tags: [build-image, build-apps]

    - name:  Build containers for the image
      containers.podman.podman_image:
        name: "{{ item.image_ref }}"
        path: "{{ current_containers_dir }}/{{ item.id }}"
        force: true
        build:
          file: "{% if item.container_file is defined %}{{ current_containers_dir }}/{{ item.id }}/{{ item.container_file }}}{% endif %}"
          cache: yes
          extra_args: "{{ item.extra_args }}"
        state: build
      loop: "{{ build.containers }}"
      environment:
        REGISTRY_AUTH_FILE: "{{ current_build_dir }}/auth.json"
        CONTAINERS_CONF: "{{ current_local_containers_dir }}/containers.conf"
        CONTAINERS_STORAGE_CONF: "{{ current_local_containers_dir }}/storage.conf"
      when: build.containers | length > 0 and item.container_context is defined
      tags: [build-image, build-apps]

    - name: Prepare directories for compose building
      ansible.builtin.file:
        path: "{{ build_dir }}/{{ item }}"
        setype: "{% if item.endswith('/storage') %}container_var_lib_t{% else%}_default{% endif %}"
        state: directory
      loop: "{{ build.compose.build.directories }}"
      tags: [build-compose]

    - name: Provision configuration files for compose building
      ansible.builtin.template:
        src: "{{ item.src }}"
        dest: "{{ build_dir }}/{{ item.dest }}"
      loop: "{{ build.compose.build.templated_files }}"
      tags: [ build-compose ]

    - name: Ensure compose directory is clean
      ansible.builtin.file:
        path: "{{ build_dir }}/compose"
        state: absent
      tags: [ build-compose ]

    - name: Prepare required directories for the compose services
      ansible.builtin.file:
        path: "{{ build_dir }}/{{ item }}"
        state: directory
      loop: "{{ build.compose.src.directories }}"
      when: build.compose.src.directories is defined
      tags: [ build-compose, build-export ]

    - name: Provision required files for the compose services
      ansible.builtin.copy:
        src: "{{ item.src }}"
        dest: "{{ build_dir }}/{{ item.dest }}"
        mode: preserve
      loop: "{{ build.compose.src.files }}"
      when: build.compose.src.files is defined
      tags: [ build-compose, build-export ]

    - name: Provision required templated files for the compose services
      ansible.builtin.template:
        src: "{{ item.src }}"
        dest: "{{ build_dir }}/{{ item.dest }}"
      loop: "{{ build.compose.src.templated_files }}"
      when: build.compose.src.templated_files is defined
      tags: [ build-compose, build-export ]

    - name: Generate .env file for the compose file.
      ansible.builtin.copy:
        dest: "{{ build_dir }}/{{ build.compose_file_path|dirname }}/.env"
        content: |
          COMPOSE_STORAGE_DIR="{{ build_dir }}/{{ build.compose.storage_dir }}"
      when: build.compose.containers is defined
      tags: [ build-compose ]

    - name: Generate compose file "$BUILD_DIR/{{ (build_dir, build.compose_file_path) | path_join | relpath(build_dir) }}"
      ansible.builtin.copy:
        dest: "{{ build_dir }}/{{ build.compose_file_path }}"
        content: "{{ build.compose_file | to_nice_yaml(indent=2) }}"
      when: build.compose.containers is defined
      tags: [ build-compose, build-export ]

    - name: Pull containers for compose
      containers.podman.podman_image:
        name: "{{ item.image_ref }}"
      loop: "{{ build.compose.containers }}"
      environment:
        REGISTRY_AUTH_FILE: "{{ current_build_dir }}/auth.json"
        CONTAINERS_CONF: "{{ build_dir }}/{{ item.build.containers_conf }}"
        CONTAINERS_STORAGE_CONF: "{{ build_dir }}/{{ item.build.storage_conf }}"
      when: build.compose.containers and item.build.src_dir is undefined
      tags: [ build-compose ]

    - name: Build containers for compose
      containers.podman.podman_image:
        name: "{{ item.image_ref }}"
        path: "{{ build_dir }}/{{ item.build.src_dir }}"
        force: true
        build:
          file: "{% if item.container_file is defined %}{{ build_dir }}/{{ item.build.src_dir }}/{{ item.container_file }}{% endif %}"
          cache: yes
        state: build
      loop: "{{ build.compose.containers }}"
      environment:
        REGISTRY_AUTH_FILE: "{{ current_build_dir }}/auth.json"
        CONTAINERS_CONF: "{{ build_dir }}/{{ item.build.containers_conf }}"
        CONTAINERS_STORAGE_CONF: "{{ build_dir }}/{{ item.build.storage_conf }}"
      when: build.compose.containers and item.build.src_dir is defined
      tags: [ build-compose ]

    - name: Prepare directories for apps building
      ansible.builtin.file:
        path: "{{ local_build_dir if build.is_remote else build_dir }}/{{ item }}"
        setype: "{% if item.endswith('/storage') %}container_var_lib_t{% else%}_default{% endif %}"
        state: directory
      loop: "{{ build.apps|map(attribute='build.directories')|sum(start=[]) }}"
      delegate_to: "{{ 'localhost' if build.is_remote else omit }}"
      tags: [build-apps]

    - name: Provision configuration files for apps building
      ansible.builtin.template:
        src: "{{ item.src }}"
        dest: "{{ local_build_dir if build.is_remote else build_dir }}/{{ item.dest }}"
      loop: "{{ build.apps|map(attribute='build.templated_files')|sum(start=[]) }}"
      delegate_to: "{{ 'localhost' if build.is_remote else omit }}"
      tags: [build-apps]

    - name: Prepare required directories for the apps
      ansible.builtin.file:
        path: "{{ local_build_dir if build.is_remote else build_dir }}/{{ item }}"
        state: directory
      loop: "{{ build.apps|map(attribute='src.directories')|sum(start=[]) }}"
      delegate_to: "{{ 'localhost' if build.is_remote else omit }}"
      tags: [build-apps]

    - name: Provision required files for the apps
      ansible.builtin.copy:
        src: "{{ item.src }}"
        dest: "{{ local_build_dir if build.is_remote else build_dir }}/{{ item.dest }}"
        mode: preserve
      loop: "{{ build.apps|map(attribute='src.files')|sum(start=[]) }}"
      delegate_to: "{{ 'localhost' if build.is_remote else omit }}"
      tags: [build-apps]

    - name: Provision required templated files for the apps
      ansible.builtin.template:
        src: "{{ item.src }}"
        dest: "{{ local_build_dir if build.is_remote else build_dir }}/{{ item.dest }}"
      loop: "{{ build.apps|map(attribute='src.templated_files')|sum(start=[]) }}"
      delegate_to: "{{ 'localhost' if build.is_remote else omit }}"
      tags: [build-apps]

    - name: Create remote app folders
      ansible.builtin.file:
        path: "{{ build_dir }}/{{ item }}"
        state: directory
      when: build.is_remote
      loop: "{{ src_dirs + containers_dirs }}"
      vars:
        src_dirs: "{{ build.apps|map(attribute='src_dir') }}"
        containers_dirs: "{{ build.apps|map(attribute='containers_dir') }}"
      tags: [build-apps]

    - name: Copy app files to remote
      ansible.builtin.synchronize:
        src: "{{ local_build_dir }}/{{ item }}/"
        dest: "{{ build_dir }}/{{ item}}"
        rsync_opts: -X
        recursive: true
      when: build.is_remote
      loop: "{{ src_dirs + containers_dirs }}"
      vars:
        src_dirs: "{{ build.apps|map(attribute='src_dir') }}"
        containers_dirs: "{{ build.apps|map(attribute='containers_dir') }}"
      tags: [build-apps]

    - name: Pull containers for apps
      containers.podman.podman_image:
        name: "{{ item.image_ref }}"
      loop: "{{ build.apps|map(attribute='containers')|sum(start=[]) }}"
      environment:
        REGISTRY_AUTH_FILE: "{{ current_build_dir }}/auth.json"
        CONTAINERS_CONF: "{{ build_dir }}/{{ item.build.containers_conf }}"
        CONTAINERS_STORAGE_CONF: "{{ build_dir }}/{{ item.build.storage_conf }}"
      when: build.apps | length > 0 and item.container_context is undefined
      tags: [build-apps]

    - name: Build containers for apps
      containers.podman.podman_image:
        name: "{{ item.image_ref }}"
        path: "{{ build_dir }}/{{ item.build.src_dir }}"
        force: true
        build:
          file: "{% if item.container_file is defined %}{{ build_dir }}/{{ item.build.src_dir }}/{{ item.container_file }}{% endif %}"
          cache: yes
          extra_args: "{{ item.extra_args }}"
        state: build
      loop: "{{ build.apps|map(attribute='containers')|sum(start=[]) }}"
      environment:
        REGISTRY_AUTH_FILE: "{{ current_build_dir }}/auth.json"
        CONTAINERS_CONF: "{{ build_dir }}/{{ item.build.containers_conf }}"
        CONTAINERS_STORAGE_CONF: "{{ build_dir }}/{{ item.build.storage_conf }}"
      when: build.apps | length > 0 and item.container_context is defined
      tags: [build-apps]

    - name: Gather apps' container images info
      containers.podman.podman_image_info:
        name: "{{ item.image }}:{{ item.version }}"
      environment:
        REGISTRY_AUTH_FILE: "{{ current_build_dir }}/auth.json"
        CONTAINERS_CONF: "{{ build_dir }}/{{ item.build.containers_conf }}"
        CONTAINERS_STORAGE_CONF: "{{ build_dir }}/{{ item.build.storage_conf }}"
      loop: "{{ build.apps|map(attribute='containers')|sum(start=[]) }}"
      register: apps_containers
      when: build.apps | length > 0
      tags: [build-apps]

    - name: Update apps' quadlet files with images' hashes
      ansible.builtin.lineinfile:
        path: "{{ local_build_dir if build.is_remote else build_dir }}/{{ container.quadlet_file }}"
        regexp: "^Image={{ container.systemd.container.image }}"
        line: "Image={{ container.systemd.container.image|split(':')|first }}@{{ image.Digest }}"
      loop: "{{ apps_containers.results }}"
      delegate_to: "{{ 'localhost' if build.is_remote else omit }}"
      vars:
        image: "{{ item.images[0] }}"
        container: "{{ item.item }}"
      when: apps_containers | length > 0
      tags: [ build-apps ]

    - name: Sign apps' quadlet files
      ansible.builtin.command:
        argv:
          - validator
          - sign
          - "--key={{ conf.validator.private_key }}"
          - "--relative-to={{ local_build_dir if build.is_remote else build_dir }}/{{ (item.dest | split('/'))[:7] | path_join }}"
          - "{{ local_build_dir if build.is_remote else build_dir }}/{{ item.dest }}"
      loop: "{{ build.apps|map(attribute='src.templated_files')|sum(start=[]) }}"
      delegate_to: "{{ 'localhost' if build.is_remote else omit }}"
      when: item.sign == 'true' and item.dest.endswith('.container')
      tags: [ build-apps ]

    - name: Sign (invalid) apps' quadlet files
      ansible.builtin.copy:
        dest: "{{ local_build_dir if build.is_remote else build_dir }}/{{ item.dest }}.sig"
        content: "Invalid transaction signature"
      loop: "{{ build.apps|map(attribute='src.templated_files')|sum(start=[]) }}"
      when: item.sign == 'invalid' and item.dest.endswith('.container')
      tags: [ build-apps ]

    - name: Sync app files to remote
      ansible.builtin.synchronize:
        src: "{{ local_apps_dir }}"
        dest: "{{ current_apps_dir }}"
        recursive: true
      when: build.is_remote
      tags: [ build-apps ]

    - name: Copy container images to the dist storage folder
      ansible.builtin.command:
        argv:
          - skopeo
          - copy
          - "containers-storage:{{ item.image_ref }}"
          - "containers-storage:[overlay@{{ build_dir }}/{{ item.build.dist_container_storage_dir }}]{{ item.image_ref }}"
      loop: "{{ build.apps|map(attribute='containers')|sum(start=[]) }}"
      environment:
        REGISTRY_AUTH_FILE: "{{ current_build_dir }}/auth.json"
        CONTAINERS_CONF: "{{ build_dir }}/{{ item.build.containers_conf }}"
        CONTAINERS_STORAGE_CONF: "{{ build_dir }}/{{ item.build.storage_conf }}"
      when: build.apps | length > 0
      tags: [build-apps]

    - name: Create tarballs for apps
      community.general.archive:
        dest: "{{ build_dir }}/{{ item.tarball_path }}"
        path: "{{ build_dir }}/{{ item.dist_dir }}/"
        format: gz
      become: true
      loop: "{{ build.apps }}"
      when: build.apps | length > 0
      tags: [build-apps]

    - name: Copy tarball from remote
      ansible.builtin.fetch:
        src: "{{ build_dir }}/{{ item.tarball_path }}"
        dest: "{{ local_apps_dir }}/{{ item.tarball_path }}"
        flat: True
      loop: "{{ build.apps }}"
      when: build.is_remote and build.app_sign and build.app_valid
      tags: [build-apps]

    - name: Sign apps' tarball
      ansible.builtin.command:
        argv:
          - validator
          - sign
          - "--key={{ conf.validator.private_key }}"
          - "{{ local_apps_dir if build.is_remote else build_dir }}/{{ item.tarball_path }}"
      loop: "{{ build.apps }}"
      delegate_to: "{{ 'localhost' if build.is_remote else omit }}"
      when: build.app_sign and build.app_valid
      tags: [ build-apps ]

    - name: Sign (invalid) apps' tarball
      ansible.builtin.copy:
        dest: "{{ build_dir }}/{{ item.tarball_path }}.sig"
        content: "Invalid transaction signature"
      loop: "{{ build.apps }}"
      when: build.app_sign and not build.app_valid
      tags: [ build-apps ]

    - name: Create installers for apps
      ansible.builtin.template:
        src: "{{ conf.centos_sig.autosd_demo.templates.apps.bundle_script }}"
        dest: "{{ build_dir }}/{{ item.installer_path }}"
      loop: "{{ build.apps }}"
      tags: [build-apps]

    - name: Create bundles for apps
      ansible.builtin.shell: |
        cat {{ bundle_script }} {{ bundle_tarball }} > {{ bundle_app }}
      vars:
        bundle_tarball: "{{ build_dir }}/{{ item.tarball_path }}"
        bundle_script: "{{ build_dir }}/{{ item.installer_path }}"
        bundle_app: "{{ build_dir }}/{{ item.bundle_path }}"
      loop: "{{ build.apps }}"
      when: build.apps | length > 0
      tags: [ build-apps ]

    - name: Prepare automotive-image-builder directories
      ansible.builtin.file:
        path: "{{ item }}"
        state: directory
      with_items:
        - "{{ current_src_dir }}/images/"
        - "{{ current_aib_mpp_cache_dir }}"
        - "{{ current_aib_build_cache_dir }}"
      tags: [build-image, build-export]

    - name: Generate automotive-image-builder simple manifest
      ansible.builtin.template:
        src: "{{ conf.centos_sig.autosd_demo.templates.aib.image_path }}"
        dest: "{{ current_src_dir }}/images/{{ conf.name }}.aib.yml"
      tags: [build-image, build-export]

    - name: Generate compose.sh
      ansible.builtin.template:
        src: "{{ conf.centos_sig.autosd_demo.templates.export.compose_script }}"
        dest: "{{ current_src_dir }}/compose.sh"
        mode: 0755
      when: build.compose.containers is defined
      tags: [ build-export ]

    - name: Generate build-container.sh
      ansible.builtin.template:
        src: "{{ conf.centos_sig.autosd_demo.templates.export.build_containers_script }}"
        dest: "{{ current_src_dir }}/build-containers.sh"
        mode: 0755
      tags: [ build-export ]

    - name: Generate build.sh
      ansible.builtin.template:
        src: "{{ conf.centos_sig.autosd_demo.templates.export.build_script }}"
        dest: "{{ current_src_dir }}/build.sh"
        mode: 0755
      tags: [ build-export ]

    - name: Ensure export directory exists
      ansible.builtin.file:
        path: "{{ build.export.dest_path }}"
        state: directory
      tags: [ build-export ]

    - name: Copy sources to export directory
      ansible.builtin.synchronize:
        src: "{{ current_src_dir }}/"
        dest: "{{ build.export.dest_path }}"
        recursive: true
      tags: [ build-export ]

    - name: Create "{{ build.export.archive_path|basename }}"
      community.general.archive:
        dest: "{{ build.export.archive_path }}"
        path: "{{ build.export.dest_path }}"
        format: gz
        remove: true
      when: build.export.archive
      tags: [ build-export ]
