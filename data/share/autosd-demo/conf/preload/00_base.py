import autosd_demo.utils
from autosd_demo.utils.addons import AddonType, get_addons_metadata

BASE_DIR = autosd_demo.utils.find_project_settings().parent.as_posix()
BUILD_DIR = (autosd_demo.utils.find_project_settings().parent / "build").as_posix()
CENTOS_SIG = {
    "autosd_demo": {
        "share_dir": autosd_demo.AUTOSD_DEMO_DATA_PATH.as_posix(),
        "compose_dir": autosd_demo.AUTOSD_DEMO_COMPOSE_PATH.as_posix(),
        "playbooks_dir": (autosd_demo.AUTOSD_DEMO_DATA_PATH / "ansible").as_posix(),
        "templates": {
            "containers": {
                "quadlet": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH / "systemd.container.j2"
                ).as_posix(),
                "containers_conf_path": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH
                    / "containers"
                    / "containers.conf.j2"
                ).as_posix(),
                "storage_conf_path": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH / "containers" / "storage.conf.j2"
                ).as_posix(),
            },
            "apps": {
                "bundle_script": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH / "apps" / "bundle.sh.j2"
                ).as_posix(),
            },
            "compose": {
                "containers_conf_path": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH / "compose" / "containers.conf.j2"
                ).as_posix(),
                "storage_conf_path": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH / "compose" / "storage.conf.j2"
                ).as_posix(),
            },
            "osbuild": {
                "image_path": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH / "image.mpp.yaml.j2"
                ).as_posix()
            },
            "aib": {
                "image_path": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH / "image.aib.yaml.j2"
                ).as_posix()
            },
            "export": {
                "build_containers_script": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH
                    / "export"
                    / "build-containers.sh.j2"
                ).as_posix(),
                "build_script": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH / "export" / "build.sh.j2"
                ).as_posix(),
                "compose_script": (
                    autosd_demo.AUTOSD_DEMO_DATA_PATH / "export" / "compose.sh.j2"
                ).as_posix(),
            },
        },
        "addons": get_addons_metadata(),
        "compose_addons": get_addons_metadata(AddonType.COMPOSE_ADDON),
    }
}
