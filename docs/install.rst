*******************
Install
*******************

Prerequisites
=============
Before you start, ensure you have the following:

- Python (version 3.6 or above).
- Git installed on your machine.
- Access to a terminal or a command prompt.

Installation
============
There are two ways to install `autosd-demo`:

Option 1: Install from GitLab repository
----------------------------------------
If you're a developer and want to contribute to `autosd-demo` or use the latest development version, follow these steps:

1. Clone the repository from GitLab:

.. code-block:: bash

   $ git clone https://gitlab.com/CentOS/automotive/src/autosd-demo.git

2. Navigate to the cloned directory:

.. code-block:: bash

   $ cd autosd-demo

3. Create a Python virtual environment (optional but recommended):

.. code-block:: bash

   $ python3 -m venv venv

4. Activate the Python virtual environment:

.. code-block:: bash

    # For Linux or MacOS
    $ source venv/bin/activate

    # For Windows
    $ .\venv\Scripts\activate

5. Install the mandatory dependencies when working in development mode:

.. code-block:: bash

   $ pip install -r requirements/dev.txt
   $ pip install -e .

Option 2: Install using RPM package
-----------------------------------
1. Enable the Copr repository:

.. code-block:: bash

   $ sudo dnf copr enable @centos-automotive-sig/autosd-demo

2. Install the `autosd-demo` packages:

.. code-block:: bash

   $ sudo dnf install autosd-demo
