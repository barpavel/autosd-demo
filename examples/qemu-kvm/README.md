# Qemu-KVM

This example project demonstrates a [QEMU](https://www.qemu.org/)
VMM running in AutoSD, inside the QM container.

### System configuration

Since this example generates an image that then is used as disk to run with
Qemu, having a nested Qemu running inside, we need to ensure that nested
virtualization is supported in our host system.

You can check how to enable nested virtualization in KVM in this
[link](https://docs.fedoraproject.org/en-US/quick-docs/using-nested-virtualization-in-kvm/).

Depending on your system, the process may differ a little.

### Build image

Build the image by entering the project folder and running:

```shell
$ autosd-demo build
```

The image is created in the project root folder:

```
-rw-r--r--. 1 aesteve aesteve 2.2G Sep 19 14:51 autosd-qemu-qemu-kvm-package.x86_64.qcow2
```

### Run image

Use `automotive-image-runner` to run the image with QEMU:

```shell
$ automotive-image-runner autosd-qemu-qemu-kvm-package.x86_64.qcow2 -device virtio-vga-gl -display dbus,gl=on
```

Note that we chose a DBus display. Feel free to change it to any other available
display type (e.g., `gtk`).

Then, you can run any DBus-specific viewer such as [Snowglobe](https://gitlab.gnome.org/bilelmoussaoui/snowglobe) to
interact with the running VM:

```shell
$ snowglobe --show-host-cursor
```

The viewer window will appear, and a nested Snowglobe pops inside.

<div align="center">
    <img src="./img/qemu-kvm.png" />
</div>
