# Shared Memory IPC Demo

This example project allows two containers to communicate through shared
memory and `eventfd` object for synchronization, and cross environments (ASIL-QM) in AutoSD.

## Architecture

This `autosd-demo` project features two containerized agents that communicate
between them through shared memory.

### Shared memory & synchronization
- The shared memory object is created by server using
[shm_open()](https://man7.org/linux/man-pages/man3/shm_open.3.html)
(with initial zero length) and mounted under `/dev/shm`.
- Then, the size of the object is set to 1024 Bytes using
[ftruncate()](https://man7.org/linux/man-pages/man2/ftruncate.2.html).
- After that both client and server map the shared memory object into memory in their respective
virtual address spaces using [mmap()](https://man7.org/linux/man-pages/man2/mmap.2.html).
- The synchronization is achieved by using [eventfd()](https://man7.org/linux/man-pages/man2/eventfd.2.html),
and the created `eventfd` object is shared through a Unix domain socket.

### The client-server flow:
- Server: the containerized service in the ASIL layer acts as a listener,
continuously waiting for notifications via `eventfd` object, and reading
the message from the shared memory every time such a notification is received.
- Client: the containerized service in the QM layer receives the `eventfd` object
from the server and uses it to notify the server every time a time update (message)
is written to the shared memory.

### QM container Quadlet
QM container Quadlet is accordingly updated to mount:
- The volume that contains the Unix domain socket.
- The shared memory "`/dev/shm/ipc_shm`" file.

Which are created during ASIL boot using `systemd-tmpfiles` mechanism.

To achieve the above actions, the example employs Quadlet's Drop-In feature.

## Usage

### Build image

Build the image by entering the project root folder and running:

```shell
$ autosd-demo build
```

The image is created in the project root folder:

```
-rw-r--r--. 1 aesteve aesteve 1.2G Nov 29 16:46 autosd-qemu-shmem_ipc-package.x86_64.qcow2
```

### Run image

Now you can run the example by doing:

```shell
$ automotive-image-runner autosd-qemu-shmem_ipc-package.x86_64.qcow2
```

QEMU window will pop up. You can log in as usual and check the logs:
- `server.service`:

    ```
    ... localhost systemd[1]: Started server container.
    ... localhost server[2205]:     3c1449aac56875a7eba93394d8032b400c62e54dc48dbdb89c32c2493088761f
    ... localhost server[2269]: Server trying to bind and listen...
    ... localhost server[2269]: Server waiting for client to connect...
    ... server[2269]: Client connected. Sending eventfd...
    ... server[2269]: Server ready. Waiting for messages...
    ... server[2269]: Server received: 2024-11-29 15:29:26
    ... localhost server[2269]: Server received: 2024-11-29 15:29:36
    ... localhost server[2269]: Server received: 2024-11-29 15:29:46
    ```

- `client.service`:

    ```
    ... localhost systemd[1]: Started client container.
    ... localhost client[38]: c1fed8e17f6a3cb95ba04828b4598dddb70cacd67a10a0deb693f45201c5aa01
    ... client[128]: Connected to server. Receiving eventfd...
    ... client[128]: Client ready. Sending messages to the server via shared memory...
    ... client[128]: Client sent: 2024-11-29 15:29:36
    ... client[128]: Client sent: 2024-11-29 15:29:46
    ```
