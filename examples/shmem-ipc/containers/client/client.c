#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/eventfd.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/un.h>

#define SHM_NAME "/ipc_shm"
#define SHM_SIZE 1024
#define SOCKET_PATH "/run/ipc/eventfd_socket"

void get_current_time(char *buffer, size_t size) {
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    if (strftime(buffer, size, "%Y-%m-%d %H:%M:%S", tm_info) == 0) {
        perror("strftime");
        exit(EXIT_FAILURE);
    }
}

// Receive a file descriptor over a Unix domain socket
int recv_fd(int socket) {
    struct msghdr message = {0};
    char ctrl_buf[CMSG_SPACE(sizeof(int))];
    char data[2];
    struct iovec io = { .iov_base = data, .iov_len = sizeof(data) };

    // Prepare the message header
    message.msg_iov = &io;
    message.msg_iovlen = 1;
    message.msg_control = ctrl_buf;
    message.msg_controllen = sizeof(ctrl_buf);

    if (recvmsg(socket, &message, 0) < 0) {
        perror("recvmsg");
        return -1;
    }

    // Extract the file descriptor
    struct cmsghdr *control_message = CMSG_FIRSTHDR(&message);
    if (control_message &&
        control_message->cmsg_len == CMSG_LEN(sizeof(int)) &&
        control_message->cmsg_level == SOL_SOCKET &&
        control_message->cmsg_type == SCM_RIGHTS) {
        return *((int *) CMSG_DATA(control_message));
    }

    return -1;
}

int main() {
    // Open the shared memory
    int shm_fd = shm_open(SHM_NAME, O_RDWR, 0666);
    if (shm_fd == -1) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    // Map the shared memory into the virtual address space
    char *shm_ptr = mmap(NULL, SHM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shm_ptr == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    // Set up a Unix domain socket to connect to the server
    int sock_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock_fd == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_un addr = {0};
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SOCKET_PATH, sizeof(addr.sun_path) - 1);

    // Connect to the server
    if (connect(sock_fd, (struct sockaddr *) &addr, sizeof(addr)) == -1) {
        perror("connect");
        exit(EXIT_FAILURE);
    }

    printf("Connected to server. Receiving eventfd...\n");
    fflush(stdout);

    // Receive the eventfd descriptor from the server
    int efd = recv_fd(sock_fd);
    if (efd == -1) {
        fprintf(stderr, "Failed to receive eventfd from server.\n");
        exit(EXIT_FAILURE);
    }

    printf("Client ready. Sending messages to the server via shared memory...\n");
    fflush(stdout);

    while (1) {
        // Get the current time as a message
        char message[SHM_SIZE] = {0};
        get_current_time(message, SHM_SIZE);

        // Write the message into the shared memory
        snprintf(shm_ptr, SHM_SIZE, "%s", message);

        // Notify the server using eventfd that the message is ready
        if (eventfd_write(efd, 1) == -1) {
            perror("eventfd_write");
            break;
        }

        printf("Client sent: %s\n", message);
        fflush(stdout);

        // Wait for 10 seconds before sending the next message
        sleep(10);
    }

    // Clean up resources
    close(efd);
    close(sock_fd);
    munmap(shm_ptr, SHM_SIZE);
    shm_unlink(SHM_NAME);
    close(shm_fd);

    return 0;
}
