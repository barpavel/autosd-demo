#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/eventfd.h>
#include <string.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/un.h>

#define SHM_NAME "/ipc_shm"
#define SHM_SIZE 1024
#define SOCKET_PATH "/run/ipc/eventfd_socket"

// Send a file descriptor over a Unix domain socket
int send_fd(int socket, int fd) {
    struct msghdr message = {0};
    char ctrl_buf[CMSG_SPACE(sizeof(int))];
    struct iovec io = { .iov_base = "FD", .iov_len = 2 };

    // Prepare the message header
    message.msg_iov = &io;
    message.msg_iovlen = 1;
    message.msg_control = ctrl_buf;
    message.msg_controllen = sizeof(ctrl_buf);

    // Attach the file descriptor
    struct cmsghdr *control_message = CMSG_FIRSTHDR(&message);
    control_message->cmsg_len = CMSG_LEN(sizeof(int));
    control_message->cmsg_level = SOL_SOCKET;
    control_message->cmsg_type = SCM_RIGHTS;
    *((int *) CMSG_DATA(control_message)) = fd;

    if (sendmsg(socket, &message, 0) < 0) {
        perror("sendmsg");
        return -1;
    }

    return 0;
}

int main() {
    // Create or open the shared memory
    int shm_fd = shm_open(SHM_NAME, O_CREAT | O_RDWR, 0666);
    if (shm_fd == -1) {
        perror("shm_open");
        exit(EXIT_FAILURE);
    }

    // Resize the shared memory to the desired size
    if (ftruncate(shm_fd, SHM_SIZE) == -1) {
        perror("ftruncate");
        exit(EXIT_FAILURE);
    }

    // Map the shared memory into the virtual address space
    char *shm_ptr = mmap(NULL, SHM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (shm_ptr == MAP_FAILED) {
        perror("mmap");
        exit(EXIT_FAILURE);
    }

    // Create an eventfd for synchronization
    int efd = eventfd(0, 0);
    if (efd == -1) {
        perror("eventfd");
        exit(EXIT_FAILURE);
    }

    // Set up a Unix domain socket to listen to the client
    int sock_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock_fd == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_un addr = {0};
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SOCKET_PATH, sizeof(addr.sun_path) - 1);
    unlink(SOCKET_PATH);

    printf("Server trying to bind and listen...\n");
    fflush(stdout);

    if (bind(sock_fd, (struct sockaddr *) &addr, sizeof(addr)) == -1) {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    if (listen(sock_fd, 1) == -1) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("Server waiting for client to connect...\n");
    fflush(stdout);

    // Accept a connection from the client
    int client_fd = accept(sock_fd, NULL, NULL);
    if (client_fd == -1) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    printf("Client connected. Sending eventfd...\n");
    fflush(stdout);

    // Send the eventfd descriptor to the client
    if (send_fd(client_fd, efd) == -1) {
        fprintf(stderr, "Failed to send eventfd to client.\n");
        exit(EXIT_FAILURE);
    }

    // Wait for messages in a loop
    printf("Server ready. Waiting for messages...\n");
    fflush(stdout);

    while (1) {
        // Wait for a notification from the client
        eventfd_t val = 0;
        if (eventfd_read(efd, &val) == -1) {
            perror("eventfd_read");
            break;
        }

        // Print the message received in the shared memory
        printf("Server received: %s\n", shm_ptr);
        fflush(stdout);

        // If the client sends "exit", terminate the server
        if (strcmp(shm_ptr, "exit") == 0) {
            printf("Server exiting...\n");
            break;
        }
    }

    // Clean up resources
    close(client_fd);
    close(sock_fd);
    close(efd);
    munmap(shm_ptr, SHM_SIZE);
    shm_unlink(SHM_NAME);
    close(shm_fd);

    return 0;
}
