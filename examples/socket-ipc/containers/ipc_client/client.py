#!/usr/bin/env python3
import datetime
import os
import socket
import sys
import time

SOCKET_PATH = "/run/ipc/ipc.socket"


def systemd_print(*args):
    print(*args)
    sys.stdout.flush()


class SimpleMessage:
    def __init__(self):
        self.now = datetime.datetime.now()

    def __str__(self) -> str:
        return self.now.strftime("%H:%M:%S") + os.linesep

    def to_bytes(self) -> bytes:
        return self.__str__().encode()


def send_loop():
    with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as sock:
        systemd_print("Connecting to:", SOCKET_PATH)
        sock.connect(SOCKET_PATH)

        while True:
            msg = SimpleMessage().to_bytes()
            systemd_print("Sending message:", msg)
            sock.sendall(msg)
            time.sleep(1)


if __name__ == "__main__":
    while True:
        try:
            send_loop()
        except OSError as e:
            systemd_print("Connection failed:", e)
            systemd_print("Retrying...")
            time.sleep(5)
