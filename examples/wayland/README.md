# Wayland

This example project demonstrates a [Wayland](https://wayland.freedesktop.org/)
compositor running in AutoSD, in the QM container.

## Build

Build the image by entering the project folder and running:

```shell
$ autosd-demo build
```

The image is created in the project root folder: `autosd-qemu-wayland-package.x86_64.qcow2`'

## Run

Use `automotive-image-runner` to run the image with QEMU:

```shell
$ automotive-image-runner autosd-qemu-wayland-package-weston.x86_64.qcow2 -vga none -device virtio-gpu-gl -display sdl,gl=on
```

The windows shall automatically pop up.

<div align="center">
    <img src="./img/weston.png" />
</div>
