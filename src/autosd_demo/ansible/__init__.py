from pathlib import Path

from autosd_demo import AUTOSD_DEMO_DATA_PATH
from autosd_demo.utils import in_notebook

AUTOSD_DEMO_ANSIBLE_PATH = Path(AUTOSD_DEMO_DATA_PATH, "ansible")

if in_notebook():  # pragma: no cover
    from autosd_demo.ansible.monkeypatch import (
        monkeypatch_ansible_display_sync,
        monkeypatch_ansible_display_sys_stdout_reconfiguration,
        monkeypatch_ansible_plugins_loader_module,
    )

    monkeypatch_ansible_display_sys_stdout_reconfiguration()
    monkeypatch_ansible_display_sync()
    monkeypatch_ansible_plugins_loader_module()
else:
    from autosd_demo.ansible.monkeypatch import (
        monkeypatch_ansible_context,
        monkeypatch_ansible_plugins_loader_module,
    )

    monkeypatch_ansible_context()
    monkeypatch_ansible_plugins_loader_module()

AUTOSD_DEMO_DEFAULT_INVENTORY = AUTOSD_DEMO_ANSIBLE_PATH / "inventory"
