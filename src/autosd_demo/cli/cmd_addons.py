import ast
from pathlib import Path

import click

from autosd_demo.cli.exceptions import AddonNotFound, catch_exception
from autosd_demo.utils.addons import get_addons_metadata


@click.group()
def addons():
    """Get information about addons"""


@addons.command()
@catch_exception
def list():
    """List the addons available"""
    addons = get_addons_metadata()
    names = sorted(addons.keys())
    for name in names:
        print(name)


@addons.command()
@click.argument("name", nargs=1)
@catch_exception
def show(name):
    """Show details about the addon"""
    addons = get_addons_metadata()
    if name not in addons:
        raise AddonNotFound(f"Cannot find addon '{name}'")
    description = read_addon_info(addons[name])
    print()
    print(name)
    print("-" * len(name))
    print(f"\n{description}\n")


def read_addon_info(addon):
    module_path = Path(addon["base_dir"]) / "__init__.py"
    try:
        module = ast.parse(module_path.read_text())
        description = ast.get_docstring(module)
        if description is not None:
            return description
    except FileNotFoundError:
        pass
    return "No description found."
