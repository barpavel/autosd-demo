import sys

import click

from autosd_demo.cli.exceptions import catch_exception
from autosd_demo.core import Playbooks
from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor
from autosd_demo.settings import settings


@click.group()
def app():
    """App related commands"""


@app.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@click.option(
    "--build-id",
    help="ID to be used for the build",
    default=None,
)
@click.option(
    "--no-clean",
    "clean",
    is_flag=True,
    help="Skip build folder cleanup after the build",
    default=True,
)
@click.option(
    "--unsigned",
    "signed",
    is_flag=True,
    help="Skip generating the app signature",
    default=True,
)
@click.option(
    "--invalid",
    "valid",
    is_flag=True,
    help="Generate an invalid app signature",
    default=True,
)
@click.argument("app_id", nargs=-1, required=False)
@catch_exception
def build(host, build_id, clean, signed, valid, app_id):
    """Build an application"""
    clean_settings_for_build_apps(app_id)

    executor = get_executor(host_id=host, become_required=True)
    with executor() as ctx:
        build_env = BuildEnv(host, build_id=build_id)
        build_env["build"].update({"app_sign": signed, "app_valid": valid})

        try:
            ctx.run_playbook(
                Playbooks.PRE_BUILD,
                tags=["build-apps", "untagged"],
                extra_vars=build_env,
            )

            if build_env.hooks["pre_build"]:
                build_env.reload_src_contents()

            ctx.run_playbook(
                Playbooks.PROVISION_BUILD,
                tags=["build-apps", "untagged"],
                extra_vars=build_env,
            )

            ctx.run_playbook(
                Playbooks.POST_BUILD,
                tags=["build-apps", "untagged"],
                extra_vars=build_env,
            )
        finally:
            if clean:
                ctx.run_playbook(
                    Playbooks.CLEAN_BUILD,
                    tags=["build-apps", "untagged"],
                    extra_vars=build_env,
                )


def clean_settings_for_build_apps(app_id=[]):
    if not settings.get("apps", {}):
        sys.exit(0)
    for container_id in settings.get("containers", {}):
        settings["containers"][container_id]["quadlet"] = False
    if "extra_files" in settings:
        del settings["extra_files"]
    if app_id:
        app_to_skip = set(settings["apps"]) - set(app_id)
        for app_name in app_to_skip:
            del settings["apps"][app_name]


@app.command()
@click.option(
    "-H",
    "--host",
    help="Remote host ID to run the command",
    default="automotive_image_runner",
)
@click.argument("app_file", type=click.Path(exists=True), required=True)
@catch_exception
def install(host, app_file):  # pragma: no cover
    executor = get_executor(host_id=host, become_required=True)
    with executor() as ctx:
        ctx.run_app_installer(app_file)
