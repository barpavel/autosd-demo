import platform
from dataclasses import dataclass
from enum import StrEnum, auto, unique

import click

import autosd_demo.settings as config
from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor

from ..core import Playbooks
from .exceptions import catch_exception


@unique
class Distro(StrEnum):
    AUTOSD = auto()
    AUTOSD9 = auto()
    CS9 = auto()
    ELN = auto()
    F40 = auto()
    F41 = auto()
    RHIVOS = auto()
    RHIVOS1 = auto()


@unique
class ExportType(StrEnum):
    ABOOT = auto()
    ABOOT_SIMG = "aboot.simg"
    CONTAINER = auto()
    EXT4 = auto()
    EXT4_SIMG = "ext4.simg"
    IMAGE = auto()
    OSTREE_COMMIT = "ostree-commit"
    OSTREE_OCI_IMAGE = "ostree-oci-image"
    QCOW2 = auto()
    ROOTFS = auto()
    RPMLIST = auto()
    SIMG = auto()
    TAR = auto()

    def to_ext(self):
        match self:
            case self.ABOOT | self.ABOOT_SIMG:
                return "images"
            case self.CONTAINER | self.TAR:
                return "tar"
            case self.EXT4 | self.EXT4_SIMG:
                return "ext4"
            case self.IMAGE | self.SIMG:
                return "img"
            case self.OSTREE_COMMIT:
                return "repo"
            case self.OSTREE_OCI_IMAGE:
                return "oci-archive"
            case self.QCOW2:
                return "qcow2"
            case self.ROOTFS:
                return None
            case self.RPMLIST:
                return "rpmlist"


@dataclass
class BuildOptions:
    distro: Distro
    export: ExportType
    host: str
    target: str
    image_arch: str
    image_mode: str
    ostree_repo: str

    def refresh(self):
        self.distro = config.settings.get("build.distro", self.distro)
        self.target = config.settings.get("build.target", self.target)
        self.image_mode = config.settings.get("build.image_mode", self.image_mode)
        self.image_arch = config.settings.get("build.image_arch", self.image_arch)
        self.export = config.settings.get("build.export", self.export)
        self.ostree_repo = config.settings.get("build.ostree_repo", self.ostree_repo)

    @property
    def ext(self):
        return self.export.to_ext()

    def get_aib_options(self):
        return (
            f"--distro {self.distro} --target {self.target} "
            f"--mode {self.image_mode} --arch {self.image_arch} "
            f"--export {self.export} "
            f"{f'--ostree-repo {self.ostree_repo} ' if self.ostree_repo else ''}"
        )

    def get_image_name(self, demo_name):
        env_suffix = (
            ""
            if config.settings.current_env.upper() == "DEVELOPMENT"
            else f"-{config.settings.current_env.lower()}"
        )
        return (
            f"{self.distro}-{self.target}-{demo_name}-{self.image_mode}{env_suffix}."
            f"{self.image_arch}.{self.ext}"
        )


@click.command
@click.option(
    "-H",
    "--host",
    help="Remote host ID to run the command          ",
    default=None,
)
@click.option(
    "-d",
    "--distro",
    help="Distro used as image base to build the demo",
    type=click.Choice(list(Distro)),
    default=config.settings.get("build.distro", Distro.AUTOSD),
    show_default=True,
)
@click.option(
    "-t",
    "--target",
    help="Target used to build the demo              ",
    default=config.settings.get("build.target", "qemu"),
    show_default=True,
)
@click.option(
    "-i",
    "--image-mode",
    help="Image mode used to build the demo          ",
    type=click.Choice(["package", "image"]),
    default=config.settings.get("build.image_mode", "package"),
    show_default=True,
)
@click.option(
    "-a",
    "--image-arch",
    help="Image architecture                         ",
    default=config.settings.get("build.image_arch", platform.machine()),
    show_default=True,
)
@click.option(
    "-e",
    "--export",
    help="Export this image type                     ",
    type=click.Choice(list(ExportType)),
    default=config.settings.get("build.export", ExportType.QCOW2),
    show_default=True,
)
@click.option(
    "-E",
    "--environment",
    help="Define the configuration environment       ",
    multiple=True,
    default=[None],
)
@click.option(
    "--generate-deltas",
    "deltas",
    is_flag=True,
    help="Generate ostree deltas for each build      ",
    default=False,
)
@click.option(
    "--define",
    help="Define osbuild key=yaml-value              ",
    multiple=True,
)
@click.option(
    "--ostree-repo",
    help="Path to ostree repo                        ",
    type=click.Path(),
    default=config.settings.get("build.ostree_repo", None),
)
@click.option(
    "--build-id",
    help="ID to be used for the build",
    default=None,
)
@click.option(
    "--no-clean",
    "clean",
    is_flag=True,
    help="Skip build folder cleanup after the build  ",
    default=True,
)
@click.option(
    "-v",
    "--verbose",
    is_flag=True,
    help="Increase image builder verbosity           ",
    default=False,
)
@catch_exception
def build(
    host,
    distro,
    target,
    image_mode,
    image_arch,
    export,
    environment,
    deltas,
    define,
    ostree_repo,
    build_id,
    clean,
    verbose,
):
    """Build the autosd-demo project"""
    # TODO: implement 'rootfs' export type (supported by "automotive-image-builder").
    if export == ExportType.ROOTFS:
        raise AttributeError("Export type 'rootfs' is not supported (yet).")

    options = BuildOptions(
        distro, export, host, target, image_arch, image_mode, ostree_repo
    )

    executor = get_executor(host_id=host, become_required=True)
    with executor() as ctx:
        ctx.run_playbook(Playbooks.DISABLE_SUDO_PASSWD, extra_vars={})
        try:
            for env in environment:
                build_env = _reset_build_env(build_id, options, env=env)
                defines = config.settings.get("build.defines", []) + list(define)

                ctx.run_playbook(
                    Playbooks.PRE_BUILD,
                    tags=["build-image", "untagged"],
                    extra_vars=build_env,
                )
                if len(build_env.hooks["pre_build"]) > 0:
                    build_env.reload_src_contents()
                ctx.run_playbook(
                    Playbooks.PROVISION_BUILD,
                    tags=["build-image", "untagged"],
                    extra_vars=build_env,
                )

                builder_cmd = "/usr/bin/automotive-image-builder"
                if verbose:
                    builder_cmd += " --verbose"
                ctx.run_builder(
                    "REGISTRY_AUTH_FILE="
                    f"$(realpath {build_env.registry_auth.as_posix()}) "
                    "CONTAINERS_CONF="
                    f"$(realpath {build_env.containers_conf.as_posix()}) "
                    "CONTAINERS_STORAGE_CONF="
                    f"$(realpath {build_env.storage_conf.as_posix()}) "
                    f"{builder_cmd} build {options.get_aib_options()}"
                    f"--build-dir {build_env.build_cache_dir.as_posix()} "
                    f"--cache-max-size={build_env.max_cache_size} "
                    f"--mpp-arg=--cache "
                    f"--mpp-arg {build_env.mpp_cache_dir.as_posix()} "
                    f"{' '.join([f'--define {d}' for d in defines])} "
                    f"{build_env.build_src_dir.as_posix()}/images/"
                    f"{build_env.conf['name']}.aib.yml "
                    f"{build_env.build_src_dir.as_posix()}/{build_env['build']['image_name']}"
                )
                ctx.run_playbook(
                    Playbooks.POST_BUILD,
                    tags=["build-image", "untagged"],
                    extra_vars=build_env,
                )
                if deltas and options.ostree_repo:
                    ctx.repo = options.ostree_repo
                    ctx.generate_delta()
        finally:
            if clean and "build_env" in locals():
                ctx.run_playbook(
                    Playbooks.CLEAN_BUILD,
                    tags=["build-image", "untagged"],
                    extra_vars=build_env,
                )
            ctx.run_playbook(Playbooks.ENABLE_SUDO_PASSWD, extra_vars={})


def _reset_build_env(build_id, options, env):
    if env is not None:
        config.reload_settings(env=env)
        options.refresh()
    build_env = BuildEnv(options.host, osbuild=True, build_id=build_id)
    build_env["build"].update(
        {
            "distro": str(options.distro),
            "target": options.target,
            "image_mode": options.image_mode,
            "image_arch": options.image_arch,
            "image_name": options.get_image_name(build_env["conf"]["name"]),
        }
    )

    return build_env
