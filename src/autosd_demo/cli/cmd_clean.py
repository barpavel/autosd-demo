import click

from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor

from ..core import Playbooks
from .exceptions import catch_exception


@click.command
@catch_exception
@click.option(
    "-H",
    "--host",
    help="Remote host ID to run the command                   ",
    default=None,
)
def clean(host):
    """Clean hooks"""
    build_env = BuildEnv(host)

    executor = get_executor(host_id=host, become_required=True)
    with executor() as ctx:
        ctx.run_playbook(
            Playbooks.CLEAN, tags=["clean", "untagged"], extra_vars=build_env
        )
