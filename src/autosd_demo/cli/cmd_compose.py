import click
import yaml

from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor

from ..core import Playbooks
from .exceptions import catch_exception


@click.group()
def compose():
    """Run container compose commands"""


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@click.option(
    "--build-id",
    help="ID to be used for the build",
    default=None,
)
@click.option(
    "--no-clean",
    "clean",
    is_flag=True,
    help="Skip build folder cleanup after the build  ",
    default=True,
)
@click.option(
    "--recreate",
    "recreate",
    is_flag=True,
    help="Recreate containers",
    default=False,
)
@catch_exception
def up(host, build_id, clean, recreate):
    """Start the off-board compose services"""
    build_env = BuildEnv(host, build_id=build_id)
    build_env["build"]["compose"].update({"recreate": recreate})
    executor = get_executor(host_id=host, become_required=True)
    with executor() as ctx:
        try:
            ctx.run_playbook(
                Playbooks.PRE_BUILD,
                tags=["build-compose", "untagged"],
                extra_vars=build_env,
            )
            if build_env.hooks["pre_build"]:
                build_env.reload_src_contents()
            ctx.run_playbook(
                Playbooks.PROVISION_BUILD,
                tags=["build-compose", "untagged"],
                extra_vars=build_env,
            )
            ctx.run_playbook(
                Playbooks.COMPOSE, tags=["compose-up", "untagged"], extra_vars=build_env
            )
        finally:
            if clean:
                ctx.run_playbook(
                    Playbooks.CLEAN_BUILD,
                    tags=["build-compose", "untagged"],
                    extra_vars=build_env,
                )


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@catch_exception
def down(host):
    """Stop the off-board compose services"""
    build_env = BuildEnv(host)
    executor = get_executor(host_id=host)
    with executor() as ctx:
        ctx.run_playbook(
            Playbooks.COMPOSE, tags=["compose-down", "untagged"], extra_vars=build_env
        )


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@catch_exception
def config(host):  # pragma: no cover
    """Display the resulting compose file"""
    build_env = BuildEnv(host)
    content = build_env.get("build", {}).get("compose_file", {})
    print(yaml.dump(content))


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@catch_exception
def ps(host):  # pragma: no cover
    """Show status of containers"""
    build_env = BuildEnv(host)
    executor = get_executor(host_id=host)
    with executor() as ctx:
        containers_conf_path = build_env["build"]["compose"]["containers_conf_path"]
        storage_conf_path = build_env["build"]["compose"]["storage_conf_path"]
        compose_file_path = build_env["build"]["compose_file_path"]

        ctx.run(
            f"CONTAINERS_CONF={build_env.build_dir}/{containers_conf_path} "
            f"CONTAINERS_STORAGE_CONF={build_env.build_dir}/{storage_conf_path} "
            f"podman compose -f {build_env.build_dir}/{compose_file_path} ps"
        )


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@catch_exception
def logs(host):  # pragma: no cover
    """Show logs from services"""
    build_env = BuildEnv(host)
    executor = get_executor(host_id=host)
    with executor() as ctx:
        containers_conf_path = build_env["build"]["compose"]["containers_conf_path"]
        storage_conf_path = build_env["build"]["compose"]["storage_conf_path"]
        compose_file_path = build_env["build"]["compose_file_path"]

        ctx.run(
            f"CONTAINERS_CONF={build_env.build_dir}/{containers_conf_path} "
            f"CONTAINERS_STORAGE_CONF={build_env.build_dir}/{storage_conf_path} "
            f"podman compose -f {build_env.build_dir}/{compose_file_path} logs"
        )


@compose.command()
@click.option("-H", "--host", help="Remote host ID to run the command", default=None)
@catch_exception
def env(host):  # pragma: no cover
    """Show env variables to run podman compose"""
    build_env = BuildEnv(host)
    containers_conf_path = build_env["build"]["compose"]["containers_conf_path"]
    storage_conf_path = build_env["build"]["compose"]["storage_conf_path"]

    print(f"export CONTAINERS_CONF={build_env.build_dir}/{containers_conf_path}")
    print(f"export CONTAINERS_STORAGE_CONF={build_env.build_dir}/{storage_conf_path}")
