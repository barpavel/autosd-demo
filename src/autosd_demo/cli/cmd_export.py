from pathlib import Path

import click

from autosd_demo.cli.exceptions import FolderExists, catch_exception
from autosd_demo.core import Playbooks
from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor


@click.command()
@click.option(
    "-a",
    "--archive",
    help="Export demo as a tarball",
    is_flag=True,
    show_default=True,
    default=False,
)
@click.argument("dest", default=Path.cwd(), type=click.Path(path_type=Path))
@catch_exception
def export(archive, dest):
    """Export demo to automotive-image-builder format"""
    build_env = BuildEnv(None, osbuild=True, export=True)
    demo_name = f'{build_env["conf"]["name"]}-{build_env["conf"]["version"]}'
    dest_path = dest / demo_name
    build_env["build"].update(
        {
            "export": {
                "dest_path": dest_path.resolve().as_posix(),
                "archive": archive,
                "archive_path": (dest / f"{demo_name}.tar.gz").resolve().as_posix(),
            },
        }
    )

    if dest_path.exists():  # pragma: no cover
        raise FolderExists(f"The folder {dest_path} already exists!")

    executor = get_executor(host_id=None)
    with executor() as ctx:
        ctx.run_playbook(
            Playbooks.PRE_BUILD, tags=["build-export", "untagged"], extra_vars=build_env
        )
        if len(build_env.hooks["pre_build"]) > 0:
            build_env.reload_src_contents()
        ctx.run_playbook(
            Playbooks.PROVISION_BUILD,
            tags=["build-export", "untagged"],
            extra_vars=build_env,
        )
        ctx.run_playbook(
            Playbooks.CLEAN_BUILD,
            tags=["build-export", "untagged"],
            extra_vars=build_env,
        )
