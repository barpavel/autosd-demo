from enum import StrEnum, auto, unique

import click

from autosd_demo.core.build import BuildEnv
from autosd_demo.core.executor import get_executor

from ..core import Playbooks
from .exceptions import catch_exception


@unique
class SetupSubCmd(StrEnum):
    INSTALL = auto()
    UPGRADE = auto()
    UNINSTALL = auto()

    def get_setup_options(self):
        match self:
            case SetupSubCmd.INSTALL:
                pkgs_state = "present"
                repo_state = "enabled"
            case SetupSubCmd.UPGRADE:
                pkgs_state = "latest"
                repo_state = "enabled"
            case SetupSubCmd.UNINSTALL:
                pkgs_state = "absent"
                repo_state = "disabled"
            case _:  # pragma: no cover
                raise ValueError(f"Unknown setup operation {self}")

        return {"pkgs_state": pkgs_state, "repo_state": repo_state}


@click.command
@catch_exception
@click.option(
    "-H",
    "--host",
    help="Remote host ID to run the command                   ",
    default=None,
)
@click.option(
    "--operation",
    help="Operation to perform",
    type=click.Choice(list(SetupSubCmd)),
    default=SetupSubCmd.INSTALL,
    show_default=True,
)
def setup(host, operation):
    """Setup autosd-demo environment"""
    build_env = BuildEnv(host)
    build_env["setup_operation"] = operation.get_setup_options()

    executor = get_executor(host_id=host, become_required=True)
    with executor() as ctx:
        ctx.run_playbook(
            Playbooks.SETUP, tags=["install", "untagged"], extra_vars=build_env
        )
