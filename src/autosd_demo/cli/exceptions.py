import traceback
from enum import Enum
from functools import wraps

import click


class FolderExists(Exception):
    pass


class AddonNotFound(Exception):
    pass


class AnsibleError(Exception):
    class Code(Enum):
        OK = 0
        ERROR = 1
        HOSTS_FAILED = 2
        HOSTS_UNREACHABLE = 3
        PARSER_ERROR = 4
        BAD_OPTIONS = 5
        USER_INTERRUPTED = 99
        UNEXPECTED = 250

        def __str__(self) -> str:
            return self.name.replace("_", " ").capitalize()

    def __init__(self, ec: int, playbook: str):
        self.code = self.Code(ec)
        self.playbook = playbook

    def __str__(self) -> str:
        return f'Failed to run playbook "{self.playbook}": {self.code}'


class BuilderError(Exception):
    def __init__(self, messsage: str):
        self.message = messsage

    def __str__(self) -> str:
        return f"automotive-image-builder failed with:\n{' '.join(self.message)}"


class ValidationError(Exception):
    def __init__(self, section: str, key: str, message: str):
        self.section = section
        self.key = key
        self.message = message

    def __str__(self) -> str:
        return f"{self.message} in section {self.section}.{self.key}"


def catch_exception(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (FolderExists, AddonNotFound, AnsibleError, ValidationError) as e:
            raise click.ClickException(str(e))
        except Exception as e:  # pragma: no cover
            click.echo("Unexpected exception occurred!")
            click.echo(traceback.format_exc())
            raise click.ClickException(str(e))

    return wrapper
