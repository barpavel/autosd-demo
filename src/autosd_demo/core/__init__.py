from enum import StrEnum


class Playbooks(StrEnum):
    PRE_BUILD = "pre-build.yaml"
    PROVISION_BUILD = "provision-build.yaml"
    POST_BUILD = "post-build.yaml"
    CLEAN_BUILD = "clean-build.yaml"
    DISABLE_SUDO_PASSWD = "sudo-password-disable.yaml"
    ENABLE_SUDO_PASSWD = "sudo-password-enable.yaml"
    INSTALL = "install.yaml"
    UNINSTALL = "uninstall.yaml"
    SETUP = "setup.yaml"
    CLEAN = "clean.yaml"
    SHOW_AIB_MANIFEST = "show-aib-manifest.yaml"
    COMPOSE = "compose.yaml"
