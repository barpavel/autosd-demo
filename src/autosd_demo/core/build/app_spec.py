from collections import UserDict
from pathlib import Path

from autosd_demo.core.build.sources import AppSources
from autosd_demo.core.build.sources.app import AppBuildSources


class AppSpec(UserDict):
    def __init__(self, conf, build_id, app_id, app_data):
        super().__init__()
        self.conf = conf
        self.app_id = app_id
        self.app_data = app_data.copy()
        self.build_id = build_id
        self.data = {
            "id": app_id,
            "info": app_data.get("info", "Information N/A."),
            "name": app_data["name"],
            "version": app_data["version"],
            "dest_dir": app_data.get("dest_dir", "apps"),
            "src": AppSources(self.conf, Path(self.build_id) / "src"),
            "build": AppBuildSources(self.conf, app_id, build_id),
            "containers": [],
        }
        self.data.update(
            {
                "dist_dir": self.dist_dir.as_posix(),
                "tarball_path": self.tarball_path.as_posix(),
                "installer_path": self.installer_path.as_posix(),
                "bundle_path": self.bundle_path.as_posix(),
                "src_dir": self.src_dir.as_posix(),
                "containers_dir": self.containers_dir.as_posix(),
            }
        )
        self.build_containers_list()

    @property
    def base_dir(self):
        return (
            Path(self.build_id)
            / "src"
            / "files"
            / self.conf["name"]
            / "apps"
            / f"{self['name']}-{self['version']}"
        )

    @property
    def src_dir(self) -> Path:
        return self.base_dir / "src"

    @property
    def dist_dir(self) -> Path:
        return self.base_dir / "dist"

    @property
    def containers_dir(self) -> Path:
        if self.conf.get("cache", {}).get("app_storage", False):
            return Path("cache") / "apps" / self.app_id
        else:
            return Path(self.build_id) / ".apps" / self.app_id

    @property
    def tarball_path(self) -> Path:
        return self.base_dir.parent / f"{self['name']}-{self['version']}.tar.gz"

    @property
    def installer_path(self) -> Path:
        return self.base_dir.parent / f"{self['name']}-{self['version']}.sh"

    @property
    def bundle_path(self) -> Path:
        return self.base_dir.parent / f"{self['name']}-{self['version']}.app"

    def build_containers_list(self):
        for container_id, container_data in self.app_data["containers"].items():
            container_data["id"] = container_id
            layer = container_data.get("layer", "root")
            var_dir = Path("var") if layer == "root" else Path("var") / layer

            c_dist_storage_dir = (
                self.dist_dir
                / var_dir
                / "apps"
                / self["name"]
                / self["version"]
                / "containers"
                / "storage"
            )

            container_data["build"] = {
                # Container config files to build/pull the container locally
                "containers_conf": self["build"].containers_conf_path.as_posix(),
                "storage_conf": self["build"].storage_conf_path.as_posix(),
                # Dist dirs for app container distribution
                "dist_dir": self.dist_dir.as_posix(),
                "dist_container_storage_dir": c_dist_storage_dir.as_posix(),
            }

            if "container_context" in container_data:
                src = Path(container_data["container_context"])
                container_data["build"].update(
                    {
                        # Source files to build the container
                        "src_dir": (self.src_dir / container_id).as_posix(),
                    }
                )
                self["src"].add_container_files(src, container_id, container_data)

            if container_data.get("quadlet", True):
                quadlet_path = self["src"].add_quadlet(
                    self.app_id, container_id, container_data
                )
                container_data["quadlet_file"] = quadlet_path.as_posix()

            self["build"].add_directory(c_dist_storage_dir.parent)
            self["build"].add_directory(c_dist_storage_dir)

            self["containers"].append(container_data)

        self["containers"] = sorted(
            self["containers"], key=lambda c: (int(c["build_order"]), c["id"])
        )
