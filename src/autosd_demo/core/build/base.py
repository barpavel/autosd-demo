from collections import UserDict
from functools import cached_property
from pathlib import Path

from autosd_demo.cli.exceptions import ValidationError
from autosd_demo.settings import settings


class BaseEnv(UserDict):
    @cached_property
    def conf(self):
        return {key.lower(): val for key, val in settings.as_dict().items()}

    def conf_exists(self, *keys):
        return self.key_exists(self.conf, *keys)

    @staticmethod
    def key_exists(element, *keys):
        _element = element
        for key in keys:
            try:
                _element = _element[key]
            except KeyError:
                return False
        return True

    def conf_required(self, section: str, key: str, *option):
        if not self.conf_exists(section, key, *option):
            option_name = ".".join(option)
            raise ValidationError(section, key, f"'{option_name}' is missing")

    @staticmethod
    def reduce_directory_list(element, key):
        dir_paths = [Path(d) for d in sorted(list(set(element[key])))]
        element[key] = [
            dir_x.as_posix()
            for index, dir_x in enumerate(dir_paths)
            if all(dir_x not in dir_y.parents for dir_y in dir_paths[index + 1 :])
        ]

    def __repr__(self):  # pragma: no cover
        return f"{type(self).__name__}({self.data})"
