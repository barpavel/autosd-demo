import re
from datetime import datetime
from functools import cached_property
from pathlib import Path

from autosd_demo.cli.exceptions import ValidationError
from autosd_demo.core.build.app_spec import AppSpec
from autosd_demo.core.build.base import BaseEnv
from autosd_demo.core.build.sources import (
    ComposeServiceBuildSources,
    ComposeServiceSources,
    ImageSources,
    SourcesOp,
)
from autosd_demo.core.osbuild import OSBuildConfig
from autosd_demo.utils import in_autosd_demo


class BuildEnv(BaseEnv):
    LINK_PATTERN = re.compile(r"^[a-z]+://")

    def __init__(self, host, osbuild=False, build_id=None, export=False):
        super().__init__()
        self.host = host
        self.export = export
        self._set_build_env_config(build_id)
        if osbuild:
            self["osbuild"] = OSBuildConfig(self.img_sources)

    @property
    def is_remote(self):
        return self.host is not None

    @property
    def is_export(self):
        return self.export

    @cached_property
    def base_dir(self):
        if self.is_remote:
            return (
                self.conf["remote_host"][self.host]["base_dir"]
                if self.conf_exists("remote_host", self.host, "base_dir")
                else "~"
            )

        return self.conf["base_dir"]

    @property
    def build_id(self):
        return self["build"]["build_id"]

    @property
    def hooks(self):
        return self["hooks"]

    @cached_property
    def build_dir(self):
        if self.is_remote:
            return (
                self.conf["remote_host"][self.host]["build_dir"]
                if self.conf_exists("remote_host", self.host, "build_dir")
                else str(Path(self.base_dir) / "build")
            )

        return self.conf["build_dir"]

    @cached_property
    def build_id_path(self):
        return Path(self.build_dir, self.build_id)

    @property
    def build_src_dir(self):
        return self.build_id_path / "src"

    @property
    def img_sources(self) -> ImageSources:
        return self["build"]["src"]

    @property
    def compose_sources(self) -> ComposeServiceSources:
        return self["build"]["compose"]["src"]

    @property
    def compose_build_sources(self) -> ComposeServiceBuildSources:
        return self["build"]["compose"]["build"]

    @property
    def registry_auth(self):
        return self.build_id_path / "auth.json"

    @cached_property
    def containers_dir(self):
        return (
            Path(self.build_dir) / "cache" / "containers"
            if self.conf["cache"]["containers_storage"]
            else self.build_id_path / ".containers"
        )

    @property
    def storage_conf(self):
        return self.containers_dir / "storage.conf"

    @property
    def containers_conf(self):
        return self.containers_dir / "containers.conf"

    def cache_dir(self, cached):
        return (
            Path(self.build_dir) / "cache" / "aib"
            if cached
            else self.build_id_path / ".aib"
        )

    @property
    def build_cache_dir(self):
        return self.cache_dir(self.conf["cache"].get("aib_build", False)) / "build"

    @property
    def mpp_cache_dir(self):
        return self.cache_dir(self.conf["cache"].get("aib_mpp", False)) / "mpp"

    @property
    def max_cache_size(self):
        return self.conf["cache"].get("aib_max_size", "2GB")

    def _set_build_env_config(self, build_id):
        self.update({"conf": self.conf, "build": {}, "hooks": {}})
        self["build"]["build_id"] = (
            build_id if build_id else f"{datetime.now():%Y%m%d_%H%M%S}"
        )
        self["build"]["is_remote"] = self.is_remote
        if in_autosd_demo():
            self["build"]["base_dir"] = self.base_dir
            self["build"]["build_dir"] = self.build_dir
            self["build"]["src"] = ImageSources(
                self.conf, Path(self.build_id) / "src", self.is_remote
            )
            self._get_configured_hooks()
            self._reset_build_src_contents()
            self._process_extra_files()
            self._process_containers()
            self._process_apps()
            self._process_compose()

    def _get_configured_hooks(self):
        stages = [
            "install",
            "pre_build",
            "post_build",
            "clean_build",
            "uninstall",
            "clean",
        ]
        self["hooks"] = {stage: [] for stage in stages}

        if not self.conf_exists("hooks"):
            return

        for stage in stages:
            hooks = []
            for hook_data in self.conf["hooks"].values():
                hooks.extend(
                    [
                        (
                            hook
                            if Path(hook).is_absolute()
                            # Hooks do not need remote base_dir, so we
                            # always use the conf variable.
                            else str(Path(self.conf["base_dir"]) / hook)
                        )
                        for hook in hook_data.get(stage, [])
                    ]
                )
            self["hooks"][stage] = sorted(hooks, key=lambda path: Path(path).name)

    def reload_src_contents(self):
        self._reset_build_src_contents()
        self._process_extra_files()
        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                if "container_context" in container_data:
                    src = Path(container_data.get("container_context"))
                    self.img_sources.add_container_files(
                        src, container_id, container_data
                    )
                if container_data.get("quadlet", True):
                    self.img_sources.add_quadlet(container_id, container_data)
            self.reduce_directory_list(self["build"]["src"], "directories")

    def _reset_build_src_contents(self):
        self["build"]["src"].reset()

    def _process_compose(self):
        if not self.conf_exists("compose", "services"):
            return

        self._process_compose_init()

        for cs_id, cs_data in self.conf["compose"]["services"].items():
            container = {
                "id": cs_id,
                "build": {
                    "containers_conf": self.compose_build_sources.containers_conf_path.as_posix(),
                    "storage_conf": self.compose_build_sources.storage_conf_path.as_posix(),
                },
            }

            if "build" in cs_data:
                service = self._process_compose_service_build(container, cs_data, cs_id)
            else:
                container["image_ref"] = cs_data["image"]
                service = cs_data.copy()

            if "extra_files" in service:
                self._process_compose_service_extra_files(service)
                service.pop("extra_files")

            self["build"]["compose_file"]["services"][cs_id] = service
            self["build"]["compose"]["containers"].append(container)

        compose_sections = ["networks", "volumes", "configs", "secrets"]
        for compose_section in compose_sections:
            if compose_section in self.conf["compose"]:
                compose_file = self["build"]["compose_file"]
                compose_file[compose_section] = self.conf["compose"][compose_section]

    def _process_compose_init(self):
        self["build"].setdefault("compose", {})
        self["build"].setdefault("compose_file", {"services": {}})

        css = ComposeServiceSources(self.conf, self.build_id, is_export=self.is_export)
        self["build"]["compose"]["src"] = css

        csbs = ComposeServiceBuildSources(self.conf, self.build_id)
        self["build"]["compose"]["build"] = csbs

        c_file_path = (
            Path(self.build_id) / "src" / "compose" / "compose.yml"
            if self.is_export
            else Path("compose") / "compose.yml"
        )
        self["build"].setdefault("compose_file_path", c_file_path.as_posix())
        self.compose_sources.add_directory(c_file_path.parent)

        self["build"]["compose"]["src_dir"] = (
            Path(self.build_id) / "src" / "compose"
        ).as_posix()

        containers_conf = self.compose_build_sources.containers_conf_path.as_posix()
        self["build"]["compose"]["containers_conf_path"] = containers_conf

        storage_conf = self.compose_build_sources.storage_conf_path.as_posix()
        self["build"]["compose"]["storage_conf_path"] = storage_conf

        storage_dir = self.compose_build_sources.storage_dir.as_posix()
        self["build"]["compose"]["storage_dir"] = storage_dir

        self["build"]["compose"].setdefault("containers", [])

    def _process_compose_service_build(self, container, cs_data, cs_id):
        build_data = cs_data["build"]

        if isinstance(build_data, dict) and "context" in build_data:
            src = Path(build_data["context"])
        elif isinstance(build_data, str):
            src = Path(build_data)
        else:
            raise ValueError

        container["image_ref"] = f"localhost/compose/{cs_id}:latest"
        container["build"]["src_dir"] = (
            self.compose_sources.dest_dir / cs_id
        ).as_posix()
        self.compose_sources.add_container_files(src, cs_id)

        custom_cs = cs_data.copy()
        custom_cs.pop("build")
        custom_cs["image"] = container["image_ref"]

        return custom_cs

    def _process_compose_service_extra_files(self, service):
        for ef_id, ef_data in service["extra_files"].items():
            touch = ef_data.get("touch", False)
            delete = ef_data.get("delete", False)

            if touch or delete:
                raise ValidationError(
                    "compose",
                    "services",
                    f"touch or delete cannot be enabled for {ef_id}",
                )

            self.compose_sources.add_extra_files(
                Path(ef_data["src"]),
                Path(ef_data["dest"]),
                SourcesOp.ADD,
            )

    def _process_apps(self):
        self["build"].setdefault("apps", [])
        if self.conf_exists("apps"):
            for app_id, app_data in self.conf["apps"].items():
                # Init app_spec
                app_spec = AppSpec(self.conf, self.build_id, app_id, app_data)

                # Add app_spec to the build.apps
                self["build"]["apps"].append(app_spec)

    def _process_containers(self):
        self["build"].setdefault("containers", [])
        if self.conf_exists("containers"):
            for container_id, container_data in self.conf["containers"].items():
                container_spec = container_data.copy()
                container_spec["id"] = container_id

                if "container_context" in container_data:
                    src = Path(container_data.get("container_context"))
                    self.img_sources.add_container_files(
                        src, container_id, container_data
                    )

                if container_data.get("quadlet", True):
                    self.img_sources.add_quadlet(container_id, container_data)

                self["build"]["containers"].append(container_spec)
            self.reduce_directory_list(self["build"]["src"], "directories")
        self["build"]["containers"] = sorted(
            self["build"]["containers"], key=lambda c: (int(c["build_order"]), c["id"])
        )

    def _process_extra_files(self):
        if self.conf_exists("extra_files"):
            for ef_id, ef_data in self.conf["extra_files"].items():
                touch = ef_data.get("touch", False)
                delete = ef_data.get("delete", False)
                if delete and touch:
                    raise ValidationError(
                        "extra_files",
                        ef_id,
                        "'touch' and 'delete' cannot be enabled simultaneously",
                    )
                if not (delete or touch):
                    self.conf_required("extra_files", ef_id, "src")
                self.conf_required("extra_files", ef_id, "dest")

                if touch:
                    self.img_sources.add_extra_files(
                        None, Path(ef_data["dest"]), SourcesOp.TOUCH, ef_data
                    )
                elif delete:
                    self.img_sources.add_extra_files(
                        None, Path(ef_data["dest"]), SourcesOp.DELETE, ef_data
                    )
                elif self.LINK_PATTERN.match(ef_data["src"]):
                    self.img_sources.add_extra_files(
                        ef_data["src"],
                        Path(ef_data["dest"]),
                        SourcesOp.FETCH,
                        ef_data,
                    )
                else:
                    self.img_sources.add_extra_files(
                        Path(ef_data["src"]),
                        Path(ef_data["dest"]),
                        SourcesOp.ADD,
                        ef_data,
                    )
