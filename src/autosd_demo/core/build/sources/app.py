from collections import UserDict
from enum import StrEnum, auto
from pathlib import Path

from autosd_demo.core.build.sources.base import Sources, SourcesOp


class SignedType(StrEnum):
    TRUE = auto()
    FALSE = auto()
    INVALID = auto()

    @classmethod
    def _missing_(cls, value):
        if isinstance(value, bool):
            return cls(str(value).lower())
        match value:
            case "True" | "yes":
                return cls.TRUE
            case "False" | "no":
                return cls.FALSE
            case _:
                return cls.INVALID


class AppSources(Sources):
    def __init__(self, conf, build_src: Path):
        super().__init__(conf=conf)
        self.data.pop("fetch_files")
        self.data.pop("delete")
        self.build_src_dir = build_src

    def add_container_files(self, src: Path, container_id, container_data):
        dest = Path(container_data["build"]["src_dir"])
        context = {
            "container_id": container_id,
            "container_data": container_data,
        }
        self.add_contents(src, dest, op=SourcesOp.ADD, context=context)

    def add_quadlet(self, app_id, container_id, container_data) -> Path:
        centos_sig_templates = self.conf["centos_sig"]["autosd_demo"]["templates"]
        layer = container_data.get("layer", "root")
        src = Path(centos_sig_templates["containers"]["quadlet"])
        quadlet_name = container_data.get("quadlet_name", container_id)
        dest = (
            Path(container_data["build"]["dist_dir"])
            / "var"
            / "apps"
            / self.conf["apps"].get(app_id).get("name")
            / self.conf["apps"].get(app_id).get("version")
            / (Path("etc") if layer == "root" else Path("etc") / layer)
            / "containers"
            / "systemd"
            / f"{quadlet_name + '.container'}"
        )
        context = {
            "container_id": container_id,
            "app_id": app_id,
            "image_storage_path": (
                Path("/var")
                / "apps"
                / self.conf["apps"].get(app_id).get("name")
                / self.conf["apps"].get(app_id).get("version")
                / "containers"
                / "storage"
            ).as_posix(),
            "sign": SignedType(container_data.get("sign", "true")).value,
        }
        self.add_contents(src, dest, op=SourcesOp.ADD_QUADLET, context=context)
        return dest


class AppBuildSources(UserDict):
    def __init__(self, conf, app_id, build_id):
        super().__init__()
        self.conf = conf
        self.app_id = app_id
        self.build_id = build_id

        templates = self.conf["centos_sig"]["autosd_demo"]["templates"]

        self.data = {
            "directories": [
                (self.build_dir / "containers").as_posix(),
                (self.build_dir / "containers" / "storage").as_posix(),
            ],
            "templated_files": [
                {
                    "src": templates["containers"]["containers_conf_path"],
                    "dest": self.containers_conf_path.as_posix(),
                    "app_id": app_id,
                },
                {
                    "src": templates["containers"]["storage_conf_path"],
                    "dest": self.storage_conf_path.as_posix(),
                    "app_id": app_id,
                },
            ],
        }

    @property
    def is_cache_enabled(self):
        return self.conf.get("cache", {}).get("app_storage", False)

    @property
    def build_dir(self) -> Path:
        if self.is_cache_enabled:
            return Path("cache/apps") / self.app_id
        else:
            return Path(self.build_id) / ".apps" / self.app_id

    @property
    def containers_conf_path(self) -> Path:
        return self.build_dir / "containers" / "containers.conf"

    @property
    def storage_conf_path(self) -> Path:
        return self.build_dir / "containers" / "storage.conf"

    @property
    def templates(self):
        return self.conf["centos_sig"]["autosd_demo"]["templates"]["containers"]

    def add_directory(self, src: Path):
        if src.as_posix() not in self["directories"]:
            self["directories"].append(src.as_posix())
