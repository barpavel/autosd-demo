from collections import UserDict
from enum import Enum, auto
from pathlib import Path
from urllib.parse import urlparse

from jinja2 import Template

from autosd_demo.core.build.sources.exceptions import SourcesNotAllowedAction


class SourcesOp(Enum):
    ADD = auto()
    DELETE = auto()
    FETCH = auto()
    TOUCH = auto()
    ADD_QUADLET = auto()


class Sources(UserDict):
    def __init__(self, conf, is_remote_build: bool = False):
        super().__init__()
        self.conf = conf
        self.is_remote_build = is_remote_build
        self.data = {
            "delete": [],
            "directories": [],
            "files": [],
            "templated_files": [],
            "fetch_files": [],
        }

    def add_contents(
        self,
        src: Path | str | None,
        dest: Path,
        op: SourcesOp,
        context: dict = {},
    ):
        if isinstance(src, str) and op != SourcesOp.FETCH:
            src = Path(src)

        if isinstance(src, Path) and not src.is_absolute() and op != SourcesOp.FETCH:
            src = Path(self.conf["base_dir"]) / src

        contents = self.__get_contents_list_from_src(src, op)

        for file in contents:
            if file and op == SourcesOp.ADD:
                self.__add_regular_file_content(file, src, dest, context)
            elif op == SourcesOp.ADD_QUADLET:
                self.__add_quadlet(src, dest, context)
            elif op == SourcesOp.DELETE:
                self.__delete_content(dest, context)
            elif op == SourcesOp.TOUCH:
                self.__add_touch_content(dest, context)
            elif op == SourcesOp.FETCH:
                self.__add_fetch_content(file, dest, context)

    @staticmethod
    def __get_contents_list_from_src(src, op):
        if isinstance(src, Path):
            if (src.is_file() and op == SourcesOp.ADD) or (op == SourcesOp.ADD_QUADLET):
                contents = [src]
            elif src.is_dir() and op == SourcesOp.ADD:
                contents = [f for f in src.glob("**/*")]
            else:
                print(f"warning: {src} doesn't exists")
                contents = []
        elif isinstance(src, str) and op == SourcesOp.FETCH:
            contents = [urlparse(src)]
        else:
            if op in [SourcesOp.DELETE, SourcesOp.TOUCH]:
                contents = [src]
            else:
                raise SourcesNotAllowedAction()
        return contents

    def __add_regular_file_content(self, file, src, dest, context):
        path_template = Template(
            file.as_posix(),
            variable_start_string="{",
            variable_end_string="}",
        )
        file_src = Path(path_template.render(conf=self.conf))
        extra_files_data = context.get("extra_files_data", {})
        dest_trailing_slash = extra_files_data.get("dest", "").endswith("/")
        source_trailing_slash = extra_files_data.get("src", "").endswith("/")
        if src.is_file():
            if dest_trailing_slash:
                if src.suffix == ".j2":
                    file_dest = dest / src.with_suffix("").name
                else:
                    file_dest = dest / src.name
            else:
                file_dest = dest
        else:
            if not source_trailing_slash and dest_trailing_slash:
                # If source_trailing_slash == False
                #    dest_trailing_slash   == True
                # Then you are moving the folder not only the contents
                # For instance:
                #    src: test/assets and dest: /usr/local/share/
                #    result: /usr/local/share/assets/*.*
                if file_src.suffix == ".j2":
                    file_dest = (
                        dest / src.name / file_src.with_suffix("").relative_to(src)
                    )
                else:
                    file_dest = dest / src.name / file_src.relative_to(src)
            else:
                if file_src.suffix == ".j2":
                    file_dest = dest / file_src.with_suffix("").relative_to(src)
                else:
                    file_dest = dest / file_src.relative_to(src)

        if file.is_file():
            if file.suffix == ".j2":
                templated_file = {"src": file.as_posix(), "dest": file_dest.as_posix()}
                self["templated_files"].append(templated_file)
            else:
                regular_file = {"src": file.as_posix(), "dest": file_dest.as_posix()}
                self["files"].append(regular_file)

            if file_dest.parent.as_posix() not in self["directories"]:
                self["directories"].append(file_dest.parent.as_posix())
        else:
            if file_dest.as_posix() not in self["directories"]:
                self["directories"].append(file_dest.as_posix())

    def __add_quadlet(self, src, dest, context):
        templated_file = {
            "src": src.as_posix(),
            "dest": dest.as_posix(),
        }
        templated_file.update(context)
        self["templated_files"].append(templated_file)
        if dest.parent.as_posix() not in self["directories"]:
            self["directories"].append(dest.parent.as_posix())

    def __delete_content(self, dest, context):
        extra_files_data = context["extra_files_data"]
        layer = extra_files_data.get("layer", "root")
        self["delete"].append({"dest": dest.as_posix(), "layer": layer})

    def __add_touch_content(self, dest, context):
        extra_files_data = context["extra_files_data"]
        if extra_files_data["dest"].endswith("/"):
            self["directories"].append(dest.as_posix())
        else:
            self["files"].append({"dest": dest.as_posix()})
            self["directories"].append(dest.parent.as_posix())

    def __add_fetch_content(self, file, dest, context):
        url = file
        extra_files_data = context["extra_files_data"]
        cache_file = Path("cache") / "downloads" / url.hostname / url.path[1:]
        self["fetch_files"].append(
            {
                "url": url.geturl(),
                "checksum": extra_files_data.get("checksum", ""),
                "file": cache_file.as_posix(),
            }
        )
        self["directories"].append(cache_file.parent.as_posix())
        downloaded_file = {
            "src": cache_file.as_posix(),
            "dest": dest.as_posix(),
            "remote_src": self.is_remote_build,
        }

        if extra_files_data["dest"].endswith("/"):
            downloaded_file["dest"] = (dest / cache_file.name).as_posix()
            self["files"].append(downloaded_file)
            self["directories"].append(dest.as_posix())
        else:
            self["files"].append(downloaded_file)
            self["directories"].append(dest.parent.as_posix())
