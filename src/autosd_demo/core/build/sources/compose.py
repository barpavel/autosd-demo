from collections import UserDict
from pathlib import Path

from autosd_demo.core.build.sources.base import Sources, SourcesOp


class ComposeServiceSources(Sources):
    def __init__(self, conf, build_id, is_export=False):
        super().__init__(conf=conf)
        self.data.pop("fetch_files")
        self.data.pop("delete")
        self.build_id = build_id
        self.is_export = is_export

        self.data.update(
            {
                "directories": [
                    self.dest_dir.parent.as_posix(),
                    self.dest_dir.as_posix(),
                ]
            }
        )

    def add_container_files(self, src, service_id):
        self.add_contents(
            src,
            self.dest_dir / service_id,
            op=SourcesOp.ADD,
            context={
                "service_id": service_id,
            },
        )

    def add_extra_files(self, src: Path, dest: Path, op: SourcesOp):
        c_dest = self.data_dir / (dest.relative_to("/") if dest.is_absolute() else dest)

        self.add_contents(
            src,
            c_dest,
            op,
            context={},
        )

    def add_directory(self, src: Path):
        if src.as_posix() not in self["directories"]:
            self["directories"].append(src.as_posix())

    @property
    def dest_dir(self):
        return Path(self.build_id) / "src" / "compose" / "services"

    @property
    def data_dir(self):
        return (
            Path(self.build_id) / "src" / "compose"
            if self.is_export
            else Path("compose")
        )


class ComposeServiceBuildSources(UserDict):
    def __init__(self, conf, build_id):
        super().__init__()
        self.conf = conf
        self.build_id = build_id

        self.data = {
            "directories": [
                self.base_dir.as_posix(),
                (self.base_dir / "storage").as_posix(),
            ],
            "templated_files": [
                {
                    "src": self.templates["containers_conf_path"],
                    "dest": self.containers_conf_path.as_posix(),
                    "compose_containers_dir": self.base_dir.as_posix(),
                },
                {
                    "src": self.templates["storage_conf_path"],
                    "dest": self.storage_conf_path.as_posix(),
                    "compose_containers_dir": self.base_dir.as_posix(),
                },
            ],
        }

    @property
    def base_dir(self) -> Path:
        return Path("cache") / "compose" / "containers"

    @property
    def containers_conf_path(self) -> Path:
        return self.base_dir / "containers.conf"

    @property
    def storage_conf_path(self) -> Path:
        return self.base_dir / "storage.conf"

    @property
    def storage_dir(self) -> Path:
        return self.base_dir / "storage"

    @property
    def templates(self):
        return self.conf["centos_sig"]["autosd_demo"]["templates"]["compose"]
