from pathlib import Path

from autosd_demo.core.build.sources.base import Sources, SourcesOp
from autosd_demo.core.build.sources.exceptions import ImageSourcesNotAllowedAction


class ImageSources(Sources):
    def __init__(self, conf, build_src: Path, is_remote_build: bool = False):
        super().__init__(conf=conf, is_remote_build=is_remote_build)
        self.build_src_dir = build_src

    def reset(self):
        self.data = {
            "delete": [],
            "directories": [],
            "files": [],
            "templated_files": [],
            "fetch_files": [],
        }

    def add_extra_files(
        self, src: Path | str, dest: Path, op: SourcesOp, extra_files_data
    ):
        if op == SourcesOp.FETCH and isinstance(src, Path):
            raise ImageSourcesNotAllowedAction

        c_src = None if op in [SourcesOp.TOUCH, SourcesOp.DELETE] else src

        match op:
            case SourcesOp.ADD | SourcesOp.ADD_QUADLET | SourcesOp.TOUCH | SourcesOp.FETCH:
                c_dest = (
                    self.build_src_dir
                    / "files"
                    / self.conf["name"]
                    / "extra_files"
                    / f"{extra_files_data.get('layer', 'root')}_fs"
                    / (dest.relative_to("/") if dest.is_absolute() else dest)
                )
            case SourcesOp.DELETE:
                c_dest = dest if dest.is_absolute() else Path("/") / dest
            case _:
                raise ImageSourcesNotAllowedAction

        self.add_contents(
            c_src,
            c_dest,
            op,
            context={
                "extra_files_data": extra_files_data,
            },
        )

    def add_container_files(self, src: Path, container_id, container_data):
        dest = (
            self.build_src_dir
            / "files"
            / self.conf["name"]
            / "containers"
            / container_id
        )
        context = {
            "container_id": container_id,
            "container_data": container_data,
        }
        self.add_contents(src, dest, op=SourcesOp.ADD, context=context)

    def add_quadlet(self, container_id, container_data):
        centos_sig_templates = self.conf["centos_sig"]["autosd_demo"]["templates"]
        src = Path(centos_sig_templates["containers"]["quadlet"])
        quadlet_name = container_data.get("quadlet_name", container_id)
        dest = (
            self.build_src_dir
            / "files"
            / self.conf["name"]
            / "extra_files"
            / f"{container_data.get('layer', 'root')}_fs"
            / "etc"
            / "containers"
            / "systemd"
            / f"{quadlet_name + '.container'}"
        )
        context = {"container_id": container_id}
        self.add_contents(src, dest, op=SourcesOp.ADD_QUADLET, context=context)
