import getpass
import os
import sys
import tempfile
import time
from collections import OrderedDict
from io import StringIO
from pathlib import Path

import ansible
import click
import yaml
from ansible import constants as C
from ansible.cli.playbook import PlaybookCLI
from fabric import Connection
from invoke import AuthFailure, Config, Context
from invoke.exceptions import UnexpectedExit
from rich import print as rich_print
from rich.progress import (
    BarColumn,
    Progress,
    SpinnerColumn,
    TaskProgressColumn,
    TextColumn,
    TimeElapsedColumn,
)
from rich.status import Status

from autosd_demo.ansible.callbacks.click_output import (
    CallbackModule as ClickOutputCallback,
)
from autosd_demo.cli.exceptions import AnsibleError, BuilderError
from autosd_demo.core.build.app_spec import AppSpec
from autosd_demo.core.build.sources import (
    AppBuildSources,
    AppSources,
    ComposeServiceBuildSources,
    ComposeServiceSources,
    ImageSources,
)
from autosd_demo.core.osbuild import OSBuildConfig
from autosd_demo.settings import settings
from autosd_demo.utils.ansible import get_ansible_playbook_path_from_name


class AppInstallerExecutor:  # pragma: no cover
    def run_app_installer(self, app_file):
        app_path = Path(app_file)
        dest_app_path = Path("/tmp") / app_path.name
        app_file_size = app_path.stat().st_size
        green_check = "[green][[/green][green]🗸[/green][green]][/green]"
        spinner_col = SpinnerColumn(
            spinner_name="point",
            finished_text=green_check,
        )
        with Progress(
            spinner_col,
            TextColumn("[progress.description]{task.description}"),
            BarColumn(),
            TextColumn("[progress.percentage]{task.percentage:>3.0f}%"),
        ) as pb:
            task = pb.add_task(f"Uploading {app_path.name}", total=app_file_size)
            sftp = self.sftp()
            sftp.put(
                app_path.as_posix(),
                dest_app_path.as_posix(),
                callback=lambda transferred, total: pb.update(
                    task, completed=transferred
                ),
            )

        self.sudo(f"chmod a+x {dest_app_path.as_posix()}", hide="both")

        out_stream = StringIO()

        with self.sudo(
            f"{dest_app_path.as_posix()} install",
            warn=True,
            asynchronous=True,
            out_stream=out_stream,
            err_stream=out_stream,
        ) as install_process:
            prev_pos = 0
            has_to_end = False
            current_msg = None
            status = Status("", spinner="point")
            status.start()

            while not has_to_end:
                if install_process.runner.process_is_finished:
                    time.sleep(1)
                    has_to_end = True

                current_pos = out_stream.tell()
                if current_pos > prev_pos:
                    out_stream.seek(prev_pos)
                    lines = out_stream.read().splitlines()
                    out_stream.seek(current_pos, os.SEEK_SET)
                    for line in lines:
                        if line.startswith("[sudo]"):
                            continue
                        elif line.startswith("# "):
                            if current_msg:
                                rich_print(f"  {green_check} {current_msg}")
                            else:
                                rich_print("Installing the app")
                            current_msg = line[2:]
                            status.update(f"{line[2:]}")
                    prev_pos = current_pos
            rich_print(f"  {green_check} {current_msg}")
            status.stop()


class OSTreeExecutor:
    def __init__(self):
        self.repo = "."

    @property
    def refs(self) -> str:
        return self.run_ostree([f"--repo={self.repo}", "refs"], hide="out")

    def rev_parse(self, ref: str):
        return self.run_ostree([f"--repo={self.repo}", "rev-parse", ref], hide="out")

    @property
    def last_ref(self) -> str:
        return self.refs.strip().split("\n")[-1]

    @property
    def head(self) -> str:
        return self.rev_parse(self.last_ref)

    @property
    def parent(self) -> str:
        return self.rev_parse(f"{self.head}^")

    def generate_delta(self):
        head = self.head
        try:
            parent = self.parent
        except UnexpectedExit as e:  # pragma: no cover
            click.echo(f"Generate delta: skip commit {head}: {e}")
            return
        filename = f"{self.last_ref.replace('/', '-')}-{parent}-{head}.update"
        self.run_ostree(
            [
                "static-delta",
                "generate",
                f"--repo={self.repo}",
                "--inline",
                "--min-fallback-size=0",
                f"--from={parent}",
                f"--to={head}",
                f"--filename={filename}",
                head,
            ]
        )

    def run_ostree(self, ostree_args, **kwargs):
        return str(
            self.run(f"ostree {' '.join(ostree_args)}", **kwargs).stdout
        ).rstrip()


class BuilderExecutor:
    TOTAL_LINES_ESTIMATE = 1500

    def run_builder(self, command):
        with self.construct_bar() as pb:
            with self.run(command, **self.run_kwargs()) as process:
                estimated_total = self.TOTAL_LINES_ESTIMATE
                building = pb.add_task(
                    "Build image with automotive-image-builder",
                    total=estimated_total,
                    start=False,
                )
                while not process.runner.stdout:  # pragma: no cover
                    self._check_run_stderr(process.runner.stderr)
                prev_cnt = 0
                pb.start_task(building)
                while not pb.finished:  # pragma: no cover
                    self._check_run_stderr(process.runner.stderr)
                    lines_cnt = len(process.runner.stdout)
                    if prev_cnt == lines_cnt:
                        continue
                    new_line = process.runner.stdout[-1].rstrip()
                    if self.debug:
                        pb.print(new_line)
                    if new_line.endswith("failed"):
                        raise BuilderError(process.runner.stdout)
                    if new_line.endswith("finished successfully"):
                        # Finished the build, finish the progress bar.
                        pb.update(building, completed=estimated_total)
                        pb.stop()
                        break
                    # Reaching the total prematurely, increase the estimation.
                    if estimated_total * 0.995 <= lines_cnt:
                        estimated_total += 50
                        pb.update(building, total=estimated_total)
                    pb.update(building, completed=lines_cnt)
                    prev_cnt = lines_cnt

    def _check_run_stderr(self, stderr):
        if any("error" in line.lower() for line in stderr):  # pragma: no cover
            raise BuilderError(stderr)

    def construct_bar(self):
        spinner_col = SpinnerColumn(
            spinner_name="point",
            finished_text="[green][[/green][yellow]🗸[/yellow][green]][/green]",
        )
        text_col = TextColumn("[progress.description]{task.description}")
        return Progress(
            spinner_col,
            text_col,
            BarColumn(),
            TaskProgressColumn(),
            TimeElapsedColumn(),
        )

    def run_kwargs(self):
        run_kwargs = {
            "warn": True,
            "hide": "out",
            "asynchronous": True,
        }
        if self.debug:
            run_kwargs["echo"] = True
        return run_kwargs


class AnsibleExecutor:
    def run_playbook(self, playbook, tags=None, skip_tags=None, extra_vars={}):
        if not self.debug:
            C.DEFAULT_STDOUT_CALLBACK = ClickOutputCallback(status=self.status)

        with tempfile.TemporaryDirectory() as tmpdir:
            ansible_cmd = ["ansible-playbook"]
            if self.debug:
                ansible_cmd += ["-vv"]
            if isinstance(self, Connection):
                ansible_cmd += [
                    "-i",
                    f"{self.host},",
                    "-u",
                    self.user,
                    "-e",
                    f"ansible_ssh_port={self.port}",
                ]
            else:
                ansible_cmd += ["-i", "localhost,", "-e", "ansible_connection=local"]

            if tags:
                ansible_cmd += ["--tags", ",".join(tags)]

            if skip_tags:
                ansible_cmd += ["--skip-tags", ",".join(skip_tags)]

            if not self.host_key_check:
                ansible_cmd += [
                    "--ssh-common-args="
                    "-o StrictHostKeyChecking=no "
                    "-o UserKnownHostsFile=/dev/null"
                ]

            if extra_vars != {}:
                extra_vars_path = Path(tmpdir, "extra-vars.yaml")
                ansible_cmd += ["-e", f"@{extra_vars_path.as_posix()}"]

                with open(extra_vars_path, "w") as fd:

                    def representer_ordered_dict(obj, data):
                        return obj.represent_mapping(
                            "tag:yaml.org,2002:map", data.items()
                        )

                    yaml.add_representer(OrderedDict, representer_ordered_dict)
                    yaml.add_representer(ImageSources, representer_ordered_dict)
                    yaml.add_representer(AppSpec, representer_ordered_dict)
                    yaml.add_representer(AppSources, representer_ordered_dict)
                    yaml.add_representer(AppBuildSources, representer_ordered_dict)
                    yaml.add_representer(
                        ComposeServiceSources, representer_ordered_dict
                    )
                    yaml.add_representer(
                        ComposeServiceBuildSources, representer_ordered_dict
                    )
                    yaml.add_representer(OSBuildConfig, representer_ordered_dict)
                    yaml.dump(
                        dict(extra_vars), fd, default_flow_style=False, sort_keys=False
                    )

            playbook_path = get_ansible_playbook_path_from_name(playbook)
            ansible_cmd += [playbook_path.as_posix()]

            if hasattr(
                ansible.utils.vars.load_extra_vars, "extra_vars"
            ):  # pragma: no cover
                # Remove ansible extra_vars cache between executions
                del ansible.utils.vars.load_extra_vars.extra_vars

            cli = PlaybookCLI(ansible_cmd)

            # Return connection and become passwords tuple without prompting.
            cli.ask_passwords = lambda: (
                self.config["sudo"]["password"],
                self.config["sudo"]["password"],
            )
            cli.parse()
            result = cli.run()
            if self.status:
                self.status.stop()  # pragma: no cover
            if result != 0:
                raise AnsibleError(result, playbook)


def ensure_required_access(become_required=False, host_conf=None):
    """Return the credentials."""
    is_remote = host_conf is not None

    if is_remote:
        ctx = Connection(
            host=host_conf["address"],
            port=host_conf["port"],
            user=host_conf["user"],
            connect_kwargs=(
                {"password": host_conf["password"]} if "password" in host_conf else None
            ),
        )
    else:
        ctx = Context()

    if become_required:
        result = ctx.run("sudo -n true", hide="both", warn=True)
        if not result.ok:
            if host_conf and host_conf.get("password", None):
                sudo_pass = host_conf["password"]
            else:
                sudo_pass = click.prompt("Sudo password", hide_input=True)
            new_config = ctx.config
            new_config["sudo"]["password"] = sudo_pass
            ctx.config = new_config
            try:
                ctx.sudo("id", hide="both", warn=True)
            except AuthFailure:
                click.echo("sudo password is not valid. Abort!")
                sys.exit(1)

    if is_remote:
        ctx.close()

    return {"sudo": ctx.config["sudo"]["password"], "ssh": None}


def __init_executor__(self):
    if isinstance(self, Connection):
        # Remote build.
        host_conf = (
            settings.remote_host.get(self.host_id, {})
            if "remote_host" in settings
            else {}
        )
        host_conf.setdefault("address", self.host_id)
        host_conf.setdefault("port", 22)
        host_conf.setdefault("user", getpass.getuser())

        self.host_key_check = host_conf.get("host_key_check", self.host_key_check)

        credentials = ensure_required_access(
            become_required=self.become_required, host_conf=host_conf
        )
        new_config = Config()
        new_config["sudo"]["password"] = credentials["sudo"]
        Connection.__init__(
            self,
            host=host_conf["address"],
            config=new_config,
            port=host_conf["port"],
            user=host_conf["user"],
            connect_kwargs=(
                {"password": host_conf["password"]} if "password" in host_conf else None
            ),
        )
    else:
        credentials = ensure_required_access(become_required=self.become_required)
        new_config = Config()
        new_config["sudo"]["password"] = credentials["sudo"]
        Context.__init__(self, config=new_config)

    AnsibleExecutor.__init__(self)
    BuilderExecutor.__init__(self)
    OSTreeExecutor.__init__(self)
    AppInstallerExecutor.__init__(self)


def __enter_executor__(self):
    if isinstance(self, Connection):
        return Connection.__enter__(self)
    return self


def __exit_executor__(self, *exc):
    if isinstance(self, Connection):
        Connection.__exit__(self, *exc)


def get_click_param(param_name, click_context=None):
    if click_context is None:
        click_context = click.get_current_context()
    try:
        return click_context.params[param_name]
    except KeyError:
        if click_context.parent is not None:
            return get_click_param(param_name, click_context.parent)
        raise


def get_executor(host_id=None, become_required=False):
    context_class = Connection if host_id is not None else Context
    return type(
        "Executor",
        (
            context_class,
            AnsibleExecutor,
            BuilderExecutor,
            OSTreeExecutor,
            AppInstallerExecutor,
        ),
        {
            "__init__": __init_executor__,
            "__enter__": __enter_executor__,
            "__exit__": __exit_executor__,
            "host_id": host_id,
            "become_required": become_required,
            "status": Status("", spinner="point"),
            "debug": get_click_param("debug"),
            "host_key_check": True,
        },
    )
