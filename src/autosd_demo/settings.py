import os
from pathlib import Path

from dynaconf import Dynaconf
from dynaconf.utils import object_merge

from autosd_demo import (
    AUTOSD_DEMO_DEFAULT_CONF_PATH,
    AUTOSD_DEMO_DEFAULT_CONTAINERS_BUILD_ORDER,
    AUTOSD_DEMO_GLOBAL_CONF_FILE,
)
from autosd_demo.utils import find_project_settings, in_autosd_demo
from autosd_demo.utils.addons import AddonType, get_addons_metadata


class Settings(Dynaconf):  # type: ignore[misc]
    def __init__(self):
        if in_autosd_demo():
            project_settings_file = find_project_settings()
            addons_includes = self._get_addons_preload_includes(project_settings_file)
            compose_addons_includes = self._get_addons_preload_includes(
                project_settings_file, addon_type=AddonType.COMPOSE_ADDON
            )

            super().__init__(
                **self._init_params(
                    project_settings_file, addons_includes + compose_addons_includes
                ),
            )
            self._find_and_remove_omitted_items()
            self._enrich_apps_settings()
            self._enrich_container_settings()
            self.unset("GIT_RELEASE_VERSION")
        elif AUTOSD_DEMO_GLOBAL_CONF_FILE.exists():
            super().__init__(
                preload=[AUTOSD_DEMO_GLOBAL_CONF_FILE.as_posix()],
                environments=True,
            )
            # Leave only the remote build related information.
            keys_to_unset = [k for k in self.as_dict() if k.upper() != "REMOTE_HOST"]
            self.unset_all(keys_to_unset)
        else:
            super().__init__()

    @classmethod
    def includes(cls):
        return [
            p.as_posix()
            for p in (AUTOSD_DEMO_DEFAULT_CONF_PATH / "includes").iterdir()
            if p.suffix in [".toml", ".py"]
        ]

    @classmethod
    def preload(cls):
        return [
            p.as_posix()
            for p in (AUTOSD_DEMO_DEFAULT_CONF_PATH / "preload").iterdir()
            if p.suffix in [".toml", ".py"]
        ]

    @classmethod
    def _init_params(cls, settings_file, extra_preload=None):
        preload = cls.preload()
        if extra_preload:
            preload += extra_preload
        if AUTOSD_DEMO_GLOBAL_CONF_FILE.exists():
            preload.append(AUTOSD_DEMO_GLOBAL_CONF_FILE.as_posix())
        return {
            "root_path": settings_file.parent.as_posix(),
            "includes": sorted(cls.includes()),
            "preload": sorted(preload),
            "secrets": [".secrets.toml"],
            "merge_enabled": True,
            "environments": True,
            "env_switcher": "AUTOSD_DEMO_ENV",
            "envvar_prefix": "AUTOSD_DEMO",
            "settings_files": [settings_file.name],
        }

    @classmethod
    def _get_addons_preload_includes(cls, settings_file, addon_type=AddonType.ADDON):
        pre_settings = Dynaconf(**cls._init_params(settings_file))
        # addons_settings_key = "addons" | "compose_addons"
        addons_settings_key = addon_type.to_settings_key()
        addons_includes = []
        if "apps" in pre_settings and AddonType.ADDON:
            pre_settings[addons_settings_key] = ["validator"]
        if addons_settings_key in pre_settings:
            addons_metadata = get_addons_metadata(addon_type=addon_type)
            for addon_name in pre_settings[addons_settings_key]:
                if addon_name not in addons_metadata:
                    continue
                addon_config = (
                    Path(addons_metadata[addon_name]["base_dir"]) / "config.toml"
                )
                # Recursively look for nested addons
                nested_addons = cls._get_addons_preload_includes(
                    addon_config, addon_type=addon_type
                )
                if nested_addons:
                    addons_includes.extend(nested_addons)
                addons_includes.append(str(addon_config.absolute()))
        return addons_includes

    def _enrich_container_settings(self):
        containers = []
        if "containers" in self:
            containers = self["containers"].items()

        if "apps" in self:
            for app_name, app_data in self["apps"].items():
                if "containers" in app_data:
                    containers += self["apps"][app_name]["containers"].items()

        if len(containers) == 0:
            return

        for container_name, container_data in containers:
            layer = container_data.get("layer", "asil")
            if "app" in container_data:
                app_name = container_data["app"]["name"]
                default_base_image = (
                    self.get("apps", {})
                    .get(app_name, {})
                    .get(layer, {})
                    .get("base_image", None)
                )
            else:
                default_base_image = self.get(layer, {}).get("base_image", None)

            container_defaults = [
                ("image", container_name),
                ("version", "latest"),
                ("extra_args", ""),
                ("build_order", AUTOSD_DEMO_DEFAULT_CONTAINERS_BUILD_ORDER),
                ("base_image", default_base_image),
            ]
            for key, value in container_defaults:
                if key not in container_data:
                    container_data[key] = value

        for container_name, container_data in containers:
            if "build_args" in container_data:
                build_args = [
                    f"--build-arg {key}={value}"
                    for key, value in container_data["build_args"].items()
                ]
                if len(container_data["extra_args"]) > 0:
                    container_data["extra_args"] += " "
                container_data["extra_args"] += " ".join(build_args)

        for container_name, container_data in containers:
            container_systemd_defaults = [
                ("unit", "description", f"{container_name} container"),
                ("container", "container_name", container_name),
                ("install", "wanted_by", "multi-user.target"),
                ("service", "restart", "always"),
            ]

            for section, key, value in container_systemd_defaults:
                try:
                    if not container_data["systemd"][section][key]:
                        # A default overwritten by a value that is interpreted
                        # as False. Remove the key from the settings.
                        del container_data["systemd"][section][key]
                except KeyError:
                    systemd = container_data.setdefault("systemd", {})
                    systemd.setdefault(section, {})[key] = value

            image_components = []
            if "container_context" in container_data:
                image_components = [
                    "localhost",
                    container_data.get("layer", None),
                    "apps" if "app" in container_data else None,
                    container_data.get("app", {}).get("name", None),
                ]
            elif any(key in container_data for key in ["registry", "namespace"]):
                image_components = [
                    container_data.get("registry", None),
                    container_data.get("namespace", None),
                ]

            image_components.append(
                f"{container_data['image']}:" f"{container_data['version']}"
            )
            image_name = "/".join([c for c in image_components if c])

            container_data["systemd"]["container"]["image"] = image_name
            container_data["image_ref"] = image_name

        for _, container_data in containers:
            base_image = container_data.get("base_image", None)
            container_ids_list = [id for id, data in containers]

            match base_image:
                case "omit" | "" | None:
                    continue
                case base_image if base_image in container_ids_list:
                    base_image_data = next(
                        data for id, data in containers if id == base_image
                    )
                    base_image_ref = base_image_data["image_ref"]
                    base_image_build_order = base_image_data["build_order"]
                    build_arg = f" --build-arg BASE_IMAGE={base_image_ref}"
                    container_data["extra_args"] += build_arg
                    if container_data["build_order"] <= base_image_build_order:
                        container_data["build_order"] = base_image_build_order + 1
                case _:
                    build_arg = f" --build-arg BASE_IMAGE={base_image}"
                    container_data["extra_args"] += build_arg

        for _, container_data in containers:
            self._extends_container(container_data)

    def _extends_container(self, container_data):
        if isinstance(container_data.get("extends", None), str):
            container_data["extends"] = [container_data["extends"]]

        for extension in container_data.get("extends", []):
            if extension in self.get("container_extensions", []):
                extension_data = self["container_extensions"][extension]
                if extension_data.get("extends"):
                    self._extends_container(extension_data)
                object_merge(extension_data, container_data)

    def _find_and_remove_omitted_items(self):
        resources = [
            "apps",
            "containers",
            "dracut",
            "extra_files",
            "fs_perm",
            "hooks",
            "systemd",
            "registry_creds",
            "remote_host",
            "rpm",
            "rpm_repo",
        ]
        for resource in resources:
            for item_id in list(self.get(resource, {})):
                omit_val = self[resource][item_id].get("omit", False)
                if isinstance(omit_val, str):
                    omit_val = True if omit_val.lower() == "true" else False
                if omit_val:
                    self[resource].pop(item_id)
                elif "omit" in self[resource][item_id]:
                    self[resource][item_id].pop("omit")

    def _enrich_apps_settings(self):
        if "apps" not in self:
            return

        for app_name, app_data in self["apps"].items():
            app_defaults = [
                ("name", app_name),
                ("version", "0.0.1"),
                ("dest_dir", "apps"),
                ("required", []),
            ]
            for key, value in app_defaults:
                if key not in app_data:
                    self["apps"][app_name][key] = value

            if "containers" in app_data:
                for container_name, container_data in app_data["containers"].items():
                    container_data["app"] = {
                        "name": app_name,
                        "version": app_data["version"],
                    }

    def __delitem__(self, key):
        self.set(key, "@del")


settings = Settings()


def reload_settings(env=None):
    global settings
    if env is not None:
        os.environ["AUTOSD_DEMO_ENV"] = env
    settings = Settings()
