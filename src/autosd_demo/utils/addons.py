from enum import StrEnum, auto

from autosd_demo import AUTOSD_DEMO_DATA_PATH


class AddonType(StrEnum):
    ADDON = auto()
    COMPOSE_ADDON = auto()

    def to_relpath(self):
        match self:
            case self.ADDON:
                return "addons"
            case self.COMPOSE_ADDON:
                return "compose/addons"

    def to_settings_key(self):
        match self:
            case self.ADDON:
                return "addons"
            case self.COMPOSE_ADDON:
                return "compose_addons"


def get_addons_metadata(addon_type=AddonType.ADDON):
    addons_metadata = {}
    for addon_path in (AUTOSD_DEMO_DATA_PATH / addon_type.to_relpath()).iterdir():
        if (addon_path / "config.toml").is_file():
            addons_metadata[addon_path.name] = {"base_dir": str(addon_path.absolute())}

    return addons_metadata
