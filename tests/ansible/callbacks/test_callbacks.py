from unittest import mock
from unittest.mock import MagicMock

import pytest

from autosd_demo.ansible.callbacks import click_output
from autosd_demo.ansible.callbacks.click_output import (
    FILTERED_TASKS,
    ITEM_FORMAT_PATTERNS,
    StatusManager,
    TaskState,
)


def test_click_output_metadata():
    cb_mod = click_output.CallbackModule(status=MagicMock())
    assert cb_mod.CALLBACK_VERSION == 2.0
    assert cb_mod.CALLBACK_TYPE == "stdout"
    assert cb_mod.CALLBACK_NAME == "autosd.demo.click_output"
    assert cb_mod.CALLBACK_NEEDS_ENABLED is False


def test_click_output_on_failed_task():
    mock_status = MagicMock()
    callback_mod = click_output.CallbackModule()
    callback_mod.status = mock_status

    mock_task_result = MagicMock()
    mock_task_result._task.get_name.return_value = "test_task"

    callback_mod.v2_runner_on_failed(mock_task_result)
    callback_mod.status.update_message.assert_called_once_with(
        TaskState.FAILED, "test_task"
    )


def test_click_output_on_ok():
    mock_status = MagicMock()
    callback_mod = click_output.CallbackModule()
    callback_mod.status = mock_status

    mock_task_result = MagicMock()
    mock_task_result._result = {"changed": False}
    mock_task_result._task.get_name.return_value = "test_task"

    callback_mod.v2_runner_on_ok(mock_task_result)
    callback_mod.status.update_message.assert_called_once_with(
        TaskState.OK, "test_task"
    )


def test_click_output_on_ok_changed():
    mock_status = MagicMock()
    callback_mod = click_output.CallbackModule()
    callback_mod.status = mock_status

    mock_task_result = MagicMock()
    mock_task_result._result = {"changed": True}
    mock_task_result._task.get_name.return_value = "test_task"

    callback_mod.v2_runner_on_ok(mock_task_result)
    callback_mod.status.update_message.assert_called_once_with(
        TaskState.CHANGED, "test_task"
    )


def test_click_output_item_failed():
    mock_status = MagicMock()
    callback_mod = click_output.CallbackModule()
    callback_mod.status = mock_status

    mock_task_result = MagicMock()
    mock_task_result._result = {"changed": False}
    mock_task_result._task.get_name.return_value = "test_task"

    callback_mod.v2_runner_item_on_failed(mock_task_result)
    callback_mod.status.update_message.assert_called_once_with(
        TaskState.ITEM_FAILED, "test_task", None
    )


def test_click_output_item_on_ok():
    mock_status = MagicMock()
    callback_mod = click_output.CallbackModule()
    callback_mod.status = mock_status

    mock_task_result = MagicMock()
    mock_task_result._result = {"changed": False}
    mock_task_result._task.get_name.return_value = "test_task"

    callback_mod.v2_runner_item_on_ok(mock_task_result)
    callback_mod.status.update_message.assert_called_once_with(
        TaskState.ITEM_OK, "test_task", None
    )


def test_click_output_item_on_ok_changed():
    mock_status = MagicMock()
    callback_mod = click_output.CallbackModule()
    callback_mod.status = mock_status

    mock_task_result = MagicMock()
    mock_task_result._result = {"changed": True}
    mock_task_result._task.get_name.return_value = "test_task"

    callback_mod.v2_runner_item_on_ok(mock_task_result)
    callback_mod.status.update_message.assert_called_once_with(
        TaskState.ITEM_CHANGED, "test_task", None
    )


def test_click_output_on_task_start():
    mock_status = MagicMock()
    callback_mod = click_output.CallbackModule()
    callback_mod.status = mock_status

    mock_task_result = MagicMock()
    mock_task_result.get_name.return_value = "test_task"

    callback_mod.v2_playbook_on_task_start(mock_task_result, False)
    callback_mod.status.update_message.assert_called_once_with(
        TaskState.STARTED, "test_task"
    )


@pytest.mark.parametrize(
    "task_state,task_msg, expected_msg",
    [
        (TaskState.OK, "Test Message", "[green][🗸][/green] Test Message"),
        (
            TaskState.CHANGED,
            "Test Message",
            "[green][[/green][yellow]🗸[/yellow][green]][/green] Test Message",
        ),
        (TaskState.FAILED, "Test Message", "[red][■] Test Message[/red]"),
    ],
    ids=["TaskState.OK", "TaskState.CHANGED", "TaskState.FAILED"],
)
@mock.patch("rich.print")
def test_update_message_with_rich_print(
    mock_rich_print, task_state, task_msg, expected_msg
):
    status = MagicMock()
    manager = StatusManager(status)

    manager.update_message(task_state, task_msg)
    mock_rich_print.assert_called_once_with(expected_msg)

    # Check with items added
    manager.current_task_items = ["an item"]
    mock_rich_print.reset_mock()
    manager.update_message(task_state, task_msg)

    assert not mock_rich_print.called


@pytest.mark.parametrize(
    "task_state",
    [
        TaskState.OK,
        TaskState.CHANGED,
        TaskState.FAILED,
        TaskState.STARTED,
    ],
)
def test_update_message_with_a_filtered_msg(task_state):
    status = MagicMock()
    manager = StatusManager(status)

    manager.update_message(task_state, FILTERED_TASKS[0])


def test_update_message_without_status_enabled():
    manager = StatusManager(None)
    manager.update_message(TaskState.OK, "test_task")
    assert len(manager.current_task_items) == 0


@mock.patch("rich.print")
def test_update_messages_with_items_from_a_tasks(mock_rich_print):
    status = MagicMock()
    manager = StatusManager(status)

    manager.update_message(TaskState.STARTED, "test_task")
    assert manager.current_task == "test_task"
    status.update.assert_called_once_with(manager.current_task)

    # Test a TaskState.ITEM_OK
    manager.update_message(TaskState.ITEM_OK, "test_task", "an_item")
    assert len(manager.current_task_items) == 0

    # Test a TaskState.ITEM_OK
    manager.has_item_formatted_messages = MagicMock(return_value=True)
    manager.get_item_formatted_message = MagicMock(return_value="test_task item")
    manager.update_message(TaskState.ITEM_OK, "test_task", "an_item")
    assert len(manager.current_task_items) == 1
    assert manager.current_task_items[-1] == "[green][🗸][/green] test_task item"

    # Test a TaskState.ITEM_CHANGED
    manager.has_item_formatted_messages = MagicMock(return_value=True)
    manager.get_item_formatted_message = MagicMock(return_value="test_task item")
    manager.update_message(TaskState.ITEM_CHANGED, "test_task", "an_item")
    assert len(manager.current_task_items) == 2
    assert (
        manager.current_task_items[-1]
        == "[green][[/green][yellow]🗸[/yellow][green]][/green] test_task item"
    )

    # Test a TaskState.ITEM_FAILED
    manager.has_item_formatted_messages = MagicMock(return_value=True)
    manager.get_item_formatted_message = MagicMock(return_value="test_task item")
    manager.update_message(TaskState.ITEM_FAILED, "test_task", "an_item")
    assert len(manager.current_task_items) == 3
    assert manager.current_task_items[-1] == "[red][■] test_task item[/red]"


def test_item_formatted_messages():
    status = MagicMock()
    manager = StatusManager(status)

    # Test formated messages allowed
    assert manager.has_item_formatted_messages(ITEM_FORMAT_PATTERNS[0][0])
    f_msg = manager.get_item_formatted_message(ITEM_FORMAT_PATTERNS[0][0], "test")
    assert f_msg == 'Create directory: "$BUILD_DIR/test"'
    f_msg = manager.get_item_formatted_message(ITEM_FORMAT_PATTERNS[0][0], {})
    assert f_msg == 'Create directory: "$BUILD_DIR/{}"'

    # Test formated messages not allowed
    assert manager.has_item_formatted_messages("Fake msg") is False
    assert manager.get_item_formatted_message("Fake msg", {}) is None
