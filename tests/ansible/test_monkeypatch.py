from io import StringIO
from unittest.mock import MagicMock, patch

from autosd_demo.ansible.monkeypatch import (
    monkeypatch_ansible_display_sync,
    monkeypatch_ansible_display_sys_stdout_reconfiguration,
    monkeypatch_ansible_plugins_loader_module,
)


@patch("sys.stdout", new_callable=StringIO)
@patch("sys.stderr", new_callable=StringIO)
def test_ansible_monkeypatch_ansible_display_sys_stdout_reconfiguration(
    mock_stdout, mock_stderr
):
    assert not hasattr(mock_stdout, "reconfigure")
    assert not hasattr(mock_stderr, "reconfigure")

    monkeypatch_ansible_display_sys_stdout_reconfiguration()

    assert hasattr(mock_stdout, "reconfigure")
    assert hasattr(mock_stderr, "reconfigure")


@patch("ansible.utils.display.Display")
def test_ansible_monkeypatch_ansible_display_sync(mock_display):
    assert isinstance(mock_display._synchronize_textiowrapper, MagicMock)
    monkeypatch_ansible_display_sync()
    assert not isinstance(mock_display._synchronize_textiowrapper, MagicMock)


def test_ansible_monkey_patch_ansible_plugins_loader_module():
    import ansible.plugins.loader

    monkeypatch_ansible_plugins_loader_module()
    assert "monkeypatch_ansible_plugins_loader_module" in str(
        ansible.plugins.loader._configure_collection_loader
    )
