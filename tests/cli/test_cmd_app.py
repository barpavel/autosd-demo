from unittest.mock import MagicMock, patch

import pytest
from click.testing import CliRunner
from dynaconf.utils import DynaconfDict

from autosd_demo.cli import cli
from autosd_demo.cli.cmd_app import clean_settings_for_build_apps
from autosd_demo.cli.exceptions import AnsibleError
from autosd_demo.core import Playbooks


@patch("autosd_demo.cli.cmd_app.clean_settings_for_build_apps")
@patch("autosd_demo.cli.cmd_app.BuildEnv")
@patch("autosd_demo.cli.cmd_app.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_app_build_with_local_env(
    _, mock_executor, mock_build_env, mock_clean_settings
):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.hooks = {"pre_build": ["test.yaml"]}

    runner = CliRunner()
    runner.invoke(cli, ["app", "build"])

    mock_clean_settings.assert_called_once()
    mock_build_env.assert_called_once_with(None, build_id=None)
    mock_executor.assert_called_once_with(host_id=None, become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-apps", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-apps", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD,
        tags=["build-apps", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD,
        tags=["build-apps", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 4


@patch("autosd_demo.cli.cmd_app.clean_settings_for_build_apps")
@patch("autosd_demo.cli.cmd_app.BuildEnv")
@patch("autosd_demo.cli.cmd_app.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_app_build_with_error(_, mock_executor, mock_build_env, mock_clean_settings):
    side_effects = [None] * 5
    side_effects[2] = AnsibleError(1, Playbooks.PROVISION_BUILD)
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = side_effects
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["app", "build"])

    mock_clean_settings.assert_called_once()
    mock_build_env.assert_called_once_with(None, build_id=None)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-apps", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-apps", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    # Cleanup if prepare env fails
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD,
        tags=["build-apps", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 4
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_app.clean_settings_for_build_apps")
@patch("autosd_demo.cli.cmd_app.BuildEnv")
@patch("autosd_demo.cli.cmd_app.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_prepare_with_error_and_no_clean(
    _, mock_executor, mock_build_env, mock_clean_settings
):
    side_effects = [None] * 4
    side_effects[2] = AnsibleError(1, Playbooks.PROVISION_BUILD)
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = side_effects
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["app", "build", "--no-clean"])

    mock_clean_settings.assert_called_once()
    mock_build_env.assert_called_once_with(None, build_id=None)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-apps", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-apps", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 3
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


def test_clean_settings_for_build_apps_with_no_apps():
    conf = {"apps": {}}
    with patch("autosd_demo.cli.cmd_app.settings", DynaconfDict(conf)):
        with pytest.raises(SystemExit) as e:
            clean_settings_for_build_apps()
            assert isinstance(e, SystemExit)
            assert e.value.code == 0


def test_clean_settings_for_build_apps_with_disable_quadlets():
    conf = {
        "apps": {"foo": {"name": "foo"}},
        "containers": {
            "container1": {"quadlet": True},
            "container2": {"quadlet": False},
            "container3": {},
        },
    }
    with patch("autosd_demo.cli.cmd_app.settings", DynaconfDict(conf)) as mock_settings:
        clean_settings_for_build_apps()
        for containers in mock_settings.get("containers").values():
            assert containers["quadlet"] is False


def test_clean_settings_for_build_apps_with_extra_files():
    conf = {
        "apps": {"foo": {"name": "foo"}},
        "extra_files": ["foo", "bar"],
    }
    with patch("autosd_demo.cli.cmd_app.settings", DynaconfDict(conf)) as mock_settings:
        clean_settings_for_build_apps()
        assert "extra_files" not in mock_settings


def test_clean_settings_for_build_apps_with_one_app():
    conf = {
        "apps": {
            "foo": {"name": "foo"},
            "bar": {"name": "bar"},
        }
    }
    with patch("autosd_demo.cli.cmd_app.settings", DynaconfDict(conf)) as mock_settings:
        clean_settings_for_build_apps(["foo"])
        assert mock_settings.get("apps") == {"foo": {"name": "foo"}}
