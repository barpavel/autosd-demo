from unittest.mock import MagicMock, call, patch

from click.testing import CliRunner

from autosd_demo.cli import cli
from autosd_demo.cli.cmd_build import ExportType
from autosd_demo.cli.exceptions import AnsibleError
from autosd_demo.core import Playbooks


class AnyStringWith(str):
    def __eq__(self, other):
        return self in other


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=False)
def test_build_no_base_dir(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["build"])

    mock_build_env.assert_not_called()


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_with_local_env(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.hooks = {"pre_build": ["test.yaml"]}

    runner = CliRunner()
    runner.invoke(cli, ["build"])

    mock_build_env.assert_called_once_with(None, osbuild=True, build_id=None)
    mock_executor.assert_called_once_with(host_id=None, become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(Playbooks.DISABLE_SUDO_PASSWD, extra_vars={})
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD,
        extra_vars={},
    )
    assert mock_run_playbook.call_count == 6


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.settings.reload_settings", return_value=MagicMock())
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_with_multiple_env(
    _, mock_reload_settings, mock_executor, mock_build_env
):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.hooks = {"pre_build": ["test.yaml"]}

    runner = CliRunner()
    runner.invoke(cli, ["build", "-E", "v1", "-E", "v2"])

    mock_build_env.assert_any_call(None, osbuild=True, build_id=None)
    assert mock_build_env.call_count == 2
    mock_reload_settings.assert_has_calls([call(env="v1"), call(env="v2")])
    mock_executor.assert_called_once_with(host_id=None, become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(Playbooks.DISABLE_SUDO_PASSWD, extra_vars={})
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD,
        extra_vars={},
    )
    assert mock_run_playbook.call_count == 9


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_generate_deltas(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_generate_deltas = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.generate_delta = (
        mock_generate_deltas
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.hooks = {"pre_build": ["test.yaml"]}

    runner = CliRunner()

    runner.invoke(cli, ["build", "--generate-deltas"])
    mock_build_env.assert_called_once_with(None, osbuild=True, build_id=None)
    mock_executor.assert_called_once_with(host_id=None, become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_generate_deltas.assert_not_called()

    runner.invoke(cli, ["build", "--ostree-repo", "_ostree", "--generate-deltas"])

    mock_build_env.assert_called_with(None, osbuild=True, build_id=None)
    mock_executor.assert_called_with(host_id=None, become_required=True)
    mock_executor_ctx.assert_called_with()
    mock_generate_deltas.assert_called_once()


@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_prepare_error_enable_pw(_, mock_executor):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = AnsibleError(1, Playbooks.DISABLE_SUDO_PASSWD)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["build"])

    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(Playbooks.DISABLE_SUDO_PASSWD, extra_vars={})
    assert mock_run_playbook.call_count == 1
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_prepare_error_prepare_env(_, mock_executor, mock_build_env):
    side_effects = [None] * 5
    side_effects[2] = AnsibleError(1, Playbooks.PROVISION_BUILD)
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = side_effects
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["build"])

    mock_build_env.assert_called_once_with(None, osbuild=True, build_id=None)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(Playbooks.DISABLE_SUDO_PASSWD, extra_vars={})
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    # Cleanup if prepare env fails
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(Playbooks.ENABLE_SUDO_PASSWD, extra_vars={})
    assert mock_run_playbook.call_count == 5
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_prepare_error_prepare_env_no_clean(_, mock_executor, mock_build_env):
    side_effects = [None] * 4
    side_effects[2] = AnsibleError(1, Playbooks.PROVISION_BUILD)
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = side_effects
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["build", "--no-clean"])

    mock_build_env.assert_called_once_with(None, osbuild=True, build_id=None)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(Playbooks.DISABLE_SUDO_PASSWD, extra_vars={})
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    # Do not clean, but still disable nopassword when fail
    mock_run_playbook.assert_any_call(Playbooks.ENABLE_SUDO_PASSWD, extra_vars={})
    assert mock_run_playbook.call_count == 4
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_prepare_error_build_post_env_no_clean(_, mock_executor, mock_build_env):
    side_effects = [None] * 5
    side_effects[3] = AnsibleError(1, Playbooks.POST_BUILD)
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = side_effects
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    result = runner.invoke(cli, ["build", "--no-clean"])

    mock_build_env.assert_called_once_with(None, osbuild=True, build_id=None)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(Playbooks.DISABLE_SUDO_PASSWD, extra_vars={})
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    # Do not clean, but still disable nopassword when fail
    mock_run_playbook.assert_any_call(Playbooks.ENABLE_SUDO_PASSWD, extra_vars={})
    assert mock_run_playbook.call_count == 5
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_setup_with_remote_env(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = True

    runner = CliRunner()
    runner.invoke(cli, ["build", "-H", "foobar"])

    mock_executor.assert_called_once_with(host_id="foobar", become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(Playbooks.DISABLE_SUDO_PASSWD, extra_vars={})
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD,
        extra_vars={},
    )
    assert mock_run_playbook.call_count == 6


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_with_local_env_and_no_clean(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["build", "--no-clean"])

    mock_build_env.assert_called_once_with(None, osbuild=True, build_id=None)
    mock_executor.assert_called_once_with(host_id=None, become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(Playbooks.DISABLE_SUDO_PASSWD, extra_vars={})
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.POST_BUILD,
        tags=["build-image", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.ENABLE_SUDO_PASSWD,
        extra_vars={},
    )
    assert mock_run_playbook.call_count == 5


@patch("autosd_demo.cli.cmd_build.BuildEnv")
@patch("autosd_demo.cli.cmd_build.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_build_with_local_env_and_verbose(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run = MagicMock(return_value=0)
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor_ctx.return_value.__enter__.return_value.run_builder = mock_run
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    runner = CliRunner()
    runner.invoke(cli, ["build", "--verbose"])

    mock_build_env.assert_called_once_with(None, osbuild=True, build_id=None)
    mock_executor.assert_called_once_with(host_id=None, become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_run.assert_called_once_with(AnyStringWith("--verbose"))
    assert mock_run.call_count == 1


def test_export_type_extension():
    extensions_table = {
        ExportType.ABOOT: "images",
        ExportType.ABOOT_SIMG: "images",
        ExportType.CONTAINER: "tar",
        ExportType.EXT4: "ext4",
        ExportType.EXT4_SIMG: "ext4",
        ExportType.IMAGE: "img",
        ExportType.OSTREE_COMMIT: "repo",
        ExportType.OSTREE_OCI_IMAGE: "oci-archive",
        ExportType.QCOW2: "qcow2",
        ExportType.ROOTFS: None,
        ExportType.RPMLIST: "rpmlist",
        ExportType.SIMG: "img",
        ExportType.TAR: "tar",
    }
    for export in ExportType:
        assert export.to_ext() == extensions_table[export]
