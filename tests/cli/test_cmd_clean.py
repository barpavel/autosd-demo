from unittest.mock import MagicMock, patch

import pytest
from click.testing import CliRunner

from autosd_demo.cli import cli
from autosd_demo.core import Playbooks


@pytest.mark.parametrize("is_remote", (False, True))
@patch("autosd_demo.cli.cmd_clean.BuildEnv")
@patch("autosd_demo.cli.cmd_clean.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=False)
def test_clean_out_autosd_demo(_, mock_executor, mock_build_env, is_remote):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = is_remote

    (args, _) = get_args(is_remote)
    runner = CliRunner()
    result = runner.invoke(cli, args)
    assert result.exit_code == 1
    assert (
        "Command 'clean' is not allowed outside of autosd-demo project."
        in result.output
    )
    mock_build_env.assert_not_called()
    mock_executor.assert_not_called()
    mock_executor_ctx.assert_not_called()
    mock_run_playbook.assert_not_called()


@pytest.mark.parametrize("is_remote", (False, True))
@patch("autosd_demo.cli.cmd_clean.BuildEnv")
@patch("autosd_demo.cli.cmd_clean.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_clean_in_autosd_demo_with_clean_hooks(
    _, mock_executor, mock_build_env, is_remote
):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.hooks = {"clean": ["clean_pkgs.yaml"]}
    mock_build_env.return_value.is_remote = is_remote

    (args, host_id) = get_args(is_remote)
    runner = CliRunner()
    result = runner.invoke(cli, args)
    assert result.exit_code == 0
    assert not result.output
    mock_build_env.assert_called_once()
    mock_executor.assert_called_once_with(host_id=host_id, become_required=True)
    mock_executor_ctx.assert_called_once()
    mock_run_playbook.assert_called_once_with(
        Playbooks.CLEAN,
        tags=["clean", "untagged"],
        extra_vars=mock_build_env.return_value,
    )


def get_args(is_remote):
    args = ["clean"]
    host_id = None
    if is_remote:
        host_id = "foobar"
        args.extend(["-H", host_id])
    return args, host_id
