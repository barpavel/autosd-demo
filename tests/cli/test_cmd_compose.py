from unittest.mock import MagicMock, patch

from click.testing import CliRunner

from autosd_demo.cli import cli
from autosd_demo.core import Playbooks


@patch("autosd_demo.cli.cmd_compose.BuildEnv")
@patch("autosd_demo.cli.cmd_compose.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_compose_up(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.hooks = {"pre_build": ["test.yaml"]}

    runner = CliRunner()
    runner.invoke(cli, ["compose", "up"])

    mock_build_env.assert_called_once_with(None, build_id=None)
    mock_executor.assert_called_once_with(host_id=None, become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-compose", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-compose", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.COMPOSE,
        tags=["compose-up", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD,
        tags=["build-compose", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 4


@patch("autosd_demo.cli.cmd_compose.BuildEnv")
@patch("autosd_demo.cli.cmd_compose.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_compose_down(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    runner.invoke(cli, ["compose", "down"])

    mock_build_env.assert_called_once_with(None)
    mock_executor.assert_called_once_with(host_id=None)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(
        Playbooks.COMPOSE,
        tags=["compose-down", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 1
