from unittest.mock import MagicMock, patch

from click.testing import CliRunner

from autosd_demo.cli import cli
from autosd_demo.cli.exceptions import AnsibleError
from autosd_demo.core import Playbooks


@patch("autosd_demo.cli.cmd_export.BuildEnv")
@patch("autosd_demo.cli.cmd_export.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_export(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.conf = {"name": "mydemo", "version": "0.0.1"}

    runner = CliRunner()
    result = runner.invoke(cli, ["export"])

    mock_build_env.assert_called_once_with(None, osbuild=True, export=True)
    mock_executor.assert_called_once()
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-export", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-export", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD,
        tags=["build-export", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 3

    assert result.exit_code == 0


@patch("autosd_demo.cli.cmd_export.BuildEnv")
@patch("autosd_demo.cli.cmd_export.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_export_with_hooks(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock(return_value=0)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.conf = {"name": "mydemo", "version": "0.0.1"}
    mock_build_env.return_value.hooks = {"pre_build": ["test.yaml"]}

    runner = CliRunner()
    result = runner.invoke(cli, ["export"])

    mock_build_env.assert_called_once_with(None, osbuild=True, export=True)
    mock_executor.assert_called_once()
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-export", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.PROVISION_BUILD,
        tags=["build-export", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    mock_run_playbook.assert_any_call(
        Playbooks.CLEAN_BUILD,
        tags=["build-export", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 3

    assert result.exit_code == 0


@patch("autosd_demo.cli.cmd_export.BuildEnv")
@patch("autosd_demo.cli.cmd_export.get_executor")
@patch("autosd_demo.utils.in_autosd_demo", return_value=True)
def test_cmd_export_with_playbook_error(_, mock_executor, mock_build_env):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_run_playbook.side_effect = AnsibleError(1, Playbooks.PRE_BUILD)
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.conf = {"name": "mydemo", "version": "0.0.1"}

    runner = CliRunner()
    result = runner.invoke(cli, ["export"])

    mock_build_env.assert_called_once_with(None, osbuild=True, export=True)
    mock_executor.assert_called_once()
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_any_call(
        Playbooks.PRE_BUILD,
        tags=["build-export", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
    assert mock_run_playbook.call_count == 1
    assert result.exit_code == 1
    assert "Failed to run playbook" in result.output
