from unittest.mock import MagicMock, patch

import pytest
from click.testing import CliRunner

from autosd_demo.cli import cli
from autosd_demo.core import Playbooks


@pytest.mark.parametrize("operation", (None, "install", "upgrade", "uninstall"))
@patch("autosd_demo.cli.cmd_setup.BuildEnv")
@patch("autosd_demo.cli.cmd_setup.get_executor")
def test_setup_with_local_env(mock_executor, mock_build_env, operation):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = False

    args = ["setup"]
    if operation:
        args.extend(["--operation", operation])

    runner = CliRunner()
    runner.invoke(cli, args)

    mock_executor.assert_called_once_with(host_id=None, become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_called_once_with(
        Playbooks.SETUP,
        tags=["install", "untagged"],
        extra_vars=mock_build_env.return_value,
    )


@pytest.mark.parametrize("operation", (None, "install", "upgrade", "uninstall"))
@patch("autosd_demo.cli.cmd_setup.BuildEnv")
@patch("autosd_demo.cli.cmd_setup.get_executor")
def test_setup_with_remote_env(mock_executor, mock_build_env, operation):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx
    mock_build_env.return_value.is_remote = True

    args = ["setup", "-H", "foobar"]
    if operation:
        args.extend(["--operation", operation])

    runner = CliRunner()
    runner.invoke(cli, args)

    mock_executor.assert_called_once_with(host_id="foobar", become_required=True)
    mock_executor_ctx.assert_called_once_with()
    mock_run_playbook.assert_called_once_with(
        Playbooks.SETUP,
        tags=["install", "untagged"],
        extra_vars=mock_build_env.return_value,
    )
