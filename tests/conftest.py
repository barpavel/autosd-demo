import tempfile
from contextlib import chdir
from pathlib import Path

import pytest

from autosd_demo import AUTOSD_DEMO_GLOBAL_CONF_FILENAME


@pytest.fixture(scope="function")
def tmp_dir():
    with tempfile.TemporaryDirectory() as tmpdir:
        with chdir(tmpdir):
            yield Path(tmpdir)


# Autogenerate pytest fixtures per each workspace
# for integration testing

test_path = Path(__file__).parent
test_workspaces = test_path.joinpath("integration", "data", "workspaces")


for workspace in test_workspaces.iterdir():
    if workspace.is_dir():
        exec(
            f"""
import shutil
import sys
import os
@pytest.fixture(scope="module")
def {workspace.name}_ws():
    with tempfile.TemporaryDirectory() as tmpdir:
        with chdir(tmpdir):
            shutil.copytree("{workspace.as_posix()}",
                            tmpdir, dirs_exist_ok=True)
            yield Path(tmpdir)
            os.system("sudo rm -fr build/")
        """
        )


@pytest.fixture
def global_config(tmp_dir):
    global_dir = tmp_dir / "global"
    global_dir.mkdir()
    yield global_dir / AUTOSD_DEMO_GLOBAL_CONF_FILENAME
