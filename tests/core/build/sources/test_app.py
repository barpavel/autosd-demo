import unittest
from pathlib import Path
from unittest.mock import patch

from autosd_demo.core.build.sources import AppSources
from autosd_demo.core.build.sources.app import AppBuildSources, SignedType
from autosd_demo.core.build.sources.base import SourcesOp


class TestAppSources(unittest.TestCase):
    def setUp(self):
        self.conf = {
            "name": "test_build",
            "centos_sig": {
                "autosd_demo": {
                    "templates": {
                        "containers": {"quadlet": "/path/to/quadlet/template"}
                    }
                }
            },
            "apps": {
                "app1": {"name": "app1", "version": "1.0"},
            },
        }
        self.build_src = Path("<build_id>/src")
        self.app_sources = AppSources(conf=self.conf, build_src=self.build_src)

    @patch.object(AppSources, "add_contents")
    def test_add_container_files(self, mock_add_contents):
        build_src_dir = (
            self.app_sources.build_src_dir
            / "files"
            / self.app_sources.conf["name"]
            / "containers"
            / "alpha"
        )
        self.app_sources.add_container_files(
            src=Path("/source/path"),
            container_id="alpha",
            container_data={
                "image": "alpha",
                "layer": "qm",
                "build": {"src_dir": build_src_dir},
            },
        )

        mock_add_contents.assert_called_once_with(
            Path("/source/path"),
            build_src_dir,
            op=SourcesOp.ADD,
            context={
                "container_id": "alpha",
                "container_data": {
                    "image": "alpha",
                    "layer": "qm",
                    "build": {"src_dir": build_src_dir},
                },
            },
        )

    @patch.object(AppSources, "add_contents")
    def test_add_quadlet(self, mock_add_contents):
        dist_dir = Path("<build_id>/app1/dist")
        container_context = {
            "image": "alpha",
            "layer": "qm",
            "build": {"dist_dir": dist_dir},
        }
        self.app_sources.add_quadlet("app1", "alpha", container_context)
        expected_dest = (
            dist_dir
            / "var"
            / "apps"
            / "app1"
            / "1.0"
            / "etc"
            / "qm"
            / "containers"
            / "systemd"
            / "alpha.container"
        )
        mock_add_contents.assert_called_once_with(
            Path("/path/to/quadlet/template"),
            expected_dest,
            op=SourcesOp.ADD_QUADLET,
            context={
                "container_id": "alpha",
                "app_id": "app1",
                "image_storage_path": "/var/apps/app1/1.0/containers/storage",
                "sign": "true",
            },
        )


class TestAppBuildSources(unittest.TestCase):
    def setUp(self):
        self.mock_conf = {
            "centos_sig": {
                "autosd_demo": {
                    "templates": {
                        "containers": {
                            "containers_conf_path": "path/to/containers.conf",
                            "storage_conf_path": "path/to/storage.conf",
                        }
                    }
                }
            },
            "cache": {"app_storage": False},
        }
        self.app_build_sources = AppBuildSources(self.mock_conf, "app1", "<build_id>")

    def test_initialization(self):
        self.assertEqual(self.app_build_sources.conf, self.mock_conf)
        self.assertEqual(self.app_build_sources.app_id, "app1")
        self.assertEqual(self.app_build_sources.build_id, "<build_id>")
        self.assertIn("directories", self.app_build_sources)
        self.assertIn("templated_files", self.app_build_sources)

    def test_is_cache_enabled(self):
        self.assertFalse(self.app_build_sources.is_cache_enabled)

    def test_build_dir_no_cache(self):
        expected_dir = Path("<build_id>/.apps/app1")
        self.assertEqual(self.app_build_sources.build_dir, expected_dir)

    def test_build_dir_with_cache(self):
        self.mock_conf["cache"]["app_storage"] = True
        sources_with_cache = AppBuildSources(self.mock_conf, "app1", "<build_id>")
        expected_dir = Path("cache/apps/app1")
        self.assertEqual(sources_with_cache.build_dir, expected_dir)

    def test_containers_conf_path(self):
        expected_path = Path("<build_id>/.apps/app1/containers/containers.conf")
        self.assertEqual(self.app_build_sources.containers_conf_path, expected_path)

    def test_storage_conf_path(self):
        expected_path = Path("<build_id>/.apps/app1/containers/storage.conf")
        self.assertEqual(self.app_build_sources.storage_conf_path, expected_path)

    def test_templates_property(self):
        expected_templates = {
            "containers_conf_path": "path/to/containers.conf",
            "storage_conf_path": "path/to/storage.conf",
        }
        self.assertEqual(self.app_build_sources.templates, expected_templates)

    def test_add_directory(self):
        new_dir = Path("/new/directory")
        self.app_build_sources.add_directory(new_dir)
        self.assertIn(new_dir.as_posix(), self.app_build_sources["directories"])

    def test_add_existing_directory(self):
        new_dir = Path("/new/directory")
        self.app_build_sources.add_directory(new_dir)
        self.assertIn(new_dir.as_posix(), self.app_build_sources["directories"])
        self.app_build_sources.add_directory(new_dir)
        self.assertIn(new_dir.as_posix(), self.app_build_sources["directories"])


class TestSignedType(unittest.TestCase):
    def test_true_from_str(self):
        assert SignedType("True") == SignedType.TRUE
        assert SignedType("yes") == SignedType.TRUE

    def test_false_from_str(self):
        assert SignedType("False") == SignedType.FALSE
        assert SignedType("no") == SignedType.FALSE

    def test_invalid_from_str(self):
        assert SignedType("invalid") == SignedType.INVALID
        assert SignedType("unknown") == SignedType.INVALID

    def test_true_from_bool(self):
        assert SignedType(True) == SignedType.TRUE

    def test_false_from_bool(self):
        assert SignedType(False) == SignedType.FALSE
