import unittest
from pathlib import Path
from unittest.mock import patch

from autosd_demo.core.build.sources import (
    ComposeServiceBuildSources,
    ComposeServiceSources,
    SourcesOp,
)


class TestComposeServiceSources(unittest.TestCase):
    def setUp(self):
        self.conf = {
            "name": "test_build",
        }
        self.cs_sources = ComposeServiceSources(conf=self.conf, build_id="<build_id>")

    @patch.object(ComposeServiceSources, "add_contents")
    def test_add_container_files(self, mock_add_contents):
        self.cs_sources.add_container_files(
            src=Path("/source/path"),
            service_id="alpha",
        )

        mock_add_contents.assert_called_once_with(
            Path("/source/path"),
            Path("<build_id>/src/compose/services/alpha"),
            op=SourcesOp.ADD,
            context={"service_id": "alpha"},
        )

    @patch.object(ComposeServiceSources, "add_contents")
    def test_add_add_extra_files(self, mock_add_contents):
        self.cs_sources.add_extra_files(
            src=Path("/source/path"),
            dest=Path("/dest/path"),
            op=SourcesOp.ADD,
        )

        mock_add_contents.assert_called_once_with(
            Path("/source/path"),
            Path("compose/dest/path"),
            SourcesOp.ADD,
            context={},
        )

    @patch.object(ComposeServiceSources, "add_contents")
    def test_add_add_extra_files_with_export(self, mock_add_contents):
        self.cs_sources.is_export = True
        self.cs_sources.add_extra_files(
            src=Path("/source/path"),
            dest=Path("/dest/path"),
            op=SourcesOp.ADD,
        )
        mock_add_contents.assert_called_once_with(
            Path("/source/path"),
            Path("<build_id>/src/compose/dest/path"),
            SourcesOp.ADD,
            context={},
        )


class TestComposeServiceBuildSources(unittest.TestCase):
    def setUp(self):
        self.conf = {
            "centos_sig": {
                "autosd_demo": {
                    "templates": {
                        "compose": {
                            "containers_conf_path": "/path/to/containers_conf",
                            "storage_conf_path": "/path/to/storage_conf",
                        }
                    }
                }
            }
        }
        self.build_id = "<build_id>"
        self.compose_service = ComposeServiceBuildSources(self.conf, self.build_id)

    def test_compose_service_build_sources_init(self):
        self.assertEqual(self.compose_service.conf, self.conf)
        self.assertEqual(self.compose_service.build_id, self.build_id)
        self.assertEqual(
            self.compose_service.data["directories"],
            [
                self.compose_service.base_dir.as_posix(),
                (self.compose_service.base_dir / "storage").as_posix(),
            ],
        )

    def test_compose_service_build_sources_base_dir(self):
        self.assertEqual(
            self.compose_service.base_dir, Path("cache") / "compose" / "containers"
        )

    def test_compose_service_build_sources_containers_conf_path(self):
        self.assertEqual(
            self.compose_service.containers_conf_path,
            self.compose_service.base_dir / "containers.conf",
        )

    def test_compose_service_build_sources_storage_conf_path(self):
        self.assertEqual(
            self.compose_service.storage_conf_path,
            self.compose_service.base_dir / "storage.conf",
        )

    def test_compose_service_build_sources_templates(self):
        expected_templates = {
            "containers_conf_path": "/path/to/containers_conf",
            "storage_conf_path": "/path/to/storage_conf",
        }
        self.assertEqual(self.compose_service.templates, expected_templates)
