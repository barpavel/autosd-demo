import unittest
from pathlib import Path
from unittest.mock import patch

from autosd_demo.core.build.sources import ImageSources
from autosd_demo.core.build.sources.base import SourcesOp
from autosd_demo.core.build.sources.exceptions import ImageSourcesNotAllowedAction


class TestImageSources(unittest.TestCase):
    def setUp(self):
        self.conf = {
            "name": "test_build",
            "centos_sig": {
                "autosd_demo": {
                    "templates": {
                        "containers": {"quadlet": "/path/to/quadlet/template"}
                    }
                }
            },
        }
        self.build_src = Path("/build/src")
        self.image_sources = ImageSources(conf=self.conf, build_src=self.build_src)

    def test_reset(self):
        self.image_sources.reset()
        self.assertEqual(
            self.image_sources.data,
            {
                "delete": [],
                "directories": [],
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            },
        )

    def test_add_extra_files_fetch_with_path_src_raises_exception(self):
        with self.assertRaises(ImageSourcesNotAllowedAction):
            self.image_sources.add_extra_files(
                src=Path("/some/path"),
                dest=Path("/destination/path"),
                op=SourcesOp.FETCH,
                extra_files_data={},
            )

    @patch.object(ImageSources, "add_contents")
    def test_add_extra_files_add_operation(self, mock_add_contents):
        self.image_sources.add_extra_files(
            src="source/path",
            dest=Path("/destination/path"),
            op=SourcesOp.ADD,
            extra_files_data={"layer": "qm"},
        )
        expected_dest = (
            self.image_sources.build_src_dir
            / "files"
            / self.image_sources.conf["name"]
            / "extra_files"
            / "qm_fs"
            / "destination"
            / "path"
        )
        mock_add_contents.assert_called_once_with(
            "source/path",
            expected_dest,
            SourcesOp.ADD,
            context={"extra_files_data": {"layer": "qm"}},
        )

    @patch.object(ImageSources, "add_contents")
    def test_add_container_files(self, mock_add_contents):
        self.image_sources.add_container_files(
            src=Path("/source/path"),
            container_id="alpha",
            container_data={"image": "alpha", "layer": "qm"},
        )
        expected_dest = (
            self.image_sources.build_src_dir
            / "files"
            / self.image_sources.conf["name"]
            / "containers"
            / "alpha"
        )
        mock_add_contents.assert_called_once_with(
            Path("/source/path"),
            expected_dest,
            op=SourcesOp.ADD,
            context={
                "container_id": "alpha",
                "container_data": {"image": "alpha", "layer": "qm"},
            },
        )

    @patch.object(ImageSources, "add_contents")
    def test_add_quadlet(self, mock_add_contents):
        self.image_sources.add_quadlet(
            container_id="alpha", container_data={"image": "alpha", "layer": "qm"}
        )
        expected_dest = (
            self.image_sources.build_src_dir
            / "files"
            / self.image_sources.conf["name"]
            / "extra_files"
            / "qm_fs"
            / "etc"
            / "containers"
            / "systemd"
            / "alpha.container"
        )
        mock_add_contents.assert_called_once_with(
            Path("/path/to/quadlet/template"),
            expected_dest,
            op=SourcesOp.ADD_QUADLET,
            context={"container_id": "alpha"},
        )

    @patch.object(ImageSources, "add_contents")
    def test_add_extra_files_delete_operation(self, mock_add_contents):
        self.image_sources.add_extra_files(
            src=None,
            dest=Path("/destination/path"),
            op=SourcesOp.DELETE,
            extra_files_data={"layer": "qm"},
        )
        expected_dest = Path("/destination/path")
        mock_add_contents.assert_called_once_with(
            None,
            expected_dest,
            SourcesOp.DELETE,
            context={"extra_files_data": {"layer": "qm"}},
        )

    @patch.object(ImageSources, "add_contents")
    def test_add_extra_files_delete_operation_with_relative_src(
        self, mock_add_contents
    ):
        self.image_sources.add_extra_files(
            src=None,
            dest=Path("destination/path"),
            op=SourcesOp.DELETE,
            extra_files_data={"layer": "qm"},
        )
        expected_dest = Path("/destination/path")
        mock_add_contents.assert_called_once_with(
            None,
            expected_dest,
            SourcesOp.DELETE,
            context={"extra_files_data": {"layer": "qm"}},
        )

    def test_add_extra_files_none_op(self):
        with self.assertRaises(ImageSourcesNotAllowedAction):
            self.image_sources.add_extra_files(
                src=Path("/some/path"),
                dest=Path("/destination/path"),
                op=None,
                extra_files_data={},
            )
