from pathlib import Path
from unittest.mock import MagicMock, patch

import pytest

from autosd_demo import (
    AUTOSD_DEMO_DEFAULT_CONTAINERS_BUILD_ORDER as DEFAULT_BUILD_ORDER,
)
from autosd_demo.cli.exceptions import ValidationError
from autosd_demo.core.build import BuildEnv
from autosd_demo.core.build.sources import ImageSources
from autosd_demo.core.osbuild import OSBuildConfig


class TestBuildEnv(BuildEnv):
    def __init__(self, conf=None, osbuild=False, build_id="<build_id>", export=False):
        super(BuildEnv, self).__init__()
        self.export = export

        if conf is None:
            conf = {"name": "mydemo"}

        self.update(
            {
                "build": {
                    "build_id": build_id,
                    "src": ImageSources(conf=conf, build_src=Path(build_id) / "src"),
                }
            }
        )

        if osbuild:
            self.update({"osbuild": OSBuildConfig(self.img_sources)})


@pytest.mark.parametrize(
    "settings_data, expected",
    [
        (
            {
                "remote_host": {
                    "foo": {"base_dir": "/home/foo", "build_dir": "/home/foo/build"}
                }
            },
            {"base_dir": "/home/foo", "build_dir": "/home/foo/build"},
        ),
        (
            {
                "remote_host": {
                    "foo": {
                        "base_dir": "/home/foo",
                    }
                }
            },
            {"base_dir": "/home/foo", "build_dir": "/home/foo/build"},
        ),
        ({"remote_host": {"foo": {}}}, {"base_dir": "~", "build_dir": "~/build"}),
        ({}, {"base_dir": "~", "build_dir": "~/build"}),
    ],
)
@patch("autosd_demo.core.build.base.settings")
@patch("autosd_demo.core.build.env.in_autosd_demo", return_value=True)
def test_get_build_env_config_with_remote_env(
    _, mock_settings, settings_data, expected
):
    mock_settings.as_dict.return_value = settings_data
    result = BuildEnv(host="foo")
    assert "conf" in result
    assert "build" in result
    assert result["build"]["base_dir"] == expected["base_dir"]
    assert result["build"]["build_dir"] == expected["build_dir"]


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, host, expected_cache",
    [
        (
            {
                "name": "demo",
                "build_dir": "~/src/build",
                "cache": {
                    "aib_mpp": True,
                    "aib_build": True,
                },
            },
            None,
            [
                "~/src/build/cache/aib/build",
                "~/src/build/cache/aib/mpp",
                "~/src/build/id/src",
                "~/src/build/id/auth.json",
            ],
        ),
        (
            {
                "name": "demo",
                "build_dir": "~/src/build",
                "cache": {
                    "aib_mpp": False,
                    "aib_build": False,
                },
            },
            None,
            [
                "~/src/build/id/.aib/build",
                "~/src/build/id/.aib/mpp",
                "~/src/build/id/src",
                "~/src/build/id/auth.json",
            ],
        ),
        (
            {
                "name": "demo",
                "build_dir": "~/src/build",
                "cache": {
                    "aib_mpp": True,
                    "aib_build": True,
                },
            },
            "host",
            [
                "~/build/cache/aib/build",
                "~/build/cache/aib/mpp",
                "~/build/id/src",
                "~/build/id/auth.json",
            ],
        ),
        (
            {
                "name": "demo",
                "build_dir": "~/src/build",
                "cache": {
                    "aib_mpp": False,
                    "aib_build": False,
                },
            },
            "host",
            [
                "~/build/id/.aib/build",
                "~/build/id/.aib/mpp",
                "~/build/id/src",
                "~/build/id/auth.json",
            ],
        ),
    ],
)
def test_build_env_with_cache(mock_settings, conf, host, expected_cache):
    mock_settings.as_dict.return_value = conf
    test = BuildEnv(host=host)
    test["build"].update(
        {
            "build_id": "id",
        }
    )
    assert test.build_cache_dir.as_posix() == expected_cache[0]
    assert test.mpp_cache_dir.as_posix() == expected_cache[1]
    assert test.build_src_dir.as_posix() == expected_cache[2]


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, host, expected",
    [
        (
            {
                "name": "demo",
                "build_dir": "~/src/build",
                "cache": {
                    "containers_storage": True,
                },
            },
            None,
            "~/src/build/cache/containers",
        ),
        (
            {
                "name": "demo",
                "build_dir": "~/src/build",
                "cache": {
                    "containers_storage": False,
                },
            },
            None,
            "~/src/build/id/.containers",
        ),
        (
            {
                "name": "demo",
                "build_dir": "~/src/build",
                "cache": {
                    "containers_storage": True,
                },
            },
            "host",
            "~/build/cache/containers",
        ),
        (
            {
                "name": "demo",
                "build_dir": "~/src/build",
                "cache": {
                    "containers_storage": False,
                },
            },
            "host",
            "~/build/id/.containers",
        ),
    ],
)
def test_build_env_containes_conf(mock_settings, conf, host, expected):
    mock_settings.as_dict.return_value = conf
    test = BuildEnv(host=host)
    test["build"].update(
        {
            "build_id": "id",
        }
    )
    assert test.storage_conf == Path(expected, "storage.conf")
    assert test.containers_conf == Path(expected, "containers.conf")


@patch("autosd_demo.core.build.base.settings")
@patch("autosd_demo.core.build.env.in_autosd_demo", return_value=False)
def test_get_build_env_config_with_no_env(_, mock_settings):
    mock_settings.as_dict.return_value = {}
    build_env = BuildEnv(host=None)
    assert "base_dir" not in build_env["build"]
    assert "build_dir" not in build_env["build"]
    assert build_env["build"]["is_remote"] is False


@patch("autosd_demo.core.build.base.settings")
@patch("autosd_demo.core.build.env.in_autosd_demo", return_value=True)
def test_get_build_env_config_with_local_env(_, mock_settings):
    mock_settings.as_dict.return_value = {
        "base_dir": "~/src",
        "build_dir": "~/src/build",
    }
    build_env = BuildEnv(host=None)
    assert build_env["conf"]["base_dir"] == "~/src"
    assert build_env["conf"]["build_dir"] == "~/src/build"
    assert build_env["build"]["base_dir"] == "~/src"
    assert build_env["build"]["build_dir"] == "~/src/build"
    assert build_env["build"]["is_remote"] is False


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected_cache_size",
    [
        ({"cache": {}}, "2GB"),
        (
            {
                "cache": {
                    "aib_max_size": "2GB",
                },
            },
            "2GB",
        ),
        (
            {
                "cache": {
                    "aib_max_size": "3GB",
                },
            },
            "3GB",
        ),
    ],
)
def test_build_env_with_cache_size(mock_settings, conf, expected_cache_size):
    mock_settings.as_dict.return_value = conf
    test = BuildEnv(host=None)
    assert test.max_cache_size == expected_cache_size


@patch("autosd_demo.core.build.base.settings")
def test_reload_src_contents(mock_settings):
    conf = {
        "name": "demo",
        "centos_sig": {
            "autosd_demo": {
                "templates": {"containers": {"quadlet": "/test/systemd.j2"}}
            }
        },
        "base_dir": "path/to/base_dir",
        "containers": {
            "alpha": {"image": "alpha", "build_order": DEFAULT_BUILD_ORDER},
            "beta": {
                "image": "beta",
                "container_context": "containers/beta",
                "build_order": DEFAULT_BUILD_ORDER,
            },
            "gamma": {"image": "alpha", "quadlet": False, "build_order": 10},
        },
    }
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv(conf=conf)
    test._process_containers()

    expected = {
        "src": {
            "delete": [],
            "directories": [
                "<build_id>/src/files/demo/extra_files/root_fs/etc/containers/systemd"
            ],
            "files": [],
            "templated_files": [
                {
                    "src": "/test/systemd.j2",
                    "dest": (
                        "<build_id>/src/files/demo/extra_files/root_fs/"
                        "etc/containers/systemd/alpha.container"
                    ),
                    "container_id": "alpha",
                },
                {
                    "src": "/test/systemd.j2",
                    "dest": (
                        "<build_id>/src/files/demo/extra_files/root_fs/"
                        "etc/containers/systemd/beta.container"
                    ),
                    "container_id": "beta",
                },
            ],
            "fetch_files": [],
        },
        "build_id": "<build_id>",
        "containers": [
            {"id": "gamma", "image": "alpha", "quadlet": False, "build_order": 10},
            {"id": "alpha", "image": "alpha", "build_order": DEFAULT_BUILD_ORDER},
            {
                "container_context": "containers/beta",
                "id": "beta",
                "image": "beta",
                "build_order": DEFAULT_BUILD_ORDER,
            },
        ],
    }
    assert test["build"] == expected
    test.reload_src_contents()
    assert test["build"] == expected


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf",
    [
        (
            {
                "name": "mydemo",
                "extra_files": {"test": {"src": "data/assets"}},
            }
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {"test": {"dest": "usr/share/demo"}},
            }
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test": {"dest": "usr/share/demo", "delete": True, "touch": True}
                },
            }
        ),
    ],
)
def test_get_build_src_extra_files_invalid_conf(mock_settings, conf):
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    test._reset_build_src_contents()

    with pytest.raises(ValidationError):
        test._process_extra_files()


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, extra_files, expected_result",
    [
        (
            {},
            [],
            {
                "delete": [],
                "directories": [],
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test": {"src": "data/assets", "dest": "usr/share/demo"}
                },
            },
            [
                "data/assets/text1.txt",
                "data/assets/text2.txt.j2",
            ],
            {
                "delete": [],
                "directories": [
                    "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo",
                ],
                "files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/text1.txt"
                        ),
                        "src": "data/assets/text1.txt",
                    }
                ],
                "templated_files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/text2.txt"
                        ),
                        "src": "data/assets/text2.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "data/assets/text1.txt",
                        "dest": "usr/share/demo/",
                        "layer": "qm",
                    },
                    "test2": {
                        "src": "data/assets/text1.txt",
                        "dest": "usr/share/text1.txt",
                        "layer": "qm",
                    },
                },
            },
            ["data/assets/text1.txt"],
            {
                "delete": [],
                "directories": [
                    "<build_id>/src/files/mydemo/extra_files/qm_fs/usr/share/demo",
                    "<build_id>/src/files/mydemo/extra_files/qm_fs/usr/share",
                ],
                "files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/qm_fs/usr/share/demo/text1.txt"
                        ),
                        "src": "data/assets/text1.txt",
                    },
                    {
                        "dest": "<build_id>/src/files/mydemo/extra_files/qm_fs/usr/share/text1.txt",
                        "src": "data/assets/text1.txt",
                    },
                ],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {"src": "data/assets/text1.txt", "dest": "usr/share/demo/"}
                },
            },
            [],
            {
                "delete": [],
                "directories": [],
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "data/assets/text1.txt.j2",
                        "dest": "usr/share/demo/",
                    }
                },
            },
            ["data/assets/text1.txt.j2"],
            {
                "delete": [],
                "directories": [
                    "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo"
                ],
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/text1.txt"
                        ),
                        "src": "data/assets/text1.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "data/assets/text1.txt.j2",
                        "dest": "usr/share/demo/test.txt",
                    }
                },
            },
            ["data/assets/text1.txt.j2"],
            {
                "delete": [],
                "directories": [
                    "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo"
                ],
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/test.txt"
                        ),
                        "src": "data/assets/text1.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {"src": "data/assets", "dest": "usr/share/demo"}
                },
            },
            ["data/assets/subfolder/text1.txt.j2"],
            {
                "delete": [],
                "directories": [
                    "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo/subfolder"
                ],
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/root_fs/"
                            "usr/share/demo/subfolder/text1.txt"
                        ),
                        "src": "data/assets/subfolder/text1.txt.j2",
                    }
                ],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "http://remote.org/data/text1.txt",
                        "dest": "usr/share/demo/test.txt",
                    }
                },
            },
            [],
            {
                "delete": [],
                "directories": [
                    "cache/downloads/remote.org/data",
                    "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo",
                ],
                "files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo/test.txt"
                        ),
                        "src": "cache/downloads/remote.org/data/text1.txt",
                        "remote_src": False,
                    },
                ],
                "templated_files": [],
                "fetch_files": [
                    {
                        "url": "http://remote.org/data/text1.txt",
                        "checksum": "",
                        "file": "cache/downloads/remote.org/data/text1.txt",
                    }
                ],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "http://remote.org/data/text1.txt",
                        "dest": "usr/share/demo/",
                    }
                },
            },
            [],
            {
                "delete": [],
                "directories": [
                    "cache/downloads/remote.org/data",
                    "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo",
                ],
                "files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo/text1.txt"
                        ),
                        "src": "cache/downloads/remote.org/data/text1.txt",
                        "remote_src": False,
                    },
                ],
                "templated_files": [],
                "fetch_files": [
                    {
                        "url": "http://remote.org/data/text1.txt",
                        "checksum": "",
                        "file": "cache/downloads/remote.org/data/text1.txt",
                    }
                ],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "src": "http://remote.org/data/text1.txt",
                        "checksum": "sha1:9e9c26a6cfee814567796d6c3c1add61d135ce73",
                        "dest": "usr/share/demo/test.txt",
                    }
                },
            },
            [],
            {
                "delete": [],
                "directories": [
                    "cache/downloads/remote.org/data",
                    "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo",
                ],
                "files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/demo/test.txt"
                        ),
                        "src": "cache/downloads/remote.org/data/text1.txt",
                        "remote_src": False,
                    },
                ],
                "templated_files": [],
                "fetch_files": [
                    {
                        "url": "http://remote.org/data/text1.txt",
                        "checksum": "sha1:9e9c26a6cfee814567796d6c3c1add61d135ce73",
                        "file": "cache/downloads/remote.org/data/text1.txt",
                    }
                ],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "dest": "usr/share/demo/",
                        "layer": "qm",
                        "touch": "true",
                    },
                    "test2": {
                        "dest": "usr/share/text1.txt",
                        "touch": "true",
                    },
                },
            },
            [],
            {
                "delete": [],
                "directories": [
                    "<build_id>/src/files/mydemo/extra_files/qm_fs/usr/share/demo",
                    "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share",
                ],
                "files": [
                    {
                        "dest": "<build_id>/src/files/mydemo/extra_files/root_fs/usr/share/text1.txt",
                    },
                ],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
        (
            {
                "name": "mydemo",
                "extra_files": {
                    "test1": {
                        "dest": "usr/share/demo/",
                        "layer": "qm",
                        "delete": "true",
                    },
                    "test2": {
                        "dest": "usr/share/text1.txt",
                        "delete": "true",
                    },
                },
            },
            [],
            {
                "delete": [
                    {"dest": "/usr/share/demo", "layer": "qm"},
                    {"dest": "/usr/share/text1.txt", "layer": "root"},
                ],
                "directories": [],
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            },
        ),
    ],
)
def test_get_build_src_extra_files(
    mock_settings, conf, extra_files, expected_result, tmp_dir
):
    conf.update({"base_dir": "."})
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv(conf=conf)
    test._reset_build_src_contents()

    for extra_file in extra_files:
        extra_file_path = tmp_dir / Path(extra_file)
        extra_file_path.parent.mkdir(parents=True, exist_ok=True)
        extra_file_path.touch()

    test._process_extra_files()
    assert test["build"]["src"] == expected_result


@pytest.mark.parametrize(
    "directory_list, expected_result",
    [
        (
            ["/usr/local", "/usr/local/share", "/usr/local/share/demo"],
            ["/usr/local/share/demo"],
        ),
        (
            ["/usr/local", "/usr/local/share/demo", "/usr/share/demo"],
            ["/usr/local/share/demo", "/usr/share/demo"],
        ),
    ],
)
def test_reduce_directory_list(directory_list, expected_result):
    test = TestBuildEnv()
    test.update({"directories": directory_list})
    test.reduce_directory_list(test, "directories")
    assert test["directories"] == expected_result


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected",
    [
        (
            {},
            {
                "install": [],
                "pre_build": [],
                "post_build": [],
                "clean_build": [],
                "uninstall": [],
                "clean": [],
            },
        ),
        (
            {"hooks": {"my_demo": {"fake_stage": []}}},
            {
                "install": [],
                "pre_build": [],
                "post_build": [],
                "clean_build": [],
                "uninstall": [],
                "clean": [],
            },
        ),
        (
            {
                "hooks": {
                    "my_demo": {
                        "install": ["install.yaml"],
                        "pre_build": ["pre_build.yaml"],
                        "post_build": ["post_build.yaml"],
                        "clean_build": ["clean_build.yaml"],
                        "uninstall": ["uninstall.yaml"],
                        "clean": ["clean.yaml"],
                    }
                }
            },
            {
                "install": ["/home/user/src/demo/install.yaml"],
                "pre_build": ["/home/user/src/demo/pre_build.yaml"],
                "post_build": ["/home/user/src/demo/post_build.yaml"],
                "clean_build": ["/home/user/src/demo/clean_build.yaml"],
                "uninstall": ["/home/user/src/demo/uninstall.yaml"],
                "clean": ["/home/user/src/demo/clean.yaml"],
            },
        ),
        (
            {
                "hooks": {
                    "my_demo": {
                        "install": ["/home/user/install.yaml"],
                        "pre_build": ["/home/user/pre_build.yaml"],
                        "post_build": ["/home/user/post_build.yaml"],
                        "clean_build": ["/home/user/clean_build.yaml"],
                        "uninstall": ["/home/user/uninstall.yaml"],
                        "clean": ["/home/user/clean.yaml"],
                    }
                }
            },
            {
                "install": ["/home/user/install.yaml"],
                "post_build": ["/home/user/post_build.yaml"],
                "pre_build": ["/home/user/pre_build.yaml"],
                "clean_build": ["/home/user/clean_build.yaml"],
                "uninstall": ["/home/user/uninstall.yaml"],
                "clean": ["/home/user/clean.yaml"],
            },
        ),
        (
            {
                "hooks": {
                    "my_demo": {
                        "install": ["/home/user/install.yaml"],
                        "pre_build": ["/home/user/pre_build.yaml"],
                        "post_build": ["/home/user/post_build.yaml"],
                        "clean_build": ["/home/user/clean_build.yaml"],
                        "uninstall": ["/home/user/uninstall.yaml"],
                        "clean": ["/home/user/clean.yaml"],
                    },
                    "my_addon": {
                        "pre_build": ["/home/user/addon/pre_build.yaml"],
                        "post_build": [],
                        "clean_build": ["/home/user/addon/clean_build.yaml"],
                        "uninstall": ["/home/user/addon/uninstall.yaml"],
                        "clean": ["/home/user/addon/clean.yaml"],
                    },
                }
            },
            {
                "install": ["/home/user/install.yaml"],
                "pre_build": [
                    "/home/user/pre_build.yaml",
                    "/home/user/addon/pre_build.yaml",
                ],
                "post_build": ["/home/user/post_build.yaml"],
                "clean_build": [
                    "/home/user/clean_build.yaml",
                    "/home/user/addon/clean_build.yaml",
                ],
                "uninstall": [
                    "/home/user/uninstall.yaml",
                    "/home/user/addon/uninstall.yaml",
                ],
                "clean": [
                    "/home/user/clean.yaml",
                    "/home/user/addon/clean.yaml",
                ],
            },
        ),
    ],
)
def test_get_configured_hooks(mock_settings, conf, expected):
    conf.update({"base_dir": "/home/user/src/demo/"})
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv()
    test.host = False
    test.base_dir = "/home/user/src/demo"

    test._get_configured_hooks()

    assert test["hooks"] == expected


@patch("src.autosd_demo.core.build.base.settings")
def test_get_preprocessed_osbuild_config(mock_settings):
    mock_settings.as_dict.return_value = {}
    expected_osbuild_conf = {
        "build": {
            "build_id": "<build_id>",
            "src": {
                "delete": [],
                "directories": [],
                "files": [],
                "templated_files": [],
                "fetch_files": [],
            },
        },
        "osbuild": {
            "chmod": {},
            "chown": {},
            "containers": [],
            "has_qm": False,
            "extra_contents": {},
            "groups": {},
            "mpp_vars": {},
            "qm_chmod": {},
            "qm_chown": {},
            "qm_containers": [],
            "qm_extra_contents": {},
            "qm_groups": {},
            "qm_systemd": {},
            "qm_users": {},
            "systemd": {},
            "users": {},
        },
    }

    actual_osbuild_conf = TestBuildEnv(osbuild=True)

    assert actual_osbuild_conf == expected_osbuild_conf


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected",
    [
        ({}, []),
        ({"apps": {}}, []),
        (
            {
                "centos_sig": {
                    "autosd_demo": {
                        "templates": {
                            "containers": {
                                "quadlet": "quadlet_template",
                                "containers_conf_path": "path/to/containers.conf.j2",
                                "storage_conf_path": "path/to/storage.conf.j2",
                            },
                        },
                    }
                },
                "apps": {
                    "my_app": {
                        "name": "my-app",
                        "info": "Some information.",
                        "version": "1.0.0",
                        "containers": {
                            "backend": {
                                "image": "fedora",
                                "container_context": "containers/backend",
                                "build_order": DEFAULT_BUILD_ORDER,
                            }
                        },
                        "paths": {"containers_conf": "path/to/containers.conf"},
                    }
                },
            },
            [
                {
                    "build": {
                        "directories": [
                            "<build_id>/.apps/my_app/containers",
                            "<build_id>/.apps/my_app/containers/storage",
                            (
                                "<build_id>/src/files/mydemo/apps/"
                                "my-app-1.0.0/dist/"
                                "var/apps/my-app/1.0.0/containers"
                            ),
                            (
                                "<build_id>/src/files/mydemo/apps/"
                                "my-app-1.0.0/dist/var/apps/"
                                "my-app/1.0.0/containers/storage"
                            ),
                        ],
                        "templated_files": [
                            {
                                "app_id": "my_app",
                                "dest": "<build_id>/.apps/my_app/containers/containers.conf",
                                "src": "path/to/containers.conf.j2",
                            },
                            {
                                "app_id": "my_app",
                                "dest": "<build_id>/.apps/my_app/containers/storage.conf",
                                "src": "path/to/storage.conf.j2",
                            },
                        ],
                    },
                    "bundle_path": "<build_id>/src/files/mydemo/apps/my-app-1.0.0.app",
                    "containers": [
                        {
                            "build": {
                                "containers_conf": "<build_id>/.apps/my_app/containers/containers.conf",
                                "dist_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/dist",
                                "dist_container_storage_dir": (
                                    "<build_id>/src/files/mydemo/apps/"
                                    "my-app-1.0.0/dist/"
                                    "var/apps/my-app/1.0.0/"
                                    "containers/storage"
                                ),
                                "src_dir": (
                                    "<build_id>/src/files/mydemo/apps/"
                                    "my-app-1.0.0/src/backend"
                                ),
                                "storage_conf": "<build_id>/.apps/my_app/containers/storage.conf",
                            },
                            "build_order": DEFAULT_BUILD_ORDER,
                            "container_context": "containers/backend",
                            "id": "backend",
                            "image": "fedora",
                            "quadlet_file": (
                                "<build_id>/src/files/mydemo/apps/"
                                "my-app-1.0.0/dist/"
                                "var/apps/my-app/1.0.0/"
                                "etc/containers/systemd/"
                                "backend.container"
                            ),
                        }
                    ],
                    "containers_dir": "<build_id>/.apps/my_app",
                    "dest_dir": "apps",
                    "dist_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/dist",
                    "id": "my_app",
                    "info": "Some information.",
                    "installer_path": "<build_id>/src/files/mydemo/apps/my-app-1.0.0.sh",
                    "name": "my-app",
                    "src": {
                        "directories": [
                            (
                                "<build_id>/src/files/mydemo/apps/"
                                "my-app-1.0.0/dist/var/apps/"
                                "my-app/1.0.0/etc/containers/systemd"
                            ),
                        ],
                        "files": [],
                        "templated_files": [
                            {
                                "app_id": "my_app",
                                "container_id": "backend",
                                "dest": (
                                    "<build_id>/src/files/mydemo/apps/"
                                    "my-app-1.0.0/dist/"
                                    "var/apps/my-app/1.0.0/"
                                    "etc/containers/systemd/backend.container"
                                ),
                                "image_storage_path": (
                                    "/var/apps/my-app/1.0.0/" "containers/storage"
                                ),
                                "sign": "true",
                                "src": "quadlet_template",
                            }
                        ],
                    },
                    "src_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/src",
                    "tarball_path": "<build_id>/src/files/mydemo/apps/my-app-1.0.0.tar.gz",
                    "version": "1.0.0",
                }
            ],
        ),
        (
            {
                "centos_sig": {
                    "autosd_demo": {
                        "templates": {
                            "containers": {
                                "quadlet": "quadlet_template",
                                "containers_conf_path": "path/to/containers.conf.j2",
                                "storage_conf_path": "path/to/storage.conf.j2",
                            },
                        },
                    }
                },
                "apps": {
                    "my_app": {
                        "name": "my-app",
                        "version": "1.0.0",
                        "containers": {
                            "fedora": {
                                "image": "fedora",
                                "quadlet": False,
                                "build_order": DEFAULT_BUILD_ORDER,
                            }
                        },
                        "paths": {"containers_conf": "path/to/containers.conf"},
                    }
                },
            },
            [
                {
                    "name": "my-app",
                    "info": "Information N/A.",
                    "version": "1.0.0",
                    "bundle_path": "<build_id>/src/files/mydemo/apps/my-app-1.0.0.app",
                    "containers": [
                        {
                            "image": "fedora",
                            "quadlet": False,
                            "id": "fedora",
                            "build": {
                                "containers_conf": "<build_id>/.apps/my_app/containers/containers.conf",
                                "storage_conf": "<build_id>/.apps/my_app/containers/storage.conf",
                                "dist_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/dist",
                                "dist_container_storage_dir": (
                                    "<build_id>/src/files/mydemo/apps/"
                                    "my-app-1.0.0/dist/"
                                    "var/apps/my-app/1.0.0/"
                                    "containers/storage"
                                ),
                            },
                            "build_order": DEFAULT_BUILD_ORDER,
                        }
                    ],
                    "containers_dir": "<build_id>/.apps/my_app",
                    "dest_dir": "apps",
                    "dist_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/dist",
                    "id": "my_app",
                    "installer_path": "<build_id>/src/files/mydemo/apps/my-app-1.0.0.sh",
                    "src": {
                        "directories": [],
                        "files": [],
                        "templated_files": [],
                    },
                    "src_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/src",
                    "tarball_path": "<build_id>/src/files/mydemo/apps/my-app-1.0.0.tar.gz",
                    "build": {
                        "directories": [
                            "<build_id>/.apps/my_app/containers",
                            "<build_id>/.apps/my_app/containers/storage",
                            (
                                "<build_id>/src/files/mydemo/apps/"
                                "my-app-1.0.0/dist/"
                                "var/apps/my-app/1.0.0/containers"
                            ),
                            (
                                "<build_id>/src/files/mydemo/apps/"
                                "my-app-1.0.0/dist/"
                                "var/apps/my-app/1.0.0/containers/storage"
                            ),
                        ],
                        "templated_files": [
                            {
                                "src": "path/to/containers.conf.j2",
                                "dest": "<build_id>/.apps/my_app/containers/containers.conf",
                                "app_id": "my_app",
                            },
                            {
                                "src": "path/to/storage.conf.j2",
                                "dest": "<build_id>/.apps/my_app/containers/storage.conf",
                                "app_id": "my_app",
                            },
                        ],
                    },
                }
            ],
        ),
        (
            {
                "centos_sig": {
                    "autosd_demo": {
                        "templates": {
                            "containers": {
                                "quadlet": "quadlet_template",
                                "containers_conf_path": "path/to/containers.conf.j2",
                                "storage_conf_path": "path/to/storage.conf.j2",
                            },
                        },
                    }
                },
                "apps": {
                    "my_app": {
                        "name": "my-app",
                        "version": "1.0.0",
                        "containers": {
                            "fedora": {
                                "image": "fedora",
                                "quadlet": False,
                                "build_order": DEFAULT_BUILD_ORDER,
                            },
                            "graphics": {
                                "image": "graphics",
                                "quadlet": False,
                                "build_order": 10,
                            },
                        },
                        "paths": {"containers_conf": "path/to/containers.conf"},
                    }
                },
            },
            [
                {
                    "name": "my-app",
                    "info": "Information N/A.",
                    "version": "1.0.0",
                    "bundle_path": "<build_id>/src/files/mydemo/apps/my-app-1.0.0.app",
                    "containers": [
                        {
                            "image": "graphics",
                            "quadlet": False,
                            "id": "graphics",
                            "build": {
                                "containers_conf": "<build_id>/.apps/my_app/containers/containers.conf",
                                "storage_conf": "<build_id>/.apps/my_app/containers/storage.conf",
                                "dist_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/dist",
                                "dist_container_storage_dir": (
                                    "<build_id>/src/files/mydemo/apps/"
                                    "my-app-1.0.0/dist/"
                                    "var/apps/my-app/1.0.0/"
                                    "containers/storage"
                                ),
                            },
                            "build_order": 10,
                        },
                        {
                            "image": "fedora",
                            "quadlet": False,
                            "id": "fedora",
                            "build": {
                                "containers_conf": "<build_id>/.apps/my_app/containers/containers.conf",
                                "storage_conf": "<build_id>/.apps/my_app/containers/storage.conf",
                                "dist_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/dist",
                                "dist_container_storage_dir": (
                                    "<build_id>/src/files/mydemo/apps/"
                                    "my-app-1.0.0/dist/"
                                    "var/apps/my-app/1.0.0/"
                                    "containers/storage"
                                ),
                            },
                            "build_order": DEFAULT_BUILD_ORDER,
                        },
                    ],
                    "containers_dir": "<build_id>/.apps/my_app",
                    "dest_dir": "apps",
                    "dist_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/dist",
                    "id": "my_app",
                    "installer_path": "<build_id>/src/files/mydemo/apps/my-app-1.0.0.sh",
                    "src": {
                        "directories": [],
                        "files": [],
                        "templated_files": [],
                    },
                    "src_dir": "<build_id>/src/files/mydemo/apps/my-app-1.0.0/src",
                    "tarball_path": "<build_id>/src/files/mydemo/apps/my-app-1.0.0.tar.gz",
                    "build": {
                        "directories": [
                            "<build_id>/.apps/my_app/containers",
                            "<build_id>/.apps/my_app/containers/storage",
                            (
                                "<build_id>/src/files/mydemo/apps/"
                                "my-app-1.0.0/dist/"
                                "var/apps/my-app/1.0.0/containers"
                            ),
                            (
                                "<build_id>/src/files/mydemo/apps/"
                                "my-app-1.0.0/dist/"
                                "var/apps/my-app/1.0.0/containers/storage"
                            ),
                        ],
                        "templated_files": [
                            {
                                "src": "path/to/containers.conf.j2",
                                "dest": "<build_id>/.apps/my_app/containers/containers.conf",
                                "app_id": "my_app",
                            },
                            {
                                "src": "path/to/storage.conf.j2",
                                "dest": "<build_id>/.apps/my_app/containers/storage.conf",
                                "app_id": "my_app",
                            },
                        ],
                    },
                }
            ],
        ),
    ],
)
def test_process_apps(mock_settings, conf, expected):
    conf.update({"name": "mydemo", "base_dir": "."})
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv(conf=conf)
    test._process_apps()

    assert test.get("build", {}).get("apps", []) == expected


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected",
    [
        ({}, {}),
        (
            {
                "compose": {
                    "services": {"service1": {"image": "quay.io/service1:latest"}}
                }
            },
            {"services": {"service1": {"image": "quay.io/service1:latest"}}},
        ),
        (
            {"compose": {"services": {"service1": {"build": "containers/service1"}}}},
            {"services": {"service1": {"image": "localhost/compose/service1:latest"}}},
        ),
        (
            {
                "compose": {
                    "services": {
                        "service1": {"build": {"context": "containers/service1"}}
                    }
                }
            },
            {"services": {"service1": {"image": "localhost/compose/service1:latest"}}},
        ),
        (
            {
                "compose": {
                    "services": {
                        "service1": {
                            "build": "containers/service1",
                            "extra_files": {
                                "config": {
                                    "src": "path/to/config.json",
                                    "dest": "path/to/config.json",
                                }
                            },
                        }
                    }
                }
            },
            {"services": {"service1": {"image": "localhost/compose/service1:latest"}}},
        ),
    ],
)
def test_process_compose(mock_settings, conf, expected):
    conf.update(
        {
            "name": "mydemo",
            "base_dir": ".",
            "centos_sig": {
                "autosd_demo": {
                    "templates": {
                        "compose": {
                            "containers_conf_path": "path/to/containers.conf.j2",
                            "storage_conf_path": "path/to/storage.conf.j2",
                        },
                    },
                }
            },
        }
    )
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv(conf=conf)
    test._process_compose()

    assert test.get("build", {}).get("compose_file", {}) == expected


@patch("autosd_demo.core.build.base.settings")
def test_process_compose_init(mock_settings):
    conf = {
        "name": "mydemo",
        "base_dir": ".",
        "centos_sig": {
            "autosd_demo": {
                "templates": {
                    "compose": {
                        "containers_conf_path": "path/to/containers.conf.j2",
                        "storage_conf_path": "path/to/storage.conf.j2",
                    },
                },
            }
        },
    }
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv(conf=conf)
    test._process_compose_init()
    assert test["build"]["compose_file_path"] == "compose/compose.yml"


@patch("autosd_demo.core.build.base.settings")
def test_process_compose_init_with_export(mock_settings):
    conf = {
        "name": "mydemo",
        "base_dir": ".",
        "centos_sig": {
            "autosd_demo": {
                "templates": {
                    "compose": {
                        "containers_conf_path": "path/to/containers.conf.j2",
                        "storage_conf_path": "path/to/storage.conf.j2",
                    },
                },
            }
        },
    }
    mock_settings.as_dict.return_value = conf
    test = TestBuildEnv(conf=conf, export=True)
    test._process_compose_init()
    assert test["build"]["compose_file_path"] == "<build_id>/src/compose/compose.yml"


@patch("autosd_demo.core.build.env.in_autosd_demo")
def test_set_build_env_config_with_id(in_autosd_mock):
    b = BuildEnv("localhost")
    in_autosd_mock.return_value = True
    mock_build_id = "testing"
    b.update = MagicMock()
    b._get_configured_hooks = MagicMock()
    b._reset_build_src_contents = MagicMock()
    b._process_extra_files = MagicMock()
    b._process_containers = MagicMock()
    b._process_apps = MagicMock()

    b._set_build_env_config(mock_build_id)

    b.update.assert_called_once()
    b._get_configured_hooks.assert_called_once()
    b._reset_build_src_contents.assert_called_once()
    b._process_extra_files.assert_called_once()
    b._process_containers.assert_called_once()
    b._process_apps.assert_called_once()

    assert b["build"]["build_id"] == mock_build_id
