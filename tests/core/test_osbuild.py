from collections import OrderedDict
from unittest.mock import patch

import pytest
from passlib.handlers.sha2_crypt import sha256_crypt

from autosd_demo.cli.exceptions import ValidationError
from autosd_demo.core.osbuild import OSBuildConfig


class TestOSBuildConfig(OSBuildConfig):
    def __init__(self, img_sources={}):
        super(OSBuildConfig, self).__init__()
        self.build_files = img_sources.get("files", [])
        self.build_files += img_sources.get("templated_files", [])
        self.build_directories = img_sources.get("directories", [])
        self.delete_contents = img_sources.get("delete", [])


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf",
    [
        ({"systemd": {"service1": {"strange": "value"}}}),
        ({"systemd": {"service1": {"enabled_services": 5}}}),
    ],
)
def test_get_preprocessed_systemd_invalid_conf(mock_settings, conf):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    with pytest.raises(ValidationError):
        test._get_preprocessed_systemd()


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected",
    [
        ({}, {"systemd": {}, "qm_systemd": {}}),
        (
            {"systemd": {"service1": {"enabled_services": "ssh"}}},
            {"systemd": {"enabled_services": ["ssh"]}, "qm_systemd": {}},
        ),
        (
            {"systemd": {"service1": {"layer": "qm", "enabled_services": "ssh"}}},
            {"systemd": {}, "qm_systemd": {"enabled_services": ["ssh"]}},
        ),
        (
            {
                "systemd": {
                    "service1": {"layer": "qm", "enabled_services": ["ssh", "httpd"]}
                }
            },
            {"systemd": {}, "qm_systemd": {"enabled_services": ["httpd", "ssh"]}},
        ),
        (
            {
                "systemd": {
                    "service1": {
                        "layer": "qm",
                        "enabled_services": ["ssh", "httpd", "ssh"],
                    }
                }
            },
            {"systemd": {}, "qm_systemd": {"enabled_services": ["httpd", "ssh"]}},
        ),
        (
            {
                "systemd": {
                    "service1": {"enabled_services": "ssh"},
                    "service2": {"enabled_services": "ssh"},
                }
            },
            {"systemd": {"enabled_services": ["ssh"]}, "qm_systemd": {}},
        ),
    ],
)
def test_get_preprocessed_systemd(mock_settings, conf, expected):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    test._get_preprocessed_systemd()
    assert test == expected


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf",
    [
        ({"users": {"user1": {"strange": "value"}}}),
    ],
)
def test_get_preprocessed_users_invalid_conf(mock_settings, conf):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    with pytest.raises(ValidationError):
        test._get_preprocessed_users()


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        ({}, {"users": {}, "qm_users": {}}),
        (
            {
                "users": {
                    "user1": {
                        "name": "user1",
                        "gid": "1000",
                        "uid": "1000",
                        "groups": "admin",
                        "home": "/home/user1",
                    }
                }
            },
            {
                "users": {
                    "user1": {
                        "gid": 1000,
                        "uid": 1000,
                        "groups": ["admin"],
                        "home": "/home/user1",
                    }
                },
                "qm_users": {},
            },
        ),
        (
            {
                "users": {
                    "user1": {
                        "name": "user1",
                        "gid": "1000",
                        "uid": "1000",
                        "groups": 1001,
                        "home": "/home/user1",
                    }
                }
            },
            {
                "users": {
                    "user1": {
                        "gid": 1000,
                        "uid": 1000,
                        "groups": [1001],
                        "home": "/home/user1",
                    }
                },
                "qm_users": {},
            },
        ),
        (
            {
                "users": {
                    "user1": {
                        "name": "user1",
                        "gid": 1000,
                        "uid": 1000,
                        "groups": ["admin"],
                        "home": "/home/user1",
                        "layer": "qm",
                    }
                }
            },
            {
                "users": {},
                "qm_users": {
                    "user1": {
                        "gid": 1000,
                        "uid": 1000,
                        "groups": ["admin"],
                        "home": "/home/user1",
                    }
                },
            },
        ),
    ],
)
def test_get_preprocessed_users(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    test._get_preprocessed_users()
    assert test == expected_result


@patch("autosd_demo.core.build.base.settings")
def test_get_preprocessed_users_with_password(mock_settings):
    conf = {"users": {"user1": {"name": "user1", "password": "password1234"}}}
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    test._get_preprocessed_users()
    assert sha256_crypt.verify("password1234", test["users"]["user1"]["password"])


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf",
    [({"groups": {"group1": {"strange": "value"}}})],
)
def test_get_preprocessed_groups_invalid_conf(mock_settings, conf):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    with pytest.raises(ValidationError):
        test._get_preprocessed_groups()


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        ({}, {"groups": {}, "qm_groups": {}}),
        (
            {"groups": {"group1": {"gid": "1020"}}},
            {"groups": {"group1": {"gid": 1020}}, "qm_groups": {}},
        ),
        (
            {"groups": {"group1": {"gid": "1020", "layer": "qm"}}},
            {"groups": {}, "qm_groups": {"group1": {"gid": 1020}}},
        ),
    ],
)
def test_get_preprocessed_groups(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    test._get_preprocessed_groups()
    assert test == expected_result


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf",
    [
        ({"fs_perm": {"perm1": {"user": "user1", "group": "group1"}}}),
        ({"fs_perm": {"perm1": {"path": "test/", "perms": 3}}}),
    ],
)
def test_get_preprocessed_permissions_invalid_conf(mock_settings, conf):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    with pytest.raises(ValidationError):
        test._get_preprocessed_permissions()


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        ({}, {"chown": {}, "chmod": {}, "qm_chown": {}, "qm_chmod": {}}),
        (
            {"fs_perm": {"perm1": {"path": "test.txt"}}},
            {"chown": {}, "chmod": {}, "qm_chown": {}, "qm_chmod": {}},
        ),
        (
            {
                "fs_perm": {
                    "perm1": {"path": "test.txt", "user": "user1", "group": "group1"}
                }
            },
            {
                "chown": {
                    "test.txt": {"user": "user1", "group": "group1", "recursive": False}
                },
                "chmod": {},
                "qm_chown": {},
                "qm_chmod": {},
            },
        ),
        (
            {
                "fs_perm": {
                    "perm1": {
                        "path": "test/",
                        "user": "user1",
                        "perms": "777",
                        "recursive": True,
                    }
                }
            },
            {
                "chown": {"test/": {"user": "user1", "recursive": True}},
                "chmod": {"test/": {"mode": "777", "recursive": True}},
                "qm_chown": {},
                "qm_chmod": {},
            },
        ),
        (
            {
                "fs_perm": {
                    "perm1": {
                        "path": "test.txt",
                        "user": "user1",
                        "perms": "o-rwx",
                        "layer": "qm",
                    }
                }
            },
            {
                "chown": {},
                "chmod": {},
                "qm_chown": {"test.txt": {"user": "user1", "recursive": False}},
                "qm_chmod": {"test.txt": {"mode": "o-rwx", "recursive": False}},
            },
        ),
    ],
)
def test_get_preprocessed_permissions(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    test._get_preprocessed_permissions()
    assert test == expected_result


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf",
    [
        ({"containers": {"app": {"systemd": {}}}}),
        ({"containers": {"app": {"systemd": {"image": "foo"}}}}),
    ],
)
def test_get_preprocessed_containers_invalid_conf(mock_settings, conf):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    with pytest.raises(ValidationError):
        test._get_preprocessed_containers()


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf,expected_result",
    [
        ({}, {"containers": [], "qm_containers": []}),
        (
            {
                "containers": {
                    "app": {
                        "image_ref": "localhost/app:latest",
                        "systemd": {"container": {"image": "localhost/app:latest"}},
                    },
                    "qm_app": {
                        "image_ref": "localhost/qm/app:latest",
                        "systemd": {"container": {"image": "localhost/qm/app:latest"}},
                        "layer": "qm",
                    },
                }
            },
            {
                "containers": [
                    OrderedDict(
                        {
                            "source": "localhost/app",
                            "tag": "latest",
                            "name": "localhost/app:latest",
                            "containers-transport": "containers-storage",
                        }
                    )
                ],
                "qm_containers": [
                    OrderedDict(
                        {
                            "source": "localhost/qm/app",
                            "tag": "latest",
                            "name": "localhost/qm/app:latest",
                            "containers-transport": "containers-storage",
                        }
                    )
                ],
            },
        ),
    ],
)
def test_get_preprocessed_containers(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    test._get_preprocessed_containers()
    assert test == expected_result


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf",
    [({"rpm_repo": {"repo1": {"layer": "qm"}}})],
)
def test_get_preprocessed_rpm_repos_invalid_conf(mock_settings, conf):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    with pytest.raises(ValidationError):
        test._get_preprocessed_rpm_repos()


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf,expected_result",
    [
        (
            {"rpm_repo": {"repo1": {"baseurl": "http://example.com", "layer": "qm"}}},
            {
                "qm_extra_repos": [{"id": "repo1", "baseurl": "http://example.com"}],
            },
        ),
        (
            {
                "rpm_repo": {
                    "repo1": {
                        "baseurl": "http://example.com",
                    }
                }
            },
            {
                "extra_repos": [{"id": "repo1", "baseurl": "http://example.com"}],
            },
        ),
    ],
)
def test_get_preprocessed_rpm_repos(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    result = test._get_preprocessed_rpm_repos()
    assert result == expected_result


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf",
    [
        ({"rpm": {"rpm1": {"no_packages_field": True}}}),
    ],
)
def test_get_preprocessed_rpms_invalid_conf(mock_settings, conf):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    with pytest.raises(ValidationError):
        test._get_preprocessed_rpms()


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected_result",
    [
        (
            {"rpm": {"rpm1": {"packages": ["pkg2", "pkg1"]}}},
            {"extra_rpms": ["pkg1", "pkg2"]},
        ),
        (
            {"rpm": {"rpm1": {"packages": ["pkg2", "pkg1"], "layer": "qm"}}},
            {"qm_extra_rpms": ["pkg1", "pkg2"]},
        ),
        (
            {
                "rpm": {
                    "rpm1": {"packages": ["pkg1", "pkg2"], "layer": "qm"},
                    "rpm2": {
                        "packages": ["pkg3", "pkg4"],
                    },
                }
            },
            {
                "extra_rpms": ["pkg3", "pkg4"],
                "qm_extra_rpms": ["pkg1", "pkg2"],
            },
        ),
        (
            {
                "rpm": {
                    "rpm1": {"packages": ["pkg1", "pkg2", "pkg2"], "layer": "qm"},
                    "rpm2": {
                        "packages": ["pkg3", "pkg4", "pkg4"],
                    },
                }
            },
            {
                "extra_rpms": ["pkg3", "pkg4"],
                "qm_extra_rpms": ["pkg1", "pkg2"],
            },
        ),
    ],
)
def test_get_preprocessed_rpms(mock_settings, conf, expected_result):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    result = test._get_preprocessed_rpms()
    assert result == expected_result


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, img_sources, expected",
    [
        (
            {},
            {"delete": [], "directories": [], "files": [], "templated_files": []},
            {"extra_contents": {}, "qm_extra_contents": {}},
        ),
        (
            {},
            {
                "delete": [],
                "directories": [
                    "<build_id>/src/files/mydemo/containers/alpha",
                    "<build_id>/src/files/mydemo/containers/alpha/conf",
                ],
                "files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/"
                            "containers/alpha/Containerfile"
                        ),
                        "src": "containers/alpha/Containerfile",
                    },
                    {
                        "dest": "<build_id>/src/files/mydemo/containers/alpha/app.py",
                        "src": "containers/alpha/app.py",
                    },
                ],
                "templated_files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/"
                            "containers/alpha/conf/config.yaml"
                        ),
                        "src": "containers/alpha/conf/config.yaml.j2",
                    }
                ],
            },
            {"extra_contents": {}, "qm_extra_contents": {}},
        ),
        (
            {},
            {
                "delete": [],
                "directories": [
                    "<build_id>/src/files/mydemo/extra_files/qm_fs/etc/containers/systemd",
                    "<build_id>/src/files/mydemo/extra_files/root_fs/etc/containers/systemd",
                ],
                "files": [],
                "templated_files": [
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/root_fs/"
                            "etc/containers/systemd/alpha.container"
                        ),
                        "src": "/test/systemd.j2",
                    },
                    {
                        "dest": (
                            "<build_id>/src/files/mydemo/extra_files/qm_fs/"
                            "etc/containers/systemd/beta.container"
                        ),
                        "src": "/test/systemd.j2",
                    },
                ],
                "fetch_files": [],
            },
            {
                "extra_contents": {
                    "add_files": [
                        OrderedDict(
                            [
                                ("path", "/etc/containers/systemd/alpha.container"),
                                (
                                    "source_path",
                                    "../files/mydemo/extra_files/root_fs/etc/containers/systemd/alpha.container",
                                ),
                            ]
                        )
                    ],
                    "directories": [
                        {
                            "exist_ok": True,
                            "parents": True,
                            "path": "/etc/containers/systemd",
                        }
                    ],
                    "inputs": {
                        "root_extra_content_0": OrderedDict(
                            {
                                "type": "org.osbuild.files",
                                "origin": "org.osbuild.source",
                                "mpp-embed": OrderedDict(
                                    {
                                        "id": "113b38d",
                                        "path": "../files/mydemo/extra_files/root_fs/"
                                        "etc/containers/systemd/alpha.container",
                                    }
                                ),
                            }
                        )
                    },
                    "paths": [
                        OrderedDict(
                            {
                                "from": {
                                    "mpp-format-string": "input://root_extra_content_0/{embedded['113b38d']}"
                                },
                                "to": "tree:///etc/containers/systemd/alpha.container",
                            }
                        )
                    ],
                },
                "qm_extra_contents": {
                    "add_files": [
                        OrderedDict(
                            [
                                ("path", "/etc/containers/systemd/beta.container"),
                                (
                                    "source_path",
                                    "../files/mydemo/extra_files/qm_fs/etc/containers/systemd/beta.container",
                                ),
                            ]
                        )
                    ],
                    "directories": [
                        {
                            "exist_ok": True,
                            "parents": True,
                            "path": "/etc/containers/systemd",
                        }
                    ],
                    "inputs": {
                        "qm_extra_content_0": OrderedDict(
                            {
                                "type": "org.osbuild.files",
                                "origin": "org.osbuild.source",
                                "mpp-embed": OrderedDict(
                                    {
                                        "id": "b1481e0",
                                        "path": "../files/mydemo/extra_files/qm_fs/"
                                        "etc/containers/systemd/beta.container",
                                    }
                                ),
                            }
                        )
                    },
                    "paths": [
                        OrderedDict(
                            {
                                "from": {
                                    "mpp-format-string": "input://qm_extra_content_0/{embedded['b1481e0']}"
                                },
                                "to": "tree:///etc/containers/systemd/beta.container",
                            }
                        )
                    ],
                },
            },
        ),
        (
            {},
            {
                "delete": [
                    {
                        "dest": "/etc/containers/systemd/alpha.container",
                        "layer": "root",
                    },
                    {"dest": "/etc/containers/systemd/beta.container", "layer": "qm"},
                ],
                "directories": [],
                "templated_files": [],
                "files": [],
                "fetch_files": [],
            },
            {
                "extra_contents": {
                    "delete": ["/etc/containers/systemd/alpha.container"],
                },
                "qm_extra_contents": {
                    "delete": ["/etc/containers/systemd/beta.container"],
                },
            },
        ),
    ],
)
def test_get_preprocessed_extra_contents(mock_settings, conf, img_sources, expected):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig(img_sources)
    test._get_preprocessed_extra_contents()
    assert test == expected


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, expected",
    [
        (
            {"dracut": {"alpha": {"modules": [], "install": []}}},
            {"dracut_install": [], "dracut_add_modules": []},
        ),
        (
            {"dracut": {"alpha": {"modules": ["mod1"], "install": ["file1"]}}},
            {"dracut_install": ["file1"], "dracut_add_modules": ["mod1"]},
        ),
        (
            {"dracut": {"alpha": {"modules": ["mod1"]}}},
            {"dracut_add_modules": ["mod1"]},
        ),
        (
            {"dracut": {"alpha": {"install": ["file1"]}}},
            {"dracut_install": ["file1"]},
        ),
    ],
)
def test_get_preprocessed_dracut_conf(mock_settings, conf, expected):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig({})

    result = test._get_preprocessed_dracut_conf()

    assert set(result) == set(expected)


@patch("autosd_demo.core.build.base.settings")
@pytest.mark.parametrize(
    "conf, osbuild, expected",
    [
        (
            {},
            {},
            {"mpp_vars": {}},
        ),
        (
            {"mpp_vars": {}},
            {},
            {"mpp_vars": {}},
        ),
        (
            {"mpp_vars": {"use_composefs": True}},
            {},
            {"mpp_vars": {"use_composefs": True}},
        ),
        (
            {"mpp_vars": {"use_composefs": True}},
            {"containers": {"alpha": {"image": "fedora"}}},
            {
                "mpp_vars": {
                    "use_composefs": True,
                    "use_containers_extra_store": True,
                    "extra_rpms": ["podman"],
                }
            },
        ),
        (
            {
                "mpp_vars": {"use_composefs": True},
                "rpm": {"extra_packages": {"packages": ["podman"]}},
            },
            {"containers": {"alpha": {"image": "fedora"}}},
            {
                "mpp_vars": {
                    "use_composefs": True,
                    "use_containers_extra_store": True,
                    "extra_rpms": ["podman"],
                }
            },
        ),
        (
            {"mpp_vars": {"use_composefs": True}},
            {"qm_containers": {"beta": {"image": "fedora"}}},
            {
                "mpp_vars": {
                    "use_composefs": True,
                    "use_qm_containers_extra_store": True,
                }
            },
        ),
        (
            {"mpp_vars": {"use_composefs": True}},
            {"has_qm": True},
            {"mpp_vars": {"use_composefs": True, "use_qm": True, "use_bluechi": True}},
        ),
        (
            {
                "mpp_vars": {"use_composefs": True},
                "rpm_repo": {"repo1": {"baseurl": "http://example.com", "layer": "qm"}},
            },
            {"has_qm": True},
            {
                "mpp_vars": {
                    "use_composefs": True,
                    "use_qm": True,
                    "use_bluechi": True,
                    "qm_extra_repos": [
                        {"id": "repo1", "baseurl": "http://example.com"}
                    ],
                }
            },
        ),
        (
            {
                "mpp_vars": {"use_composefs": True},
                "rpm": {
                    "rpm1": {"packages": ["pkg1", "pkg2"], "layer": "qm"},
                    "rpm2": {
                        "packages": ["pkg3", "pkg4"],
                    },
                },
            },
            {"has_qm": True},
            {
                "mpp_vars": {
                    "use_composefs": True,
                    "use_qm": True,
                    "use_bluechi": True,
                    "extra_rpms": [
                        "pkg3",
                        "pkg4",
                    ],
                    "qm_extra_rpms": ["pkg1", "pkg2"],
                }
            },
        ),
        (
            {"mpp_vars": {"use_composefs": True}, "dracut": {"alpha": {"modules": []}}},
            {"has_qm": True},
            {
                "mpp_vars": {
                    "use_composefs": True,
                    "use_qm": True,
                    "use_bluechi": True,
                    "dracut_add_modules": [],
                }
            },
        ),
    ],
)
def test_get_preprocessed_mpp_vars(mock_settings, conf, osbuild, expected):
    mock_settings.as_dict.return_value = conf
    test = TestOSBuildConfig()
    test.update(osbuild)
    test._get_preprocessed_mpp_vars()

    assert test["mpp_vars"] == expected["mpp_vars"]
