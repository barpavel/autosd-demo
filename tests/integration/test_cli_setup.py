import os
from pathlib import Path

import pytest
import yaml

from autosd_demo import AUTOSD_DEMO_DATA_PATH


@pytest.mark.integration
def test_cli_setup():
    ansible_defaults_path = Path(
        AUTOSD_DEMO_DATA_PATH, "ansible", "vars", "default.yaml"
    )
    with ansible_defaults_path.open() as fd:
        ansible_defaults = yaml.safe_load(fd)

    assert run_cli_command("autosd-demo setup") == 0
    for pkg in ansible_defaults["required_pkgs_general"]:
        assert run_cli_command(f"rpm -qi {pkg} > /dev/null") == 0
    for pkg in ansible_defaults["required_pkgs_automotive"]:
        assert run_cli_command(f"rpm -qi {pkg} > /dev/null") == 0

    assert run_cli_command("autosd-demo setup --operation upgrade") == 0
    for pkg in ansible_defaults["required_pkgs_general"]:
        assert run_cli_command(f"rpm -qi {pkg} > /dev/null") == 0
    for pkg in ansible_defaults["required_pkgs_automotive"]:
        assert run_cli_command(f"rpm -qi {pkg} > /dev/null") == 0

    assert run_cli_command("autosd-demo setup --operation uninstall") == 0
    for pkg in ansible_defaults["required_pkgs_general"]:
        assert run_cli_command(f"rpm -qi {pkg} > /dev/null") == 0
    for pkg in ansible_defaults["required_pkgs_automotive"]:
        assert run_cli_command(f"rpm -qi {pkg} > /dev/null") == 1

    assert run_cli_command("autosd-demo setup --operation install") == 0
    for pkg in ansible_defaults["required_pkgs_general"]:
        assert run_cli_command(f"rpm -qi {pkg} > /dev/null") == 0
    for pkg in ansible_defaults["required_pkgs_automotive"]:
        assert run_cli_command(f"rpm -qi {pkg} > /dev/null") == 0


def run_cli_command(command):
    return os.WEXITSTATUS(os.system(command))
