import os
from pathlib import Path

import pytest

from autosd_demo import AUTOSD_DEMO_LOCAL_CONF_FILENAME


@pytest.mark.integration
def test_cli_setup(simple_ws: Path):
    cfg = simple_ws.joinpath(AUTOSD_DEMO_LOCAL_CONF_FILENAME)

    assert cfg.exists()
    assert run_cli_command("autosd-demo show config") == 0
    assert run_cli_command("autosd-demo show manifest") == 0


def run_cli_command(command):
    return os.WEXITSTATUS(os.system(command))
