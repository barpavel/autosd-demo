import os
from pathlib import Path
from unittest.mock import patch

import pytest
from dynaconf.utils import DynaconfDict

import autosd_demo.settings as config
from autosd_demo import (
    AUTOSD_DEMO_DEFAULT_CONTAINERS_BUILD_ORDER as DEFAULT_BUILD_ORDER,
)
from autosd_demo import AUTOSD_DEMO_LOCAL_CONF_FILENAME
from autosd_demo.settings import Settings

GLOBAL_SETTINGS = """
    [default]
    version="1.0.0"

    [default.rpm._dnf]
    packages=["dnf"]

    [default.remote_host.my_builder]
    user="myuser"
    address="192.168.1.1"
"""

LOCAL_SETTINGS = """
    [default]
    version="2.0.0"

    [default.remote_host.my_builder]
    user="myuser"
    address="192.168.2.2"
"""

EXTRA_FILE_SETTINGS = """
    [default]
    version="2.0.0"

    [default.extra_files.touch_file]
    dest="/tmp/touch.file"
    touch=true

    [default.containers.server]
    image="server"
    container_context="containers/server"

    [default.containers.client]
    image="client"
    container_context="containers/client"
    layer="qm"
"""


class MockSettings(Settings):
    def __init__(self, mock_settings):
        super(Settings, self).__init__(DynaconfDict(mock_settings))


def test_all_settings_in_autosd_demo(tmp_dir, global_config):
    local_config = tmp_dir / AUTOSD_DEMO_LOCAL_CONF_FILENAME
    local_config.write_text(LOCAL_SETTINGS)

    global_config.write_text(GLOBAL_SETTINGS)
    with patch("autosd_demo.settings.AUTOSD_DEMO_GLOBAL_CONF_FILE", global_config):
        settings = Settings()
    assert Path(settings.base_dir) == tmp_dir
    assert settings.version == "2.0.0"
    assert settings.rpm._dnf.packages == ["dnf"]
    assert settings.remote_host.my_builder.user == "myuser"
    assert settings.remote_host.my_builder.address == "192.168.2.2"


def test_no_global_settings_in_autosd_demo(tmp_dir, global_config):
    local_config = tmp_dir / AUTOSD_DEMO_LOCAL_CONF_FILENAME
    local_config.write_text(LOCAL_SETTINGS)

    with patch("autosd_demo.settings.AUTOSD_DEMO_GLOBAL_CONF_FILE", global_config):
        settings = Settings()
    assert Path(settings.base_dir) == tmp_dir
    assert settings.version == "2.0.0"
    assert "_dnf" not in settings["rpm"]
    assert settings.remote_host.my_builder.user == "myuser"
    assert settings.remote_host.my_builder.address == "192.168.2.2"


def test_global_settings_out_autosd_demo(tmp_dir, global_config):
    global_config.write_text(GLOBAL_SETTINGS)
    with patch("autosd_demo.settings.AUTOSD_DEMO_GLOBAL_CONF_FILE", global_config):
        settings = Settings()
    # Only remote host is allowed in the settings.
    assert "base_dir" not in settings
    assert "version" not in settings
    assert "rpm" not in settings
    assert settings.remote_host.my_builder.user == "myuser"
    assert settings.remote_host.my_builder.address == "192.168.1.1"


def test_no_global_settings_out_autosd_demo(tmp_dir, global_config):
    with patch("autosd_demo.settings.AUTOSD_DEMO_GLOBAL_CONF_FILE", global_config):
        settings = Settings()
    # Settings ar expected to be empty.
    assert not settings.as_dict()


def test_build_update_env(tmp_dir, global_config):
    local_config = tmp_dir / AUTOSD_DEMO_LOCAL_CONF_FILENAME
    local_config.write_text(LOCAL_SETTINGS)

    with patch("autosd_demo.settings.AUTOSD_DEMO_GLOBAL_CONF_FILE", global_config):
        config.reload_settings()
    assert "AUTOSD_DEMO_ENV" not in os.environ
    assert config.settings.current_env == "DEVELOPMENT"

    with patch("autosd_demo.settings.AUTOSD_DEMO_GLOBAL_CONF_FILE", global_config):
        config.reload_settings("devel")
    assert os.environ["AUTOSD_DEMO_ENV"] == "devel"
    assert config.settings.current_env == "devel"

    with patch("autosd_demo.settings.AUTOSD_DEMO_GLOBAL_CONF_FILE", global_config):
        config.reload_settings("prod")
    assert os.environ["AUTOSD_DEMO_ENV"] == "prod"
    assert config.settings.current_env == "prod"


enrich_container_settings_test_cases = [
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "unit",
        "description",
        "test_container container",
    ),
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "container",
        "container_name",
        "test_container",
    ),
    (
        {"containers": {"test_container": {"version": "0.1"}}},
        "test_container",
        "container",
        "image",
        "test_container:0.1",
    ),
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "install",
        "wanted_by",
        "multi-user.target",
    ),
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "service",
        "restart",
        "always",
    ),
    (
        {"containers": {"test_container": {"systemd": {}}}},
        "test_container",
        "service",
        "restart",
        "always",
    ),
    (
        {"containers": {"test_container": {"systemd": {"service": {}}}}},
        "test_container",
        "service",
        "restart",
        "always",
    ),
    (
        {
            "containers": {
                "test_container": {"systemd": {"service": {"restart": "never"}}}
            }
        },
        "test_container",
        "service",
        "restart",
        "never",
    ),
    (
        {
            "containers": {
                "test_container": {
                    "image": "myapp",
                    "registry": "quay.io",
                    "namespace": "myorg",
                }
            }
        },
        "test_container",
        "container",
        "image",
        "quay.io/myorg/myapp:latest",
    ),
]


@pytest.mark.parametrize(
    "settings,container_name,section,key,expected", enrich_container_settings_test_cases
)
def test_enrich_container_settings_systemd(
    settings, container_name, section, key, expected
):
    mock_settings = MockSettings(settings)
    mock_settings._enrich_container_settings()
    assert (
        mock_settings["containers"][container_name]["systemd"][section][key] == expected
    )


enrich_container_settings_build_args_test_cases = [
    (
        {"containers": {"test_container": {"build_args": {"ARG1": "VALUE1"}}}},
        "test_container",
        "--build-arg ARG1=VALUE1",
    ),
    (
        {
            "containers": {
                "test_container": {
                    "extra_args": "--cache python:latest",
                    "build_args": {"ARG1": "VALUE1"},
                }
            }
        },
        "test_container",
        "--cache python:latest --build-arg ARG1=VALUE1",
    ),
    (
        {"containers": {"test_container": {"extra_args": "--cache python:latest"}}},
        "test_container",
        "--cache python:latest",
    ),
]


@pytest.mark.parametrize(
    "settings,container_name,expected", enrich_container_settings_build_args_test_cases
)
def test_enrich_container_settings_extra_args(settings, container_name, expected):
    mock_settings = MockSettings(settings)
    mock_settings._enrich_container_settings()
    assert mock_settings["containers"][container_name]["extra_args"] == expected


@pytest.mark.parametrize(
    "settings,expected",
    [
        (
            {
                "apps": {
                    "app1": {},
                }
            },
            {"app1": {}},
        ),
        (
            {
                "apps": {
                    "app1": {"containers": {"backend": {"image": "backend"}}},
                }
            },
            {
                "app1": {
                    "containers": {
                        "backend": {
                            "base_image": None,
                            "build_order": DEFAULT_BUILD_ORDER,
                            "extra_args": "",
                            "image": "backend",
                            "image_ref": "backend:latest",
                            "systemd": {
                                "container": {
                                    "container_name": "backend",
                                    "image": "backend:latest",
                                },
                                "install": {"wanted_by": "multi-user.target"},
                                "service": {"restart": "always"},
                                "unit": {"description": "backend " "container"},
                            },
                            "version": "latest",
                        }
                    }
                }
            },
        ),
        (
            {
                "apps": {
                    "app1": {
                        "containers": {
                            "base": {
                                "image": "base",
                                "container_context": "containers/base",
                            },
                            "backend": {
                                "base_image": "base",
                                "image": "backend",
                            },
                        }
                    }
                }
            },
            {
                "app1": {
                    "containers": {
                        "backend": {
                            "base_image": "base",
                            "build_order": 81,
                            "extra_args": " --build-arg "
                            "BASE_IMAGE=localhost/base:latest",
                            "image": "backend",
                            "image_ref": "backend:latest",
                            "systemd": {
                                "container": {
                                    "container_name": "backend",
                                    "image": "backend:latest",
                                },
                                "install": {"wanted_by": "multi-user.target"},
                                "service": {"restart": "always"},
                                "unit": {"description": "backend " "container"},
                            },
                            "version": "latest",
                        },
                        "base": {
                            "base_image": None,
                            "build_order": 80,
                            "container_context": "containers/base",
                            "extra_args": "",
                            "image": "base",
                            "image_ref": "localhost/base:latest",
                            "systemd": {
                                "container": {
                                    "container_name": "base",
                                    "image": "localhost/base:latest",
                                },
                                "install": {"wanted_by": "multi-user.target"},
                                "service": {"restart": "always"},
                                "unit": {"description": "base " "container"},
                            },
                            "version": "latest",
                        },
                    }
                }
            },
        ),
        (
            {
                "apps": {
                    "app1": {
                        "containers": {
                            "base": {
                                "image": "fedora",
                                "version": "40",
                                "namespace": "mydemo",
                                "registry": "quay.io",
                            },
                            "backend": {
                                "base_image": "base",
                                "image": "backend",
                            },
                        }
                    }
                }
            },
            {
                "app1": {
                    "containers": {
                        "backend": {
                            "base_image": "base",
                            "build_order": 81,
                            "extra_args": " --build-arg "
                            "BASE_IMAGE=quay.io/mydemo/fedora:40",
                            "image": "backend",
                            "image_ref": "backend:latest",
                            "systemd": {
                                "container": {
                                    "container_name": "backend",
                                    "image": "backend:latest",
                                },
                                "install": {"wanted_by": "multi-user.target"},
                                "service": {"restart": "always"},
                                "unit": {"description": "backend " "container"},
                            },
                            "version": "latest",
                        },
                        "base": {
                            "base_image": None,
                            "build_order": 80,
                            "extra_args": "",
                            "image": "fedora",
                            "image_ref": "quay.io/mydemo/fedora:40",
                            "namespace": "mydemo",
                            "registry": "quay.io",
                            "systemd": {
                                "container": {
                                    "container_name": "base",
                                    "image": "quay.io/mydemo/fedora:40",
                                },
                                "install": {"wanted_by": "multi-user.target"},
                                "service": {"restart": "always"},
                                "unit": {"description": "base " "container"},
                            },
                            "version": "40",
                        },
                    }
                }
            },
        ),
        (
            {
                "apps": {
                    "app1": {
                        "containers": {
                            "base": {
                                "image": "fedora",
                                "version": "40",
                                "namespace": "mydemo",
                                "registry": "quay.io",
                            },
                            "backend": {
                                "build_order": 90,
                                "base_image": "base",
                                "image": "backend",
                            },
                        }
                    }
                }
            },
            {
                "app1": {
                    "containers": {
                        "backend": {
                            "base_image": "base",
                            "build_order": 90,
                            "extra_args": " --build-arg "
                            "BASE_IMAGE=quay.io/mydemo/fedora:40",
                            "image": "backend",
                            "image_ref": "backend:latest",
                            "systemd": {
                                "container": {
                                    "container_name": "backend",
                                    "image": "backend:latest",
                                },
                                "install": {"wanted_by": "multi-user.target"},
                                "service": {"restart": "always"},
                                "unit": {"description": "backend " "container"},
                            },
                            "version": "latest",
                        },
                        "base": {
                            "base_image": None,
                            "build_order": 80,
                            "extra_args": "",
                            "image": "fedora",
                            "image_ref": "quay.io/mydemo/fedora:40",
                            "namespace": "mydemo",
                            "registry": "quay.io",
                            "systemd": {
                                "container": {
                                    "container_name": "base",
                                    "image": "quay.io/mydemo/fedora:40",
                                },
                                "install": {"wanted_by": "multi-user.target"},
                                "service": {"restart": "always"},
                                "unit": {"description": "base " "container"},
                            },
                            "version": "40",
                        },
                    }
                }
            },
        ),
        (
            {
                "apps": {
                    "app1": {
                        "containers": {
                            "backend": {
                                "base_image": "quay.io/mydemo/fedora:40",
                                "image": "backend",
                            },
                        }
                    }
                }
            },
            {
                "app1": {
                    "containers": {
                        "backend": {
                            "base_image": "quay.io/mydemo/fedora:40",
                            "build_order": 80,
                            "extra_args": " --build-arg "
                            "BASE_IMAGE=quay.io/mydemo/fedora:40",
                            "image": "backend",
                            "image_ref": "backend:latest",
                            "systemd": {
                                "container": {
                                    "container_name": "backend",
                                    "image": "backend:latest",
                                },
                                "install": {"wanted_by": "multi-user.target"},
                                "service": {"restart": "always"},
                                "unit": {"description": "backend " "container"},
                            },
                            "version": "latest",
                        }
                    }
                }
            },
        ),
        (
            {
                "apps": {
                    "app1": {
                        "asil": {
                            "base_image": "quay.io/mydemo/fedora:40",
                        },
                        "containers": {
                            "backend": {
                                "app": {"name": "app1"},
                                "image": "backend",
                            },
                        },
                    }
                }
            },
            {
                "app1": {
                    "asil": {"base_image": "quay.io/mydemo/fedora:40"},
                    "containers": {
                        "backend": {
                            "app": {"name": "app1"},
                            "base_image": "quay.io/mydemo/fedora:40",
                            "build_order": 80,
                            "extra_args": " --build-arg "
                            "BASE_IMAGE=quay.io/mydemo/fedora:40",
                            "image": "backend",
                            "image_ref": "backend:latest",
                            "systemd": {
                                "container": {
                                    "container_name": "backend",
                                    "image": "backend:latest",
                                },
                                "install": {"wanted_by": "multi-user.target"},
                                "service": {"restart": "always"},
                                "unit": {"description": "backend " "container"},
                            },
                            "version": "latest",
                        }
                    },
                }
            },
        ),
    ],
)
def test_enrich_container_settings(settings, expected):
    mock_settings = MockSettings(settings)
    mock_settings._enrich_container_settings()

    assert mock_settings.get("apps", {}) == expected


@pytest.mark.parametrize(
    "settings,expected",
    [
        (
            {
                "apps": {
                    "app1": {
                        "name": "app1",
                        "containers": {"backend": {"image": "backend"}},
                    },
                    "app2": {"name": "app2"},
                }
            },
            {
                "app1": {
                    "containers": {
                        "backend": {
                            "app": {"name": "app1", "version": "0.0.1"},
                            "image": "backend",
                        }
                    },
                    "name": "app1",
                    "version": "0.0.1",
                    "dest_dir": "apps",
                    "required": [],
                },
                "app2": {
                    "name": "app2",
                    "version": "0.0.1",
                    "dest_dir": "apps",
                    "required": [],
                },
            },
        ),
        ({}, {}),
    ],
)
def test_enrich_apps_settings(settings, expected):
    mock_settings = DynaconfDict(settings)
    Settings._enrich_apps_settings(mock_settings)

    assert mock_settings.get("apps", {}) == expected


@pytest.mark.parametrize(
    "settings,container_name,section,key",
    [
        (
            {
                "containers": {
                    "test_container": {"systemd": {"install": {"wanted_by": ""}}}
                }
            },
            "test_container",
            "install",
            "wanted_by",
        ),
        (
            {
                "containers": {
                    "test_container": {"systemd": {"service": {"restart": ""}}}
                }
            },
            "test_container",
            "service",
            "restart",
        ),
    ],
)
def test_enrich_container_settings_clean(settings, container_name, section, key):
    mock_settings = MockSettings(settings)
    mock_settings._enrich_container_settings()
    with pytest.raises(KeyError):
        mock_settings["containers"][container_name]["systemd"][section][key]


def test_get_addons_preload_includes(tmp_dir):
    with (tmp_dir / AUTOSD_DEMO_LOCAL_CONF_FILENAME).open("w") as fd:
        fd.write("[default]\n")
        fd.write("addons=['weston', 'ssh_server', 'test']\n")

    settings = Settings()
    assert settings.rpm._ssh_server_pkgs.packages == ["openssh-server"]
    # Ensure nested addons are listed.
    assert "qm_dbus_broker" in settings.addons
    assert "wayland" in settings.addons


def test_get_addons_preload_validator_with_apps(tmp_dir):
    with (tmp_dir / AUTOSD_DEMO_LOCAL_CONF_FILENAME).open("w") as fd:
        fd.write("[default]\n")
        fd.write("addons=['ssh_server', 'test']\n")
        fd.write("[default.apps.example]\n")
        fd.write("image='example_image'\n")

    settings = Settings()
    assert "ssh_server" in settings.centos_sig.autosd_demo.addons
    # Ensure validator addon is appended when apps are configured.
    assert "validator" in settings.centos_sig.autosd_demo.addons


def test_delete_settings_key(tmp_dir):
    with (tmp_dir / AUTOSD_DEMO_LOCAL_CONF_FILENAME).open("w") as fd:
        fd.write(EXTRA_FILE_SETTINGS)

    settings = Settings()
    assert settings.extra_files
    assert settings.containers
    assert settings.containers.server
    assert settings.containers.client
    # delete extra_files
    del settings["extra_files"]
    assert "extra_files" not in settings
    del settings["containers"]["client"]
    assert "containers" in settings
    assert "client" not in settings["containers"]


def test_find_and_remove_omitted_items():
    mock_settings = DynaconfDict({})
    resources = [
        "apps",
        "containers",
        "dracut",
        "extra_files",
        "fs_perm",
        "hooks",
        "systemd",
        "registry_creds",
        "remote_host",
        "rpm",
        "rpm_repo",
    ]

    for resource in resources:
        mock_settings[resource] = {}
        mock_settings[resource][f"{resource}_1"] = {}
        mock_settings[resource][f"{resource}_2"] = {"omit": True}
        mock_settings[resource][f"{resource}_3"] = {"omit": False}
        mock_settings[resource][f"{resource}_4"] = {"omit": "True"}
        mock_settings[resource][f"{resource}_5"] = {"omit": "False"}

    Settings._find_and_remove_omitted_items(mock_settings)

    for resource in resources:
        assert f"{resource}_1" in mock_settings[resource]
        assert f"{resource}_2" not in mock_settings[resource]
        assert f"{resource}_3" in mock_settings[resource]
        assert f"{resource}_4" not in mock_settings[resource]
        assert f"{resource}_5" in mock_settings[resource]
