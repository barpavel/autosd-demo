#!/usr/bin/env bash

set -e

CURRENT_VERSION=$(grep -Po '(?<=^version = ")[^"]*' ./pyproject.toml)
NEW_VERSION=$(git cliff --bumped-version)

if git rev-parse "refs/tags/$NEW_VERSION" >/dev/null 2>&1
then
  echo "tag $NEW_VERSION exists"
  exit 1
fi

# Update CHANGELOG.md
git cliff --bump -o CHANGELOG.md

# Update pyproject.toml
sed -e "s/^version = \"$CURRENT_VERSION\"$/version = \"$NEW_VERSION\"/" \
        -i ./pyproject.toml

# Prepare commit and commit it
git add pyproject.toml CHANGELOG.md
git commit --no-verify -m "chore(release): update files for $NEW_VERSION"

# Create the new release tag
git tag -am "$NEW_VERSION" "$NEW_VERSION"
